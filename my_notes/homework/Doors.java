import java.util.*;


public class Doors{

	public static void main(String[] args)
	{
		//assume that all doors are locked
		int[] doors = new int[10];


		//initialize doors
		for(int i = 0; i < doors.length;i++)
		{
			doors[i] = 0;
		}

		//other
		int n = 1;
		while(n <= 10)
		{

			for(int i = 0; i < doors.length;i++)
			{
				if(i+1%n == 0)
				{
					if(doors[i] == 0)
					{
						doors[i] = 1;
					}
					else
					{
						doors[i] = 0;
					}
				}	
			}
			n++;
		}

		int count = 0;
		for(int i = 0; i < doors.length;i++)
		{
			if(doors[i] == 1)
			{
				count++;
			}
		}
		System.out.println(count);
	
	}

}