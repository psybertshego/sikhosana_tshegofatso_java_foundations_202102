
import java.util.*;

public class Primes{


	public static void main(String[] args) {

    ArrayList<Integer> primes = new ArrayList<>();

    int n = 1;

    while(n <= 2000000){

      if(primes.size() > 0){

        for(int i = 0;i < primes.size();i++ ){

					if(isPrime(n,primes)){
            primes.add(n);
          }
        }
      }
      else{

			  if(n>1){
          primes.add(n);
        }
      }
      n++;
    }
		print(primes);
  }

	public static boolean isPrime(int n,ArrayList<Integer> primes){

		for(int i = 0; i < primes.size();i++){
			if(n%primes.get(i) == 0){
				return false;
			}
		}
		return true;
	}

  public static void print(ArrayList<Integer> primes){
    for(int i = 0;i < primes.size();i++ ){
      System.out.println(primes.get(i)+" ");
    }
  }
}
