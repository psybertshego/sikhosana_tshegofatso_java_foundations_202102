package com.psybergate.grad2021.core.relfection.ce1a;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

public class StudentReflectionClient {
  public static void main(String[] args) {
    Student s =new Student();

    Class studentClass = s.getClass();

    System.out.println("studentClass.getSimpleName() = " + studentClass.getSimpleName());
    System.out.println("studentClass.getPackage() = " + studentClass.getPackage());

    System.out.println("Annotations:");
    printData(studentClass.getAnnotations());

    System.out.println("Interfaces:");
    printData(studentClass.getInterfaces());

    System.out.println("Public fields:");
    printMemberData(studentClass.getFields());

    System.out.println("Fields:");
    printMemberData(studentClass.getDeclaredFields());

    System.out.println("Constructor Parameters:");
    printConstructorParameterNames(studentClass.getConstructors());

    System.out.println("Method names");
    printMemberData(studentClass.getDeclaredMethods());
  }

  private static <T extends Member> void printMemberData(T[] information){
    for (T data : information) {
      System.out.println(data.getName());
    }
    System.out.println();
  }

  private static <T> void printData(T[] information){
    for (T data : information) {
      System.out.println(data);
    }
    System.out.println();

  }

  private static void printConstructorParameterNames(Constructor[] constructors){
    for (Constructor constructor : constructors) {
      System.out.println("constructor.getName() = " + constructor.getName());
      printData(constructor.getParameters());
    }
  }
}
