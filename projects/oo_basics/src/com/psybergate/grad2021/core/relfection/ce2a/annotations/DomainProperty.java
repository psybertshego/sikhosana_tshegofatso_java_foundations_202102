package com.psybergate.grad2021.core.relfection.ce2a.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.LOCAL_VARIABLE,ElementType.FIELD})
public @interface DomainProperty {
  boolean nullable() default false;
  boolean primaryKey() default false;
  boolean unique() default false;
}
