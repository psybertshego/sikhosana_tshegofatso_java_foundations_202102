package com.psybergate.grad2021.core.relfection.hw2;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class MonthsClient {
  public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException {

    String months = "January,February,March,April,May,June,July,August,September,October,November,December";

    ClassLoader classLoader = MonthsClient.class.getClassLoader();

//    Class c = classLoader.loadClass("java.util.List");

    Properties properties = new Properties();

    InputStream file = MonthsClient.class.getClassLoader().getResourceAsStream("com/psybergate/grad2021/core/relfection/hw2/list_type.txt");

    properties.load(file);

    String listType = properties.getProperty("listType");

    System.out.println("listType = " + listType);

    Class<List<String>> c2 = (Class<List<String>>) Class.forName(listType);

    List<String> list = c2.newInstance();


//    List<String> linkedList = LinkedList.class.newInstance();
//    printList(linkedList);
//    //ClassLoader classLoader1 = c2.getClassLoader();
//    List<String> arrayList = ArrayList.class.newInstance();
//
//    printList(setMonths(months,linkedList));
//    printList(setMonths(months,arrayList));
    printList(setMonths(months, list));

  }

  private static <T extends List<String>> T setMonths(String months, T list) {
    list.addAll(Arrays.asList(months.split(",")));
    return list;
  }

  private static <T extends List<String>> void printList(T months) {
    int i = 1;
    for (String month : months) {
      System.out.println(i + ". " + month);
      i++;
    }
  }
}
