package com.psybergate.grad2021.core.relfection.hw1;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class StudentReflectionClient {
  public static void main(String[] args) {
    Student s = new Student();

    Class studentClass = s.getClass();

    System.out.println("studentClass.getPackage() = " + studentClass.getPackage());

    System.out.println("\nSuper Class:" + studentClass.getSuperclass().getSimpleName());

    System.out.println("\nClass:  \n" + addModifiers(studentClass.getModifiers()) + " " + studentClass.getSimpleName());

    System.out.println("\nAnnotations:");
    printData(studentClass.getAnnotations());

    System.out.println("Interfaces:");
    printData(studentClass.getInterfaces());

    System.out.println("Public fields:");
    printMemberData(studentClass.getFields());

    System.out.println("Fields:");
    printMemberData(studentClass.getDeclaredFields());

    System.out.println("Constructor Parameters:");
    printConstructorParameterNames(studentClass.getConstructors());

    System.out.println("Method names");
    printMemberData(studentClass.getDeclaredMethods());
  }

  private static <T extends Member> void printMemberData(T[] information) {
    for (T member : information) {
      if(member instanceof Method){
        System.out.print("Returns (" + ((Method) member).getReturnType().getName() + ") ");
      }
      System.out.println(addModifiers(member.getModifiers()) + " " + member.getName());
    }
    System.out.println();
  }

  private static <T> void printData(T[] information) {
    for (T data : information) {
      System.out.println(data);
    }
    System.out.println();
  }

  private static void printConstructorParameterNames(Constructor<String>[] constructors) {
    for (Constructor<String> constructor : constructors) {
      System.out.println("constructor.getName() = " + constructor.getName());
      printData(constructor.getParameters());
    }
  }

  private static String addModifiers(int modifier) {
    return Modifier.toString(modifier);
  }
}
