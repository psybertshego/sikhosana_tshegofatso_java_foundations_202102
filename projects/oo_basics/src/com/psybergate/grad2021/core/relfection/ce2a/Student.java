package com.psybergate.grad2021.core.relfection.ce2a;


import com.psybergate.grad2021.core.relfection.ce2a.annotations.DomainClass;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable {

  public int studentNum;

  private String name;

  private String surname;

  private int age;

  private int yearOfStudy;

  public Student() {
  }

  public Student(int studentNum, String name) {
    this.studentNum = studentNum;
    this.name = name;
  }

  public Student(int studentNum, String name, String surname, int age, int yearOfStudy) {
    this.studentNum = studentNum;
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.yearOfStudy = yearOfStudy;
  }

  public int getStudentNum() {
    return studentNum;
  }

  public void setStudentNum(int studentNum) {
    this.studentNum = studentNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getYearOfStudy() {
    return yearOfStudy;
  }

  public void setYearOfStudy(int yearOfStudy) {
    this.yearOfStudy = yearOfStudy;
  }

  public String getFullName(){
    return getName() + " " + getSurname();
  }

  public int yearsLeftBeforeCompletion(){
    return 3 - yearOfStudy;
  }
}
