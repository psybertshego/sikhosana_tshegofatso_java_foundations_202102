package com.psybergate.grad2021.core.relfection.ce2a;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class StudentReflectionClient {
  public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

    Class studentClass = Student.class;

    Constructor<Student> constructor1 = studentClass.getConstructor();

    Constructor<Student> constructor3 = studentClass.getConstructor(int.class, String.class, String.class, int.class, int.class);

    Student kate = constructor3.newInstance(140008, "katie", "perry", 21, 3);

    Method m1 = studentClass.getMethod("getName");
    System.out.println("m1.invoke(kate) = " + m1.invoke(kate));

  }

//  private static <T> T  getStudentInstance(){
//    return T;
//  }

  private static <T> void printData(T[] information) {
    for (T data : information) {
      System.out.println(data);
    }
    System.out.println();
  }

  private static void printConstructorParameterNames(Constructor[] constructors) {
    for (Constructor constructor : constructors) {
      System.out.println("constructor.getName() = " + constructor.getName());
      printData(constructor.getParameters());
    }
  }
}
