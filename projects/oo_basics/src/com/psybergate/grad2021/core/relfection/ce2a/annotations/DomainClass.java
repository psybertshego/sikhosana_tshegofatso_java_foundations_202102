package com.psybergate.grad2021.core.relfection.ce2a.annotations;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DomainClass {

}
