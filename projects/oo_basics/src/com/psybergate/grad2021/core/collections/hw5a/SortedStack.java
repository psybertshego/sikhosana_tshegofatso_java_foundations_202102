package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;

public class SortedStack extends AbstractStack {

  private Object[] elements = new Object[100_000];
  private int size = 0;


  public void push(Object obj) {
    if (size == 0) {
      elements[size] = obj;
      size++;
      return;
    }
    //sort the stack before I insert

    for (int i = 0; i < size; i++) {

      if (((Comparable) elements[i]).compareTo(obj) < 0) {
        // negative
        Object temp = elements[i];
        elements[i] = obj;
        size++;
        // we shifting the elements up, using a swap
        for (int j = i + 1; j < size; j++) {
          Object secondTemp = elements[j];
          elements[j] = temp;
          temp = secondTemp;
        }
        return;
      }
    }

    // when it's the smallest element in the list
    elements[size] = obj;
    size++;
  }

  public void pop() {
    elements[size] = null;
    size--;
  }

  @Override
  public Object[] toArray() {
    return elements;
  }

  @Override
  public Iterator iterator() {
    return new SortedStackIterator(this);
  }

  @Override
  public int size() {
    return size;
  }
}
