package com.psybergate.grad2021.core.collections.hw2a;

import java.util.List;

public class SortingUtils {

  public static void sortByProductNumDescending(List<Product> products) {
    for (int i = 0; i < products.size(); i++) {
      for (int j = 0; j < products.size() - i - 1; j++) {
        if (getProductNum(products.get(j)) < getProductNum(products.get(j + 1))) {
          Product product = products.get(j + 1);
          products.set(j + 1, products.get(j));
          products.set(j, product);
        }
      }
    }
  }

  public static void sortByProductNumAscending(List<Product> products) {
    for (int i = 0; i < products.size(); i++) {
      for (int j = 0; j < products.size() - i - 1; j++) {
        if (getProductNum(products.get(j)) > getProductNum(products.get(j + 1))) {
          Product product = products.get(j + 1);
          products.set(j + 1, products.get(j));
          products.set(j, product);
        }
      }
    }
  }


  public static void sortByProductPriceDescending(List<Product> products) {
    for (int i = 0; i < products.size(); i++) {
      for (int j = 0; j < products.size() - i - 1; j++) {
        if (products.get(j).getPrice() < products.get(j + 1).getPrice()) {
          Product product = products.get(j + 1);
          products.set(j + 1, products.get(j));
          products.set(j, product);
        }
      }
    }
  }

  public static void sortByProductPriceAscending(List<Product> products) {
    for (int i = 0; i < products.size(); i++) {
      for (int j = 0; j < products.size() - i - 1; j++) {
        if (products.get(j).getPrice() > products.get(j + 1).getPrice()) {
          Product product = products.get(j + 1);
          products.set(j + 1, products.get(j));
          products.set(j, product);
        }
      }
    }
  }

  public static int getProductNum(Product product) {
    return Integer.parseInt(product.getProductNum());
  }


}
