package com.psybergate.grad2021.core.collections.ce1a;

import java.util.Collection;

public interface Stack extends Collection {

  public void push(Object o);

  public void pop();

  public Object get(int position);
}
