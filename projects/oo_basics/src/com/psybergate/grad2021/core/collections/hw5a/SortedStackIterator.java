package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;

public class SortedStackIterator implements Iterator {

  private int currentIndex;
  private Object[] elements;

  public SortedStackIterator(SortedStack sortedStack) {
    currentIndex = sortedStack.size() - 1;
    elements = sortedStack.toArray();
  }

  @Override
  public boolean hasNext() {
    return currentIndex > -1;
  }

  @Override
  public Object next() {
    return elements[currentIndex--];
  }
}
