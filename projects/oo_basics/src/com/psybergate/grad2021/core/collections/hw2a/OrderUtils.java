package com.psybergate.grad2021.core.collections.hw2a;

import java.util.HashMap;
import java.util.Set;

public class OrderUtils {

    private static HashMap<Product, Integer> products = new HashMap<>();

    public static void main(String[] args) {
        products = initializeProductList();
        printProducts(products);
    }

    private static void printProducts(HashMap<Product, Integer> products) {
        Set keys = products.keySet();
        int i = 0;
        for (final Object key : keys) {
            System.out.println(i++ + " (" + key + ")  = " + products.get(key));
        }
        System.out.println("map.size = " + products.size());

    }

    private static HashMap<Product, Integer> initializeProductList() {
        // 18 Products
        HashMap<Product, Integer> products = new HashMap<>();

        products.put(new Product("001", "Bicycle"), 700);
        products.put(new Product("001", "Skateboard"), 500);
        products.put(new Product("003", "Hoverboard"), 1000);
        products.put(new Product("005", "Scooter"), 800);
//        products.put(new Product("010", "Couch"), 1000);
//        products.put(new Product("011", "Chair"), 4000);
//        products.put(new Product("012", "Table"), 6000);
//        products.put(new Product("022", "Laptop"), 200000);
//        products.put(new Product("021", "Wireless Mouse"), 200);
//        products.put(new Product("020", "Wireless Keyboard"), 300);
//        products.put(new Product("055", "Wine"), 1500);
//        products.put(new Product("056", "Roses"), 500);
//        products.put(new Product("067", "Chocolate"), 300);
//        products.put(new Product("076", "Lemon"), 30);
//        products.put(new Product("077", "Orange"), 20);
//        products.put(new Product("023", "Stapler") , 100);
//        products.put(new Product("025", "Pencil"), 5);
//        products.put(new Product("027", "Notebook"), 20);
        return products;
    }
}
