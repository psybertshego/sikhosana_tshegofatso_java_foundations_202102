package com.psybergate.grad2021.core.collections.hw4a;

public class Node {
  private Object data;
  public Node nextNode;

  public Node(Object object) {
    this.data = object;
  }

  public void setNextNode(Node nextNode) {
    this.nextNode = nextNode;
  }

  public Node getNextNode() {
    return nextNode;
  }

  public Object getData() {
    return data;
  }

  public Object setData(Object element) {
    Object previous = this.data;
    this.data = element;
    return previous;
  }
}
