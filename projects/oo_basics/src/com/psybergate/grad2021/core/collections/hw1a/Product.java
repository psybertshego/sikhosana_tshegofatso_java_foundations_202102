package com.psybergate.grad2021.core.collections.hw1a;

public class Product {
    private String productNum;
    private String productName;
    private double price;


    public Product(String productNum, String productName, double price) {
        this.productNum = productNum;
        this.productName = productName;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productNum = '" + productNum + '\'' +
                ", productName = '" + productName + '\'' +
                ", price = " + price +
                '}';
    }

    public Product(String productName, double price) {
        this.productName = productName;
        this.price = price;

    }

    public String getProductNum() {
        return productNum;
    }

    public double getPrice() {
        return price;
    }

    public String getProductName() {
        return productName;
    }
}
