package com.psybergate.grad2021.core.collections.inclass;

public class Arrays {
  public static void main(String[] args) {
    char[] chars = new char[5]; // prints "" (no character)
//    String[] chars = new String[5]; //prints null

    for (int i = 0; i < chars.length; i++) {
      System.out.println((int) chars[i]);
    }

    arrayReflection();

    int[][][] dimension3 = new int[2][][];
    int[][][] implicit3D = {{{1}, {2, 2}, {3, 3, 3}}, {{4, 4, 4, 4}, {5, 5, 5, 5, 5}}, {{6, 6, 6, 6, 6, 6}}};
    print3dArray(implicit3D);
  }


  //Make a 3d array and more
  private static void print3dArray(int[][][] arr3D) {
    System.out.println("arr3D.length = " + arr3D.length);
    System.out.println("arr3D[0][1].length = " + arr3D[0][1].length);
    System.out.println("arr3D[1][1].length = " + arr3D[1][1].length);
    System.out.println("arr3D[2].length = " + arr3D[2][0].length);
    for (int i = 0; i < arr3D.length; i++) {
      //level1
      System.out.println("Level 1: ");
      for (int j = 0; j < arr3D[i].length; j++) {
        //level2
        System.out.println("Level 2: ");
        for (int k = 0; k < arr3D[i][j].length; k++) {
          //level3
//          System.out.println("Level 3: ");
          System.out.print(arr3D[i][j][k] + ",");
        }
        System.out.println();
      }
    }
  }

  private static void arrayReflection() {
    int[] arr = new int[3];
    String[] strArr = new String[5];

    System.out.println("int[].class = " + int[].class);
    System.out.println("strArr.getClass() = " + strArr.getClass());
    System.out.println("arr.getClass() = " + arr.getClass());
    System.out.println("arr = " + arr.getClass().getSuperclass());

  }
}



