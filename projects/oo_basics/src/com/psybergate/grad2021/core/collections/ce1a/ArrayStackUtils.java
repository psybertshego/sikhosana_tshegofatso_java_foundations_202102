package com.psybergate.grad2021.core.collections.ce1a;

public class ArrayStackUtils {
  public static void main(String[] args) {
    ArrayStack arrayStack = new ArrayStack();
    arrayStack.push(1);
    arrayStack.push(2);
    arrayStack.push(3);
    arrayStack.push(4);
    arrayStack.pop();
//    arrayStack.pop();

    System.out.println("arrayStack.contains(2) = " + arrayStack.contains(2));

    printStack(arrayStack);
    System.out.println("arrayStack.size() = " + arrayStack.size());
  }


  private static void printStack(ArrayStack arrayStack) {
//    for (int i = 0; i < arrayStack.size(); i++) {
//      System.out.println(arrayStack.get(i));
//    }
    for (Object o : arrayStack) {
      System.out.println(o);
    }
  }
}
