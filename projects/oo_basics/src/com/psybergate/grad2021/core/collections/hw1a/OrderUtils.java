package com.psybergate.grad2021.core.collections.hw1a;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {

    private static List<Product> products = new ArrayList<>();

    public static void main(String[] args) {
        products = initializeProductList();
        printProducts(products,ListOrder.DESCENDING);
    }

    private static void printProducts(List<Product> products,ListOrder type) {

        if(type == ListOrder.ASCENDING){
            SortingUtils.sortByProductNumAscending(products);
        }else if(type == ListOrder.DESCENDING){
            SortingUtils.sortByProductNumDescending(products);
        }

        for (Product product : products) {
            System.out.println(product);
        }
    }


    private static List<Product> initializeProductList() {
        // 17 Products
        List<Product> productList = new ArrayList<>();
        productList.add(new Product("001", "Bicycle", 700));
        productList.add(new Product("002", "Skateboard", 500));
        productList.add(new Product("003", "Hoverboard", 1000));
        productList.add(new Product("005", "Scooter", 800));
        productList.add(new Product("010", "Couch", 1000));
        productList.add(new Product("011", "Chair", 4000));
        productList.add(new Product("012", "Table", 6000));
        productList.add(new Product("022", "Laptop", 20000));
        productList.add(new Product("021", "Wireless Mouse", 200));
        productList.add(new Product("020", "Wireless Keyboard", 300));
        productList.add(new Product("055", "Wine", 1500));
        productList.add(new Product("056", "Roses", 500));
        productList.add(new Product("067", "Chocolate", 300));
        productList.add(new Product("076", "Lemon", 30));
        productList.add(new Product("077", "Orange", 20));
        productList.add(new Product("023", "Stapler", 100));
        productList.add(new Product("025", "Pencil", 5));
        productList.add(new Product("027", "Notebook", 20));
        return productList;
    }
}
