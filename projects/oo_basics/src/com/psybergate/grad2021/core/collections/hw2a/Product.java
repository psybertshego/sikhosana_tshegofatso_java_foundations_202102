package com.psybergate.grad2021.core.collections.hw2a;

public class Product {
    private String productNum;
    private String productName;
    private double price;


    public Product(String productNum, String productName, double price) {
        this.productNum = productNum;
        this.productName = productName;
        this.price = price;
    }

    public Product(String productNum, String productName) {
        this.productNum = productNum;
        this.productName = productName;
    }

    public String getProductNum() {
        return productNum;
    }

    public double getPrice() {
        return price;
    }

    public String getProductName() {
        return productName;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productNum = '" + productNum + '\'' +
                ", productName = '" + productName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || !(obj instanceof Product)) return false;
        Product product = (Product) obj;
        return product.productNum == this.productNum;
    }

//    @Override
//    public int hashCode() {
//        int prime = 13;
//        return prime * Integer.parseInt(productNum);
//    }
}
