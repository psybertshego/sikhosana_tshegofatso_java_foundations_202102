package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Iterator;
import java.util.List;

public class LinkedListUtils {
  public static void main(String[] args) {
    List myLinkedList = new MyLinkedList();
    myLinkedList.add("abc");
    myLinkedList.add("cd");
    myLinkedList.add("gif");

    System.out.println("myLinkedList.size() = " + myLinkedList.size());
    for (Iterator i = myLinkedList.iterator(); i.hasNext(); ) {
      System.out.println(i.next());
    }

    MyLinkedListTest test  = new MyLinkedListTest();
    test.before();
    test.testEmptyList();

  }
}

