package com.psybergate.grad2021.core.collections.hw5a;

import java.util.Iterator;

public class SortedStackClient {
  public static void main(String[] args) {
    SortedStack s = new SortedStack();
    s.push(1203);
    s.push(100);
    s.push(29);
    s.push(42);
    s.push(21);
    s.push(13);
    s.pop();

    System.out.println("s.size() = " + s.size());

    for(Iterator i = s.iterator(); i.hasNext();){
      System.out.println(i.next().toString());
    }
  }
}
