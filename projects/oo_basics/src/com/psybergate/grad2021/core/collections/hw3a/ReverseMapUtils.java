package com.psybergate.grad2021.core.collections.hw3a;

import java.util.*;

public class ReverseMapUtils {
  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    List<String> strData = new ArrayList<>();
    System.out.println("<-<-<- MAPS LIFE ->->-> \n" + "Enter car data (brand,model): ( -1 = exit) ");
    String brand = "";
    while (scanner.hasNextLine()) {
      brand = scanner.nextLine();
      if (brand.equals("exit")) {
        break;
      }
      strData.add(brand);
    }

    SortingUtils.sortByDescending(strData);

    printData(strData);

    //System.out.println("a".compareTo("bcd"));

  }

  private static void printData(List<String> strData) {
    System.out.println("strData.size() = " + strData.size());
    for (int i = 0; i < strData.size(); i++) {
      System.out.println(strData.get(i));
    }
  }


}
