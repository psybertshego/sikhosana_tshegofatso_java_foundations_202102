package com.psybergate.grad2021.core.collections.ce1a;

import java.util.Collection;
import java.util.Iterator;

public class ArrayStack implements Stack {

  private int size = 0;
  private Object[] objects = new Object[size];

  @Override
  public void push(Object o) {
    size++;
    objects = copyOfArray();
    objects[size - 1] = o;
  }

  private Object[] copyOfArray() {
    Object[] arr = new Object[size + 1];
    for (int i = 0; i < objects.length; i++) {
      arr[i] = objects[i];
    }
    return arr;
  }

  @Override
  public void pop() {
    size--;
    Object[] arr = new Object[size];
    for (int i = 0; i < arr.length; i++) {
      arr[i] = objects[i];
    }
    objects = arr;
  }

  @Override
  public Object get(int position) {
    return objects[position];
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size < 1;
  }

  @Override
  public boolean contains(Object o) {
    for (int i = 0; i < objects.length; i++) {
      if (o.equals(objects[i])) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator iterator() {
    return new ArrayStackIterator();
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean add(Object o) {
    for (int i = 0; i < objects.length; i++) {
      if (o.equals(objects[i])) {
        return true;
      }
    }
    push(o);
    return false;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public void clear() {
    size = 0;
    objects = new Object[size];
  }

  @Override
  public boolean equals(Object o) {
//    if (this == o) {
//      return true;
//    }
//    if (o == null) {
//      return false;
//    }
    return false;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    for (Object o : c) {
      if(!contains(o)){
        return false;
      }
    }
    return true;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }

  //How the iterator works essentially
//  private void anyName() {
//    Iterator iterator = this.iterator();
//    while (iterator.hasNext()) {
//      System.out.println(iterator().next());
//    }
//  }

  private class ArrayStackIterator implements Iterator {

    private int currentIndex = 0;

    @Override
    public boolean hasNext() {
      return size > currentIndex;
    }

    @Override
    public Object next() {
      return objects[currentIndex++];
    }
  }
}
