package com.psybergate.grad2021.core.collections.hw4a;

import java.util.*;

public class MyLinkedList implements List {

  private int size;
  private Node head;
  private Node currentNode;


  public MyLinkedList() {
    head = null;
    currentNode = null;
  }

  public Node getNextNode() {
    return head.getNextNode();
  }

  @Override
  public boolean add(Object o) {
    if (isEmpty()) {
      head = new Node(o);
      head.setNextNode(null);
      currentNode = head;
      return true;
    }

    Node tempNode = new Node(o);
    currentNode.setNextNode(tempNode);
    //current node points to next node
    currentNode = tempNode;
    return true;
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public boolean isEmpty() {
    return size() < 1 && head == null;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator iterator() {
    return new MyLinkedListIterator(this);
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean addAll(Collection c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public Object get(int index) {
    return this.getNode(index, head).getData();
  }

  @Override
  public Object set(int index, Object element) {
    Node node = head;

    node = getNode(index, node);
    return node.setData(element);
  }

  private Node getNode(int index, Node node) {
    if (index < 0 || index >= size()) {
      throw new IndexOutOfBoundsException();
    }
    for (int i = 0; i < index; i++) {
      node = node.getNextNode();
    }
    return node;
  }

  @Override
  public void add(int index, Object element) {

  }

  @Override
  public Object remove(int index) {
    return null;
  }

  @Override
  public int indexOf(Object o) {
    return 0;
  }

  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator listIterator() {
    return null;
  }

  @Override
  public ListIterator listIterator(int index) {
    return null;
  }

  @Override
  public List subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public boolean containsAll(Collection c) {
    return false;
  }

  @Override
  public Object[] toArray(Object[] a) {
    return new Object[0];
  }


  private class MyLinkedListIterator implements Iterator {

    private Node currentNode;

    public MyLinkedListIterator(MyLinkedList myLinkedList) {
      this.currentNode = myLinkedList.head;
    }

    @Override
    public boolean hasNext() {
      return !isEmpty() && this.currentNode != null;
    }

    @Override
    public Object next() {
      if(!hasNext()){
        throw new NoSuchElementException();
      }
      Object data = this.currentNode.getData();
      currentNode = currentNode.getNextNode();
      return data;
    }
  }
}


//List traversal
//    for(Node n = currentNode; n != null; n = n.getNextNode()){
//
//    }
