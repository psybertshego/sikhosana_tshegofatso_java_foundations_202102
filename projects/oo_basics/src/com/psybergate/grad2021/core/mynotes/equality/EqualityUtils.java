package com.psybergate.grad2021.core.mynotes.equality;

public class EqualityUtils {
    public static void main(String[] args) {
        Person person1 = new Person("Fin",21);
        Person person2 = new Person("Fin",21);
        Person person3 = new Person("Jake",45);
        Person person4 = person3;

        //Equality
        System.out.println("person1.equals(person2) = " + person1.equals(person2));
        System.out.println("person1.equals(person3) = " + person1.equals(person3));
        System.out.println("person3.equals(person4) = " + person3.equals(person4));

        //Same object reference
        System.out.println("person1.isTheSame(person2) = " + person1.isTheSame(person2));
        System.out.println("person3.isTheSame(person4) = " + person3.isTheSame(person4));
    }
}
