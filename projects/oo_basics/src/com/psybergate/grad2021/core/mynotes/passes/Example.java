package com.psybergate.grad2021.core.mynotes.passes;

public class Example {
    public static void main(String[] args) {
        int i = 10;
        Customer c = new Customer();
        c.setInterestRate(i);
        System.out.println("i = " + i);
        System.out.println("c.getInterestRate() = " + c.getInterestRate());
        i = 15;
        calcInterest(i, c);
        System.out.println("c.getInterestRate() = " + c.getInterestRate());

    }

    // Object state variables can be changed when passing an object into a method
    // this is known as pass by object reference.
    //

    public static void calcInterest(int i, Customer p) {
        p.setInterestRate(i);
        p = new Customer();
        i = 11;
        System.out.println("i = " + i);
        System.out.println("p.getInterestRate() = " + p.getInterestRate());
    }

}
