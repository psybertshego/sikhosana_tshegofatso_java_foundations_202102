package com.psybergate.grad2021.core.mynotes.equality;

import java.util.ArrayList;
import java.util.List;

public class Person {

    public static void main(String[] args) {
        Person person1 = new Person("Jay", 23);
        Person person2 = new Person("Jay", 21);
        System.out.println("person1.equals(person1) = " + person1.equals(person1));
        System.out.println("person1.equals(person1) = " + person1.equals(null));
        System.out.println("person1.equals(person1) = " + person1.equals("ads"));

        List list = new ArrayList();
        list.add(person1);
        System.out.println("list.contains(person1) = " + list.contains(person1));
        Integer i1 = new Integer(20);
        Integer i2 = new Integer(20);
        System.out.println("i1.equals(i2) = " + i1.equals(i2));
        System.out.println("i1.equals(i1) = " + i1.equals(i1));
        System.out.println("i1 == i2 => " + (i1 == i2));

        System.out.println("null instanceof Integer = " + (null instanceof Integer));
        Integer i3 = null;
    }


    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
//    public boolean equals(Object object) {
//        if (this == object) {
//            return true;
//        }
//        if (object == null) {
//            return false;
//        }
//        if (!(object instanceof Person)) {
//            return false;
//        }
//
//        Person person = (Person) object;
//        return this.name == person.name;
//    }


    public boolean equals(Object object) {
        if (this == object) {
            return false;
        }
        return true;
    }

    public boolean isTheSame(Person person) {
        return hashCode() == person.hashCode();
    }

    public boolean isIdentical(Person person) {
        return this == person;
    }
}
