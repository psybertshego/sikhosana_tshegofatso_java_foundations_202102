package com.psybergate.grad2021.core.mynotes.exceptions;

public class ParentException {

    private int age;
    private String name;
    private static final int MINIMUM_AGE = 18;

    public ParentException(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public boolean isAgeValid() throws Exception {
        if (age < MINIMUM_AGE) {
            throw new Exception("You aren't legal");
        }
        return true;
    }
}
