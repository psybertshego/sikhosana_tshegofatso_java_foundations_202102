package com.psybergate.grad2021.core.mynotes.objectlifecycle;

public class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
