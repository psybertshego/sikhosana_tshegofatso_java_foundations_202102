package com.psybergate.grad2021.core.mynotes.passes;

public class Customer {

    private int InterestRate = 0;

    public Customer() {
    }

    public void setInterestRate(int i) {
        this.InterestRate = i;
    }

    public int getInterestRate() {
        return InterestRate;
    }
}
