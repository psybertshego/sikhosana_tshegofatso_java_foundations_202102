package com.psybergate.grad2021.core.mynotes.exceptions;

public class ChildException extends ParentException {


    public static void main(String[] args) {
        ChildException childException = new ChildException(16,"Jake");
        System.out.println("childException.isLegal() = " + childException.isLegal());
    }

    private boolean isLegal;

    public ChildException(int age, String name) {

        super(age, name);
        try {
            if(isAgeValid()){
                this.isLegal = isAgeValid();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isLegal() {
        return isLegal;
    }

    @Override
    public boolean isAgeValid() throws Exception {
        return super.isAgeValid();
    }
}
