package com.psybergate.grad2021.core.mynotes.objectlifecycle;

public class Student extends Person{

    //if a class has no constructor, the default constructor is there, including super()
    private String studentNum;

//    public Student(){
//          super(studentNum,0);    // this line won't compile because studentNum doesn't
//                                  // exist yet inside the Student object because super() is ran first and
//                                  // it initializes the parent's attributes first. But in parent it calls Object super()
//                                  // because it doesn't inherit from another class
//
//    }


    public Student(String name, int age, String studentNum) {
        super(name, age);
        this.studentNum = studentNum;
    }
}
