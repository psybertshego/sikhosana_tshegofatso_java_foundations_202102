
package com.psybergate.grad2021.core.lambda.ce1a;

import java.util.function.Predicate;


public class Even implements Predicate {
  @Override
  public boolean test(Object o) {
    return ((Integer) o) % 2 == 0;
  }
}
