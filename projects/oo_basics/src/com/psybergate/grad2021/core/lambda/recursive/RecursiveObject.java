
package com.psybergate.grad2021.core.lambda.recursive;

public class RecursiveObject {

  private int num;
  private MyRecursive myRecursive;

  public RecursiveObject(int num,MyRecursive myRecursive) {
    this.num = num;
    this.myRecursive = myRecursive;
  }

  public void recurring(){
    //myRecursive.recursive(num);
  }
}
