
package com.psybergate.grad2021.core.lambda.ce1a;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamsClient {


  public static void main(String[] args){

    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    Stream<Integer> intStream = numbers.stream();

    intStream.filter((num) -> (num) % 2 == 0);

//    printStream(intStream);

    short a = 10;
    short b = 17;
    System.out.println(a+b);
  }


  private static  <T extends Integer>  void printStream(Stream<T> intStream){
    intStream.forEach((num) -> System.out.println(num));
  }
}
