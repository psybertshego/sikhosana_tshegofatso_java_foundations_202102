
package com.psybergate.grad2021.core.lambda.recursive;

public interface MyRecursive {
  public void recursive(int num, MyRecursive myRecursive);
}
