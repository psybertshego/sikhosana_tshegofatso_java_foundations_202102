
package com.psybergate.grad2021.core.lambda.recursive;

public class MyRecursiveClient {
  public static void main(String[] args) {

    MyRecursive myRecursive = new MyRecursive() {
      @Override
      public void recursive(int num, MyRecursive myRecursive) {
        if(num < 10){
          System.out.println("num " + num);
          num++;
          recursive(num,myRecursive);
        }
      }

//      @Override
//      public void recursive(int num) {
//        if(num < 10){
//          System.out.println("num " + num);
//          num++;
//          recursive(num);
//        }
//      }
    };

   // myRecursive.recursive(-10,myRecursive);

    MyRecursive myRecursive1 = (num,myRecursive2) -> {
      if(num < 10){
        System.out.println("num " + num);
        num++;
        myRecursive2.recursive(num,myRecursive2);
      }

      //myRecursive.recursive(num);
    };

    myRecursive1.recursive(2,myRecursive1);

//    RecursiveObject object = new RecursiveObject(0,num -> {
//      if(num < 10){
//        System.out.println("num " + num);
//        num++;
//        myRecursive.recursive(num);
//      }
//
//      //myRecursive.recursive(num);
//    });
//    myRecursive.recursive(0);
    //object.recurring();
  }
}
