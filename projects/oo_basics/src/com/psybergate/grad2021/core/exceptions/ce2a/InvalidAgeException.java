package com.psybergate.grad2021.core.exceptions.ce2a;

public class InvalidAgeException extends Throwable{

  public InvalidAgeException() {
    super("Invalid age entered");
  }
}
