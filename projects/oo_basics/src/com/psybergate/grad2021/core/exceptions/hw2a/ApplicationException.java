package com.psybergate.grad2021.core.exceptions.hw2a;

public class ApplicationException extends Exception{

  public ApplicationException() {
  }



  public ApplicationException(String message, Throwable cause) {
    super(message, cause);
  }
}
