package com.psybergate.grad2021.core.exceptions.hw1a;

public class NestedRuntimeException {
  public static void main(String[] args) {
    try {
      someMethod1();
    } catch (Throwable e) {
      e.printStackTrace();
    }
  }

  private static void someMethod1() {
    someMethod2();
  }

  private static void someMethod2() {
    someMethod3();
  }

  private static void someMethod3() {
    try {
      someMethod4();
    } catch (NullPointerException e) {
      e.printStackTrace();
    }
  }

  private static void someMethod4() throws NullPointerException {
    someMethod5(null);
//    someMethod6(null);
  }

  private static void someMethod5(String str) throws NullPointerException {
    if (str == null) {
      throw new NullPointerException();
    }
  }

  // someMethod5 can be rewritten like this
  private static void someMethod6(String str) {
    if (str == null) {
      throw new NullPointerException();
    }
  }
}
