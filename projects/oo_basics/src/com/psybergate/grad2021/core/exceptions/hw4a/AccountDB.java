package com.psybergate.grad2021.core.exceptions.hw4a;

import java.util.HashMap;

public class AccountDB {
  private HashMap<String, Account> accounts = new HashMap<>();

  public AccountDB() {
    seedDatabase();
  }

  private void seedDatabase() {
    accounts.put("001", new Account("001", 2000));
    accounts.put("002", new Account("001", 2000));
    accounts.put("003", new Account("001", 2000));
    accounts.put("004", new Account("001", 2000));
    accounts.put("005", new Account("001", 2000));
  }

  public void addAccount(String accountNum) {
    accounts.put(accountNum, new Account(accountNum, 0));
  }

  public void removeAccount(String accountNum) {
    accounts.remove(accountNum);
  }

  public HashMap<String, Account> getDatabaseInstance() {
    return accounts;
  }

}
