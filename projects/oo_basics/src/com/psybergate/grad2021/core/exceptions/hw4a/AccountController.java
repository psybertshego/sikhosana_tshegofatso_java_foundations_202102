package com.psybergate.grad2021.core.exceptions.hw4a;

public class AccountController {

  public static void createAccount(String accountNum) {
    //check if it exists
    //then add account
  }

  public static void removeAccount(String accountNum){
    //retrieve account
    //then delete
  }

  public static void depositToAccount(Account account, int amount) {
    //check if it exists
    
    //then deposit to account
    account.deposit(amount);
  }

  public static void withdrawFromAccount(Account account, int amount) {
    //check if it exists
    // then withdraw form account
    account.withdraw(amount);
  }

  public static void printAccountDetails(String accountNum) {
    //retrieve account
    //then print account details
  }
}
