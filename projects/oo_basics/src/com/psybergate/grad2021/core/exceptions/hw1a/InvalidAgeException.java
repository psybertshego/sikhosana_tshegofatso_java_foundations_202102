package com.psybergate.grad2021.core.exceptions.hw1a;

public class InvalidAgeException extends Exception{

  public InvalidAgeException() {
    super("Invalid age entered");
  }
}
