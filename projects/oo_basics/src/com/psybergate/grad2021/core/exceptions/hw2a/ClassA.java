package com.psybergate.grad2021.core.exceptions.hw2a;

import java.sql.SQLException;

public class ClassA {
  //I can write main like this and it will still print the stacktrace
//  public static void main(String[] args) throws ApplicationException{
//    methodA();
//  }

  public static void main(String[] args) {
    try {
      methodA();
    } catch (ApplicationException e) {
      e.printStackTrace();
    }
  }

  public static void methodA() throws ApplicationException {
    try {
      methodB();
    } catch (SQLException e) {
      throw new ApplicationException("Something went wrong in the database", e);
    }
  }

  public static void methodB() throws SQLException {
    //failed database connection
    throw new SQLException("Database connection failed");
  }
}
