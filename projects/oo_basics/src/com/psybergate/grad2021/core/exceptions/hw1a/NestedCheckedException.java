package com.psybergate.grad2021.core.exceptions.hw1a;

public class NestedCheckedException {
  public static void main(String[] args) {
    someMethod2();
  }

  private static void someMethod1() {
    someMethod3();
  }

  private static void someMethod2() {
    someMethod1();
  }

  private static void someMethod3() {
    try {
      someMethod4();
    } catch (InvalidAgeException e) {
      e.printStackTrace();
    }
  }

  private static void someMethod4() throws InvalidAgeException {
    someMethod5(16);
  }

  private static void someMethod5(int age) throws InvalidAgeException {
    if (age < 18) {
      throw new InvalidAgeException();
    }
  }
}
