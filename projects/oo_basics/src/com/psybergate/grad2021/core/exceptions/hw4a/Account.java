package com.psybergate.grad2021.core.exceptions.hw4a;

public class Account {

  private String accountNumber;
  private double balance;

  public Account(String accountNumber, int balance) {
    this.accountNumber = accountNumber;
    this.balance = balance;
  }

  public void withdraw(double amount) {
    balance -= amount;
  }

  public void deposit(double amount) {
    balance += amount;
  }

  public double getBalance() {
    return this.balance;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void printDetails() {
    System.out.println("accountNumber " + accountNumber+ ", balance=" + balance);
  }
}
