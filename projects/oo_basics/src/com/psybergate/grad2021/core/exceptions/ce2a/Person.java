package com.psybergate.grad2021.core.exceptions.ce2a;

import java.time.LocalDate;

public class Person {

  private final String name;
  private final String dateOfBirth;
  private int age;

  public Person(String name, String dateOfBirth) {
    this.name = name;
    this.dateOfBirth = dateOfBirth;
    try {
      setAge();
    } catch (InvalidAgeException e) {
      e.printStackTrace();
      System.out.println("Person must be over the age of 18.");
    }

  }

  private void setAge() throws InvalidAgeException {
    if (isOver18(dateOfBirth)) {
      this.age = LocalDate.now().getYear() - Integer.parseInt(dateOfBirth.split("/")[0]);
    } else {
      throw new InvalidAgeException();
    }
  }

  private void setName(String name){

  }

  private boolean isOver18(String dateOfBirth) {
    return Integer.parseInt(dateOfBirth.split("/")[0]) > 18;
  }

}
