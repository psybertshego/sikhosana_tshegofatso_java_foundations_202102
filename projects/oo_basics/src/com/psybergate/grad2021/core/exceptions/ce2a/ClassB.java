package com.psybergate.grad2021.core.exceptions.ce2a;

public class ClassB {

  public static void myCheckedExceptionMethod() throws CheckedException {
    throw new CheckedException();
    // since it's of type checked exception, it needs to be handled by
    // either propagating it to the calling method or handle the exception in it's body
  }

  public static void myRuntimeExceptionMethod() {
    throw new RuntimeException();
    // since it's of type runtime exception, it needs not to be handled.
  }

  public static void myErrorExceptionMethod() {
    throw new InvalidNameError();
    // since it's of type runtime exception, it needs not to be handled.
  }

  public static void myThrowableExceptionMethod() throws InvalidAgeException {
    throw new InvalidAgeException();
    // since it's a checked exception, it needs to be handled by
    // either propagating it to the calling method or handle the exception in it's body
  }
}
