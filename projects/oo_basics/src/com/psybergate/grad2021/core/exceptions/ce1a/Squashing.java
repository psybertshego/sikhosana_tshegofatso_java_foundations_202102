package com.psybergate.grad2021.core.exceptions.ce1a;

public class Squashing {
  public static void main(String[] args) {

    //In this method the exception is handled in the method body
    exceptionMethod();

    //In this method the exception is propagated to main and handled here
    try {
      throwableExceptionMethod();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
  }

  public static void exceptionMethod() {
    try {
      throw new Throwable();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
  }

  public static void throwableExceptionMethod() throws Throwable {
    throw new Throwable();
  }
}
