package com.psybergate.grad2021.core.exceptions.ce2a;

public class ExceptionsUtils {
  public static void main(String[] args) {

  checkedException("rand");

  Person person = new Person("Jay","1998/02/04");

  }


  public static void checkedException(String str) {
    try {
      throw new Throwable(); // Checked exception
    } catch (Throwable throwable) {
      System.out.println("The exception has been caught, for " + str);
      throwable.printStackTrace();
    }
  }

  public static void propagatedCheckedException() throws Throwable {
    throw new Throwable();
  }

  public static void runtimeException(String str) {
    if (str == null) {
      throw new NullPointerException(); //runtime exception
    } else {
      System.out.println("In runtime Exception Method -> " + str);
    }
  }
}
