package com.psybergate.grad2021.core.oopart2.homework.hw3a;

public class Customer {
    private String customerNum;
    private String customerAddress;

    public Customer(String customerNum, String customerAddress) {
        this.customerNum = customerNum;
        this.customerAddress = customerAddress;
    }

    public boolean isIdentical(Customer customer) {
        return this == customer;
    }

    public boolean equals(Object o) {
        if(this == o) return true;
        if(this == null) return false;
        if(o instanceof Customer){
            Customer customer = (Customer) o;
            return this.customerNum.equals(customer.getCustomerNum());
        }
        return false;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public String getCustomerNum() {
        return customerNum;
    }
}
