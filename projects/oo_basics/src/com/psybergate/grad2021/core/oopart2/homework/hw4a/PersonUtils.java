package com.psybergate.grad2021.core.oopart2.homework.hw4a;

public class PersonUtils {
    public static void main(String[] args) {
        Student student = new Student("Chad","1998/02/04",1525976);

        // Shows that static methods are inherited
        System.out.println("student.getAge() = " + student.getAge());
        System.out.println("student.getDateOfBirth() = " + student.getDateOfBirth());
//        Student.calculateAge();

        //Shows that static methods cannot be invoked polymorphically
//        System.out.println("Person.getAlias() = " + Person.getAlias());
//        System.out.println("Student.getAlias() = " + Student.getAlias());


        //Shows that static methods cannot be invoked polymorphically (Better version especially line 20)
        Person p1 = new Student("Fin","1988/03/04",12);
        Person p2 = null;
        System.out.println("p1.getAlias() = " + p1.getAlias());
        System.out.println("p1.getAlias() = " + Person.getAlias());
    }
}
