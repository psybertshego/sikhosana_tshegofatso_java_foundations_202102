package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money3 {
    private Currency currency;      ///currency is currently mutable
    private final BigDecimal value;


    public static void main(String[] args) {
        String str = "";
        Money3 money1 = new Money3(10);
        Money3 money2 = new Money3(10);
        money1.add(money2);
    }

    public Money3(String amount) {
        this(new BigDecimal(amount));
    }

    public Money3(BigDecimal amount) {
        this.value = amount;
    }


    public Money3(int amount) {
        this.value = new BigDecimal(amount);
    }

    public Money3(double amount) {
        this(String.valueOf(amount));
    }

    public Money3 add(Money3 money) {
        return new Money3(value.add(money.value));
    }

}

