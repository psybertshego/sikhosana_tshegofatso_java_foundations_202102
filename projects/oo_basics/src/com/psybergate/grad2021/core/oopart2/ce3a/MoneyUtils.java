package com.psybergate.grad2021.core.oopart2.ce3a;

public class MoneyUtils {
    public static void main(String[] args) {
        Money1 cash = new Money1(20);
        Money1 digital = new Money1(324.2);

        System.out.println("cash.sumMoney(23) = " + cash.sumMoney(23));
        System.out.println("digital.sumMoney(300.5) = " + digital.sumMoney(300.3));
    }
}
