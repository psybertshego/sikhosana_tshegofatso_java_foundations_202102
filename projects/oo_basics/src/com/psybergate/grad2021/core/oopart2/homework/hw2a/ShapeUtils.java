package com.psybergate.grad2021.core.oopart2.homework.hw2a;

public class ShapeUtils {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(200, 50);

        Rectangle rect2 = new Rectangle(100, 25);
        Rectangle rect3 = new Rectangle(130, 13);
        Rectangle rect4 = rect2;
        Rectangle rect5 = rect3;

        System.out.println("rectangle.equals(rect3) = " + rectangle.equals(rect3));
        System.out.println("rect3.equals(rect5) = " + rect3.equals(rect5));
        System.out.println("rect2.isIdentical(rect4) = " + rect2.isIdentical(rect4));

//        System.out.println("rectangle.calArea() = " + rectangle.calArea());
//        System.out.println("rect3.calPerimeter() = " + rect3.calPerimeter());
//        System.out.println("rect2.calArea() = " + rect2.calArea());

    }
}
