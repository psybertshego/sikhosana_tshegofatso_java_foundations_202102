package com.psybergate.grad2021.core.oopart2.homework.hw5a2;

public class LocalOrder extends Order {

    public LocalOrder(String orderNum) {
        super(orderNum);
    }

    public double getOrderTotal() {
        double total = 0;
        for (Product orderItem : getOrderItems()) {
            total += orderItem.getTotal();
        }
        return total;
    }

}
