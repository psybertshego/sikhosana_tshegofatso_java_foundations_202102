package com.psybergate.grad2021.core.oopart2.homework.hw4a;

import java.time.LocalDate;
import java.util.Date;

public class Person {

    private String name;
    private String dateOfBirth;

    public Person(String name, String dateOfBirth) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public static int calculateAge(String dateOfBirth){
        return LocalDate.now().getYear() - Integer.parseInt(dateOfBirth.substring(0,4));
    }

    public static String getAlias(){
        return "Anonymous";
    }
}
