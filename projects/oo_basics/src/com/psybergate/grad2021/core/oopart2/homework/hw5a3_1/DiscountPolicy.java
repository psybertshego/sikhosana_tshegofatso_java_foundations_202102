package com.psybergate.grad2021.core.oopart2.homework.hw5a3_1;

public class DiscountPolicy {

  private int policy;
  private int yearsWithUs;

  public DiscountPolicy(int policy, int yearsWithUs) {
    this.policy = policy;
    this.yearsWithUs = yearsWithUs;
  }

  public double calculateDiscountAmount(double total) {
    if (policy == 1) {
      return yearBasedDiscount(total);
    } else if (policy == 2) {
      return totalBasedDiscount(total);
    }
    return 0;
  }

  private double yearBasedDiscount(double total) {
    if (yearsWithUs > 2 && yearsWithUs <= 5) {
      return total*0.075;
    } else if (yearsWithUs > 5) {
      return total * 0.125;
    }
    return 0;
  }

  private double totalBasedDiscount(double total) {
    if (total > 500_000 && total < 1_000_000) {
      return total * 0.05;
    } else if (total > 1_000_000) {
      return total * 0.1;
    }
    return 0;
  }

  // If it were a static method
//  public static double calculateDiscount(int policy, Customer customer, Order order) {
//    if (policy == 1) {    //year based
//      if (customer.getYearsWithUs() > 2 && customer.getYearsWithUs() <= 5) {
//        return  0.075;
//      } else if (customer.getYearsWithUs() > 5) {
//        return 0.125;
//      }
//      return 0;
//    }
//    else if(policy ==2){  //total based
//      if (order > 500_000 && order < 1_000_000) {
//        return order * (1 + 0.05);
//      } else if (order > 1_000_000) {
//        return order * (1 + 0.1);
//      }
//      return order;
//    }
//
//    return 0;
//  }

}
