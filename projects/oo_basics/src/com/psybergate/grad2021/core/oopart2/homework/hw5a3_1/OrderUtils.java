package com.psybergate.grad2021.core.oopart2.homework.hw5a3_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class OrderUtils {

  private static List<Product> products = new ArrayList<>();

  public static void main(String[] args) {
    products = initializeProductList();

    // Local is lekker
    Customer customer1 = new Customer("001", 22, "2016/02/04", false);
    Customer customer2 = new Customer("002", 24, "2020/02/04", false);

    // Ama-International
    Customer customer3 = new Customer("003", 20, "2010/02/04", true);
    Customer customer4 = new Customer("004", 23, "2012/02/04", true);


    LocalOrder localOrder = new LocalOrder("012555", customer1, 1);
    LocalOrder localOrder1 = new LocalOrder("0127767", customer1, 2);
    LocalOrder localOrder2 = new LocalOrder("0110543", customer1, 1);
    LocalOrder localOrder3 = new LocalOrder("0114535", customer2, 2);
    LocalOrder localOrder4 = new LocalOrder("012546", customer2, 3);

    localOrder.addOrderItem(generateOrderItem());
    localOrder.addOrderItem(generateOrderItem());
    localOrder.addOrderItem(generateOrderItem());

    localOrder1.addOrderItem(generateOrderItem());
    localOrder1.addOrderItem(generateOrderItem());
    localOrder1.addOrderItem(generateOrderItem());

    localOrder2.addOrderItem(generateOrderItem());
    localOrder2.addOrderItem(generateOrderItem());
    localOrder2.addOrderItem(generateOrderItem());

    localOrder3.addOrderItem(generateOrderItem());
    localOrder3.addOrderItem(generateOrderItem());
    localOrder4.addOrderItem(generateOrderItem());

    InternationalOrder internationalOrder = new InternationalOrder("220323", customer3, 2);
    InternationalOrder internationalOrder1 = new InternationalOrder("220354", customer3, 1);
    InternationalOrder internationalOrder2 = new InternationalOrder("220964", customer4, 2);
    InternationalOrder internationalOrder3 = new InternationalOrder("220332", customer4, 1);
    InternationalOrder internationalOrder4 = new InternationalOrder("220431", customer4, 2);

    internationalOrder.addOrderItem(generateOrderItem());
    internationalOrder.addOrderItem(generateOrderItem());
    internationalOrder.addOrderItem(generateOrderItem());

    internationalOrder1.addOrderItem(generateOrderItem());
    internationalOrder1.addOrderItem(generateOrderItem());

    internationalOrder2.addOrderItem(generateOrderItem());
    internationalOrder2.addOrderItem(generateOrderItem());

    internationalOrder3.addOrderItem(generateOrderItem());
    internationalOrder3.addOrderItem(generateOrderItem());
    internationalOrder3.addOrderItem(generateOrderItem());

    internationalOrder4.addOrderItem(generateOrderItem());

    List<Order> orders = new ArrayList<>();

    orders.add(localOrder);
    orders.add(localOrder1);
    orders.add(localOrder2);
    orders.add(localOrder3);
    orders.add(localOrder4);
    orders.add(internationalOrder);
    orders.add(internationalOrder1);
    orders.add(internationalOrder2);
    orders.add(internationalOrder3);
    orders.add(internationalOrder4);

    printOrders(orders);
  }

  private static OrderItem generateOrderItem() {
    return new OrderItem(products.get(new Random().nextInt(17)), new Random().nextInt(3) + 1);
  }

  private static void printOrders(List<Order> orders) {
    for (Order order : orders) {
      order.printOrdersItems();
    }
  }

  private static List<Product> initializeProductList() {
    // 17 Products
    List<Product> productList = new ArrayList<>();
    productList.add(new Product("Bicycle", 700));
    productList.add(new Product("Skateboard", 500));
    productList.add(new Product("Hoverboard", 1000));
    productList.add(new Product("Scooter", 800));
    productList.add(new Product("Couch", 1000));
    productList.add(new Product("Chair", 4000));
    productList.add(new Product("Table", 6000));
    productList.add(new Product("Laptop", 20000));
    productList.add(new Product("Wireless Mouse", 200));
    productList.add(new Product("Wireless Keyboard", 300));
    productList.add(new Product("Wine", 1500));
    productList.add(new Product("Roses", 500));
    productList.add(new Product("Chocolate", 300));
    productList.add(new Product("Lemon", 30));
    productList.add(new Product("Orange", 20));
    productList.add(new Product("Stapler", 100));
    productList.add(new Product("Pencil", 5));
    productList.add(new Product("Notebook", 20));
    return productList;
  }
}
