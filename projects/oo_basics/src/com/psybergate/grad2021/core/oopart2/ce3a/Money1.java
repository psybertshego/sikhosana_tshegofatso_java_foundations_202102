package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money1 {
    private int intAmount;
    private double dblAmount;
    private BigDecimal bigDecimal;


    public static void main(String[] args) {
        String str = "";
        Money1 money1 = new Money1(10);
    }
    public Money1(int intAmount) {
        this.intAmount = intAmount;
        this.bigDecimal = new BigDecimal(intAmount);
    }

    public Money1(double dblAmount) {
        this.dblAmount = dblAmount;
        this.bigDecimal = new BigDecimal(dblAmount);
    }

    public BigDecimal sumMoney(int amount) {
        return new BigDecimal(intAmount + amount);
//       return bigDecimal.add(new BigDecimal(amount));
    }


    public BigDecimal sumMoney(double amount) {
        return bigDecimal.add(new BigDecimal(amount));
    }
}

