package com.psybergate.grad2021.core.oopart2.homework.hw5a2;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
    public static void main(String[] args) {
        // Local is lekker
        Customer customer1 = new Customer("001  ", 22, false);
        Customer customer2 = new Customer("002", 19, false);

        // Ama-International
        Customer customer3 = new Customer("003", 20, true);
        Customer customer4 = new Customer("004", 25, true);

        LocalOrder localOrder = new LocalOrder("012555");
        addListOfProducts(localOrder, new ArrayList<Product>() {{
            add(new Product("Bicycle", 1, 700));
            add(new Product("Skateboard", 2, 500));
        }});

        LocalOrder localOrder1 = new LocalOrder("0127767");
        localOrder1.addOrderItem(new Product("Hoverboard", 1, 1000));

        LocalOrder localOrder4 = new LocalOrder("012546");
        localOrder4.addOrderItem(new Product("Scooter", 1, 800));
        addCustomerOrders(customer1, new ArrayList<Order>() {{
            add(localOrder);
            add(localOrder1);
            add(localOrder4);
        }});

        LocalOrder localOrder2 = new LocalOrder("0110543");
        localOrder2.addOrderItem(new Product("Laptop", 1, 20000));
        LocalOrder localOrder3 = new LocalOrder("0114535");
        addListOfProducts(localOrder3, new ArrayList<Product>() {{
            add(new Product("Wireless Mouse", 1, 200));
            add(new Product("Wireless Keyboard", 1, 300));
        }});

        addCustomerOrders(customer2, new ArrayList<Order>() {{
            add(localOrder2);
            add(localOrder3);
        }});

        InternationalOrder internationalOrder = new InternationalOrder("220323");
        addListOfProducts(internationalOrder, new ArrayList<Product>() {{
            add(new Product("Wine", 2, 1500));
            add(new Product("Roses", 1, 500));
            add(new Product("Chocolate", 3, 300));
        }});


        InternationalOrder internationalOrder1 = new InternationalOrder("220354");
        addListOfProducts(internationalOrder1, new ArrayList<Product>() {{
            add(new Product("Lemon", 20, 30));
            add(new Product("Orange", 50, 20));
        }});

        addCustomerOrders(customer3, new ArrayList<Order>() {{
            add(internationalOrder);
            add(internationalOrder1);
        }});

        InternationalOrder internationalOrder2 = new InternationalOrder("220964");
        addListOfProducts(internationalOrder2, new ArrayList<Product>() {{
            add(new Product("Chair", 3, 4000));
            add(new Product("Table", 3, 6000));
        }});

        InternationalOrder internationalOrder3 = new InternationalOrder("220332");
        internationalOrder3.addOrderItem(new Product("Couch", 2, 1000));

        InternationalOrder internationalOrder4 = new InternationalOrder("220431");
        addListOfProducts(internationalOrder4, new ArrayList<Product>() {{
            add(new Product("Stapler", 5, 100));
            add(new Product("Pencil", 100, 5));
            add(new Product("Notebook", 5, 20));
        }});

        addCustomerOrders(customer4, new ArrayList<Order>() {{
            add(internationalOrder2);
            add(internationalOrder3);
            add(internationalOrder4);
        }});

        customer1.printOrders();
//        customer2.printOrders();
//        customer3.printOrders();
//        customer4.printOrders();
    }

    private static void addCustomerOrders(Customer customer, List<Order> orderArrayList) {
        for (Order order : orderArrayList) {
            customer.addOrder(order);
        }
    }

    private static void addListOfProducts(Order order, List<Product> products) {
        for (Product product : products) {
            order.addOrderItem(product);
        }
    }
}
