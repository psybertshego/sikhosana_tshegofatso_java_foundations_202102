package com.psybergate.grad2021.core.oopart2.homework.hw5a2_1;

import java.util.ArrayList;
import java.util.List;


/**
 * work on validation and abstraction of Policies
 */

public abstract class Order {
    private String orderNum;
    private List<OrderItem> orderItems;
    protected Customer customer;

    public Order(String orderNum, Customer customer) {
        this.orderNum = orderNum;
        this.customer = customer;
        this.orderItems = new ArrayList<>();
    }

    public void printOrdersItems() {
        System.out.println("\n*********************\n Customer : " + getCustomerNum());
        System.out.println("==> Order " + orderNum + ":");
        for (OrderItem orderItem : orderItems) {
            orderItem.printOrderDetails();
        }
        System.out.println("Order total = R" + getOrderTotal());
    }

    public String getCustomerNum() {
        return customer.getCustomerNum();
    }

    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public abstract double getOrderTotal();

}
