package com.psybergate.grad2021.core.oopart2.demo1a;

public class Square extends Rectangle{

    private double size;

    public Square(double size) {
        super(size, size);
        this.size = size;
    }

//    @Override
//    public Rectangle(double length,double width){
//        super(length,width);
//    }
}
