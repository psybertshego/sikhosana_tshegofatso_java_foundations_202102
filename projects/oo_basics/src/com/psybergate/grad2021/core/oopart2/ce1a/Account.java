package com.psybergate.grad2021.core.oopart2.ce1a;

public abstract class Account {

    private int accountNum;
    private int balance;
    private String bankName;
//
//    public Account(int accountNum){
//        this.accountNum = accountNum;
//    }

//    public Account(){
//        System.out.println("I am in Account default");
//    }

    public Account(int accountNum, int balance,String bankName) {
        this.accountNum = accountNum;
        this.balance = balance;
        this.bankName = bankName;
        System.out.println("I am in Account");
    }

    public int getBalance() {
        return balance;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public abstract boolean isOverDrawn();

    public abstract boolean needsToBeReviewed();


}
