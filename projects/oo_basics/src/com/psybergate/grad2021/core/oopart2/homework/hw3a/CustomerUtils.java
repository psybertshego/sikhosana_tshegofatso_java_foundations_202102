package com.psybergate.grad2021.core.oopart2.homework.hw3a;

public class CustomerUtils {
    public static void main(String[] args) {
        Person person = new Person("001", "The Moon", "Tshego", 23);
        Company company = new Company("002", "The Sun and Beyond", "A0023", 2010);
        Company company1 = company;
        Customer genericCustomer = new Customer("003", "Generic world");

        System.out.println("company.equals(person) = " + company.equals(person));
        System.out.println("genericCustomer.equals(person) = " + genericCustomer.equals(person));
        System.out.println("company.equals(company1) = " + company.equals(company1));
        System.out.println("company.isIdentical(company1) = " + company.isIdentical(company1));
    }

    private static void isCustomerInstance(Object object) {
        if (object instanceof Customer) {
            System.out.println("Is an instance of Customer");
        }
    }
}
