package com.psybergate.grad2021.core.oopart2.homework.hw5a1_1;

public class OrderItem {

    private Product product;
    private int quantity;

    public OrderItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public double getTotal() {
        return quantity * product.getPrice();
    }

    public void printOrderDetails() {
        System.out.println("    Product details: " + product.getProductName());
        System.out.println("    Quantity = " + quantity + " ( R" + getTotal() + " )");
    }
}
