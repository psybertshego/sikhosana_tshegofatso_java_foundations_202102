package com.psybergate.grad2021.core.oopart2.ce3a;

public class Currency {
    private String currencyCode;

    public Currency(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
}
