package com.psybergate.grad2021.core.oopart2.homework.hw5a3_1;

import java.util.List;

public class InternationalOrder extends Order {
  private final double importDuties = 0.05; //calculated at 5% for all international orders

  public InternationalOrder(String orderNum, Customer customer, int policy) {
    super("I" + orderNum, customer, policy);
  }

  public double getOrderTotal() {
    return super.getOrderTotal() * (1 + importDuties);
  }

}
