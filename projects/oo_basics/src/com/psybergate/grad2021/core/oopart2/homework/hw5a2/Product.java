package com.psybergate.grad2021.core.oopart2.homework.hw5a2;

public class Product {
    private String productName;
    private int quantity;
    private double price;

    public Product(String productName, int quantity, double price) {
        this.productName = productName;
        this.quantity = quantity;
        this.price = price;
    }

    public double getTotal() {
        return quantity * price;
    }

    public void printOrderDetails() {
        System.out.println("    Product details: " + productName);
        System.out.println("    Quantity = " + quantity + " ( R"+ getTotal() + " )" );
    }
}
