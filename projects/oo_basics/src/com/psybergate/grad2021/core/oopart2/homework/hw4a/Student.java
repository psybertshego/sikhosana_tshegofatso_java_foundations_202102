package com.psybergate.grad2021.core.oopart2.homework.hw4a;

public class Student extends Person{

    private int studentNum;

    public Student(String name, String dateOfBirth, int studentNum) {
        super(name, dateOfBirth);
        this.studentNum = studentNum;
    }



    //Demonstrates that static methods are inherited
    public int getAge(){
        return calculateAge(getDateOfBirth());
    }

    public static String getAlias(){
        return "Jeff";
    }
}
