package com.psybergate.grad2021.core.oopart2.homework.hw5a3_1;


public class LocalOrder extends Order {
  //private final double discount = 0.1;  // 10% discount

  public LocalOrder(String orderNum, Customer customer, int policy) {
    super("L" + orderNum, customer, policy);
  }
}
