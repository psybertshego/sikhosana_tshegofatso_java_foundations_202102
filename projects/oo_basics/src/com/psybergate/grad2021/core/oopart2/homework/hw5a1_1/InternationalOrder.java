package com.psybergate.grad2021.core.oopart2.homework.hw5a1_1;

public class InternationalOrder extends Order {
    private final double importDuties = 0.2; //calculated at 20% for all the orders

    public InternationalOrder(String orderNum, Customer customer) {
        super(orderNum, customer);
    }

    @Override
    public double getOrderTotal() {
        double total = 0;
        for (OrderItem orderItem : getOrderItems()) {
            total += orderItem.getTotal();
        }
        return total * (1 + importDuties);
    }

}
