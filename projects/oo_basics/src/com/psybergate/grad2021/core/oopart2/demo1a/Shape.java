package com.psybergate.grad2021.core.oopart2.demo1a;

public abstract class Shape {

    public abstract double calculateArea();

    public abstract double calculatePerimeter();

    public static double calculateRectangleArea(double length,double width){
        return 0;
    }

    private String getShapeType(){
        return "Issa Square";
    }
}
