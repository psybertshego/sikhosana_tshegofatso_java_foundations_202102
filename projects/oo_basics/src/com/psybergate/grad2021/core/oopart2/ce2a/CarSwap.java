package com.psybergate.grad2021.core.oopart2.ce2a;

public class CarSwap {
    public static void main(String[] args) {
        Car car1 = new Car("Ferrari",420);
        Car car2 = new Car("Tesla",210);

        swap(car1,car2);

        System.out.println("car1.getCarDetails() = " + car1.getCarDetails());
        System.out.println("car2.getCarDetails() = " + car2.getCarDetails());
    }


    // My swap method doesn't work :/
    public static void swap(Car a, Car b){
        Car tempCar = a;
        //a.setName("Jaguar");
        a = b;
        b = tempCar;
//        System.out.println("a.getCarDetails() = " + a.getCarDetails());
//        System.out.println("b.getCarDetails() = " + b.getCarDetails() + "\n");
    }



    // Based on my results the objects I pass into swap() are not changed,
    // and this means that the object references are being passed on by value
    // but I can change the object state of these objects that I have passed into the swap() method
    // because each variable
}
