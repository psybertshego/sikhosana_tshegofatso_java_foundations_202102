package com.psybergate.grad2021.core.oopart2.demo1a;

public class Rectangle extends Shape {

    private double length;
    private double width;
    private final String SHAPE_TYPE = "Rectangle";

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public double calculateArea() {
        return length * width;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (length * width);
    }

    public static double calculateRectangleArea(double length,double width){
        return length*width;
    }

    public String getShapeType() {
        return SHAPE_TYPE;
    }
}
