package com.psybergate.grad2021.core.oopart2.homework.hw5a1_1;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private boolean isInternational;
    private String customerNum;
    private int age;

    public Customer(String customerNum, int age, boolean isInternational) {
        this.isInternational = isInternational;
        this.customerNum = customerNum;
        this.age = age;
    }

    public boolean isInternational() {
        return isInternational;
    }

    public String getCustomerNum() {
        return customerNum;
    }
}
