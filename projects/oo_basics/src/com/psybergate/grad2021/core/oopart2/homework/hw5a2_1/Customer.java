package com.psybergate.grad2021.core.oopart2.homework.hw5a2_1;

import java.time.LocalDate;

public class Customer {
  private boolean isInternational;
  private String customerNum;
  private int age;
  private String signUpDate;

  public Customer(String customerNum, int age, String signUpDate, boolean isInternational) {
    this.isInternational = isInternational;
    this.customerNum = customerNum;
    this.age = age;
    this.signUpDate = signUpDate;
  }

  public int getYearsWithUs() {
    return LocalDate.now().getYear() - Integer.parseInt(signUpDate.split("/")[0]);
  }

  public boolean isInternational() {
    return isInternational;
  }

  public String getCustomerNum() {
    return customerNum;
  }
}
