package com.psybergate.grad2021.core.oopart2.homework.hw5a1_1;

import com.sun.xml.internal.bind.v2.TODO;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {
    private String orderNum;
    private List<OrderItem> orderItems;
    private Customer customer;

    public Order(String orderNum, Customer customer) {
        this.orderNum = orderNum;
        this.customer = customer;
        this.orderItems = new ArrayList<>();
    }

    public void printOrdersItems() {
        System.out.println("\n*********************\n Customer : " + getCustomerNum());
        System.out.println("==> Order " + orderNum + ":");
        for (OrderItem orderItem : orderItems) {
            orderItem.printOrderDetails();
        }
        System.out.println("Order total = R" + getOrderTotal());
    }

    public String getCustomerNum() {
        return customer.getCustomerNum();
    }

    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public abstract double getOrderTotal();

}
