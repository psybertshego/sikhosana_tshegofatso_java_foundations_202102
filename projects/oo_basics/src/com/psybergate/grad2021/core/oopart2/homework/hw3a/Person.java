package com.psybergate.grad2021.core.oopart2.homework.hw3a;

public class Person extends Customer {
    private static final int MIN_AGE = 18;

    private String name;
    private int age;

    public Person(String customerNum, String customerAddress, String name, int age) {
        super(customerNum, customerAddress);
        isUnderAge();
        this.age = age;
        this.name = name;

    }

    public String getName() {
        return name;
    }

    private void isUnderAge() {
        if (age > MIN_AGE) {
            //throw some exception
            throw new RuntimeException("Person has to be over" + MIN_AGE);
        }
    }
}

