package com.psybergate.grad2021.core.oopart2.homework.hw6a;

public class Product {
    private final String productName;
    private final double price;

    public Product(String productName, double price) {
        this.productName = productName;
        this.price = price;
    }

    public double getTotal() {
        return price;
    }

    public String getProductDetails(){
        return productName + " => R"+ price + " ";
    }

    public void printOrderDetails() {
        System.out.println("    Product details: " + productName + " ( R"+ getTotal() + " )" );
    }
}
