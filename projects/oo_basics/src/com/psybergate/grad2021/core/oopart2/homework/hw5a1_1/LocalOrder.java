package com.psybergate.grad2021.core.oopart2.homework.hw5a1_1;

public class LocalOrder extends Order {
    private final double discount = 0.1;  // 10% discount

    public LocalOrder(String orderNum, Customer customer) {
        super(orderNum, customer);
    }

    public double getOrderTotal() {
        double total = 0;
        for (OrderItem orderItem : getOrderItems()) {
            total += orderItem.getTotal();
        }
        return total - (total * discount);
    }

}
