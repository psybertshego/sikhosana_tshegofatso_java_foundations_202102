package com.psybergate.grad2021.core.oopart2.homework.hw6a;

import java.util.*;

public class OrderUtils {
    private static List<Customer> customerList = new ArrayList<>();
    private static List<Product> productList = new ArrayList<>();
    private static int customerNumber = 1;

    public static void main(String[] args) {

        initializeProducts();
        boolean systemOn = true;
        System.out.println("😁 Welcome to the MegaMini Order System 😁 \n");
        Scanner scanner = new Scanner(System.in);

        while (systemOn) {
            Customer customer = createCustomer(scanner);
            addCustomerToList(customer);
            addCustomerOrders(customer, scanner);
            System.out.println("Add a new customer? (y/n)");
            if (scanner.next().equals("y")) {
                customerNumber += 1;
                continue;
            } else {
                printCustomers(customerList);
                systemOn = false;
            }
        }
        scanner.close();
    }

    private static Customer createCustomer(Scanner scanner) {
        System.out.println("Enter the customer name:");
        String customerName = scanner.next();
        System.out.println("Is customer international? (y/n)");
        boolean isInternational = scanner.next().equals("y");
        System.out.println("How many years has this customer been with us?");
        int years = scanner.nextInt();

        return new Customer("C" + customerNumber, customerName, years, isInternational);
    }

    private static void addCustomerToList(Customer customer) {
        customerList.add(customer);
    }

    private static void addCustomerOrders(Customer customer, Scanner scanner) {
        boolean isOrdering = true;
        while (isOrdering) {
            printListOfProducts();
            Order order = createOrder(customer.isInternational());
            System.out.println("What would you like to order today? (to select multiple split products by comma)");
            addListOfProducts(order, scanner);
            customer.addOrder(order);
            System.out.println("----> Order was added successfully 😃 <----\n");
            System.out.println("Like to add another order? (y/n)");
            if (scanner.next().equals("y")) {
                continue;
            } else {
                isOrdering = false;
            }
        }
        System.out.println("----> Customer orders are complete <----\n");
    }

    private static Order createOrder(boolean isInternational) {
        if (isInternational) {
            return new InternationalOrder("I" + new Random().nextInt(100));
        }
        return new LocalOrder("L" + new Random().nextInt(100));
    }

    private static void addListOfProducts(Order order, Scanner scanner) {
        String[] products = scanner.next().split(",");
        for (int i = 0; i < products.length; i++) {
            int productNumber = Integer.parseInt(products[i]);
            order.addOrderItem(productList.get(productNumber));
        }
    }

    private static void initializeProducts() {
        productList.add(new Product("Bicycle", 700));
        productList.add(new Product("Skateboard", 600));
        productList.add(new Product("Hoverboard", 1000));
        productList.add(new Product("Scooter", 800));
        productList.add(new Product("Tesla Model S", 750000));
        productList.add(new Product("Laptop", 20000));
        productList.add(new Product("Wireless Mouse", 200));
        productList.add(new Product("Wireless Keyboard", 300));
    }

    private static void printCustomers(List<Customer> customerList) {
        for (Customer customer : customerList) {
            customer.printOrders();
        }
    }

    private static void printListOfProducts() {
        System.out.println("Here are the available products you can choose from: ");
        for (int i = 0; i < productList.size(); i++) {
            System.out.println(i + ". " + productList.get(i).getProductDetails());
        }
    }
}
