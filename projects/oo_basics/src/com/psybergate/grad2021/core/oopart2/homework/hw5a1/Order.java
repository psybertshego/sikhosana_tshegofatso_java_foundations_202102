package com.psybergate.grad2021.core.oopart2.homework.hw5a1;

import java.util.ArrayList;
import java.util.List;

public abstract class Order {
    private String orderNum;
    private List<Product> products;

    public Order(String orderNum) {
        this.orderNum = orderNum;
        this.products = new ArrayList<>();
    }

    public void printOrdersItems(){
        System.out.println("==> Order " + orderNum + ":");
        for (Product product : products) {
            product.printOrderDetails();
        }
        System.out.println("Order total = R" + getOrderTotal());
    }

    public abstract double getOrderTotal();

    public void addOrderItem(Product product){
        products.add(product);
    }

    public List<Product> getOrderItems() {
        return products;
    }
}
