package com.psybergate.grad2021.core.oopart2.homework.hw5a1;

public class LocalOrder extends Order {
    private final double discount = 0.1;  // 10% discount

    public LocalOrder(String orderNum) {
        super(orderNum);
    }

    public double getOrderTotal() {
        double total = 0;
        for (Product orderItem : getOrderItems()) {
            total += orderItem.getTotal();
        }
        return total - (total * discount);
    }

}
