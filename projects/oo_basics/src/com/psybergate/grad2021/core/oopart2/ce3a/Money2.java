package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money2 {
    private final BigDecimal value;

    public static void main(String[] args) {
        String str = "";
        Money2 money1 = new Money2(10);
    }

    public Money2(String amount) {
        this(new BigDecimal(amount));
    }

    public Money2(BigDecimal amount) {
        this.value = amount;
    }


    public Money2(int amount) {
        this.value = new BigDecimal(amount);
    }

    public Money2(double amount) {
        this(String.valueOf(amount));
    }

    public BigDecimal getValue() {
        return value;
    }

    public Money2 sumMoney(Money2 money) {
        return new Money2(value.add(money.value));
    }


    public BigDecimal sumMoney(double amount) {
        return value.add(new BigDecimal(amount));
    }
}

