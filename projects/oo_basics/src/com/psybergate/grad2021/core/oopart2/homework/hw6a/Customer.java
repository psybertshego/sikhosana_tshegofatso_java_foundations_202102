package com.psybergate.grad2021.core.oopart2.homework.hw6a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final boolean isInternational;
    private final String customerNum;
    private String name;
    private List<Order> orders;
    private int yearsWithUs;

    public Customer(String customerNum, String name, int yearsWithUs, boolean isInternational) {
        this.isInternational = isInternational;
        this.customerNum = customerNum;
        this.name = name;
        this.orders = new ArrayList<>();
        this.yearsWithUs = yearsWithUs;
    }

    public void printOrders() {
        System.out.println("Customer: " + customerNum + " has the following orders:");
        for (Order order : orders) {
            order.printOrdersItems();
        }
        System.out.println("*** Grand Total: R" + calculateDiscountedTotal() + " *** \n");
    }

    public boolean isInternational() {
        return isInternational;
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public double calculateDiscountedTotal() {
        if (isInternational()) {
            if (getOrdersTotal() > 500_000 && getOrdersTotal() < 1_000_000) {
                return getOrdersTotal() - getOrdersTotal() * 0.05;
            } else if (getOrdersTotal() > 1_000_000) {
                return getOrdersTotal() - getOrdersTotal() * 0.1;
            }
        } else {
            if (yearsWithUs > 2 && yearsWithUs <= 5) {
                return getOrdersTotal() - (getOrdersTotal() * 0.075);
            } else if (yearsWithUs > 5) {
                return getOrdersTotal() - (getOrdersTotal() * 0.125);
            }
        }
        return getOrdersTotal();
    }

    private double getOrdersTotal() {
        double total = 0;
        for (Order order : orders) {
            total += order.getOrderTotal();
        }
        return total;
    }
}
