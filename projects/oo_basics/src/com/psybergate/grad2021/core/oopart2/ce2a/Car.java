package com.psybergate.grad2021.core.oopart2.ce2a;

public class Car {

    private String name;
    private int topSpeed;

    public Car(String name, int topSpeed) {
        this.name = name;
        this.topSpeed = topSpeed;
    }

    public String getCarDetails(){
        return name + " has a top speed of " + topSpeed + "km/h";
    }

    public void setName(String name) {
        this.name = name;
    }
}
