package com.psybergate.grad2021.core.oopart2.demo1a;

public class ShapeUtils {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(20,20);
        Square square = new Square(3);
        Shape shape = new Rectangle(30,20);
        System.out.println("square.calculateArea() = " + square.calculateArea());
        System.out.println("rectangle.calculateArea() = " + rectangle.calculateArea());

        //Demonstrates that static methods cannot be overridden
        System.out.println("Rectangle.calculateRectangleArea(20,40) = " + Rectangle.calculateRectangleArea(20, 40));
        System.out.println("Shape.calculateRectangleArea(20,500) = " + Shape.calculateRectangleArea(20, 500));

        //Demonstrates that private methods can be overridden

        System.out.println("rectangle.getShapeType() = " + rectangle.getShapeType());
    }
}
