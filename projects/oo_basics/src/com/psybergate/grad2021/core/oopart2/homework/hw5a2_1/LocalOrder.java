package com.psybergate.grad2021.core.oopart2.homework.hw5a2_1;

public class LocalOrder extends Order {
  //private final double discount = 0.1;  // 10% discount

  public LocalOrder(String orderNum, Customer customer) {
    super(orderNum, customer);
  }

  public double getOrderTotal() {
    double total = 0;
    for (OrderItem orderItem : getOrderItems()) {
      total += orderItem.getTotal();
    }
    return total - (total * calculateDiscount());
  }

  public double calculateDiscount() {
    if (customer.getYearsWithUs() > 2 && customer.getYearsWithUs() <= 5) {
      return 0.075;
    } else if (customer.getYearsWithUs() > 5) {
      return 0.125;
    }
    return 0;
  }

}
