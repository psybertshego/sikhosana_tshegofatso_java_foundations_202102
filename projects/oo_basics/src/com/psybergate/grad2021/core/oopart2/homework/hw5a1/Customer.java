package com.psybergate.grad2021.core.oopart2.homework.hw5a1;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private boolean isInternational;
    private String customerNum;
    private int age;
    private List<Order> orders;

    public Customer(String customerNum, int age, boolean isInternational) {
        this.isInternational = isInternational;
        this.customerNum = customerNum;
        this.age = age;
        this.orders = new ArrayList();
    }

    public void printOrders() {
        System.out.println("Customer: "+ customerNum + " has the following orders:");
        for (Order order : orders) {
            order.printOrdersItems();
        }
        System.out.println("");
    }

    public boolean isInternational() {
        return isInternational;
    }

    public void addOrder(Order order) {
        orders.add(order);
//        if(isInternational)
//        {
//            orders.add((InternationalOrder)order);
//        }
//        else{
//            orders.add((LocalOrder)order);
//        }
    }


}
