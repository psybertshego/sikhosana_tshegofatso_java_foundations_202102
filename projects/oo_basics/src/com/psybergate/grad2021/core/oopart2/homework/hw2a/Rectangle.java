package com.psybergate.grad2021.core.oopart2.homework.hw2a;

public class Rectangle {

    private static final double MAX_LENGTH = 200;
    private static final double MAX_WIDTH = 100;
    private static final double MAX_AREA = 15000;

    private double length;
    private double width;

    public Rectangle(double length, double width) {
        checkConstraints(length, width);
        this.length = length;
        this.width = width;
    }

    private void checkConstraints(double length, double width) {
        if (length > MAX_LENGTH) {
            throw new RuntimeException("cannot create object with length greater than " + MAX_LENGTH);
        } else if (width > MAX_WIDTH) {
            throw new RuntimeException("cannot create object with width greater than " + MAX_WIDTH);
        } else if ((length * width) > MAX_AREA) {
            throw new RuntimeException("cannot create object with an area greater than " + MAX_AREA);
        } else if (length < width) {
            throw new RuntimeException("cannot create object with a width greater than length");
        }
    }

    public double calArea() {
        return length * width;
    }

    public double calPerimeter() {
        return 2 * (length + width);
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public boolean equals(Object o) {
        if(this == o) return  true;
        if(this == null) return false;

        if(o instanceof Rectangle){
            Rectangle rectangle = (Rectangle) o;
            return this.length == rectangle.getLength() && this.width == rectangle.getWidth();
        }
        return false;
    }

    public boolean isIdentical(Rectangle rectangle) {
        return this == rectangle;
    }

}
