package com.psybergate.grad2021.core.oopart2.homework.hw5a1;

public class InternationalOrder extends Order {
    private double importDuties = 0.2; //calculated at 20% for all the orders

    public InternationalOrder(String orderNum) {
        super(orderNum);
    }


    @Override
    public double getOrderTotal() {
        double total = 0;
        for (Product orderItem : getOrderItems()) {
            total += orderItem.getTotal();
        }
        return total * (1 + importDuties);
    }
}
