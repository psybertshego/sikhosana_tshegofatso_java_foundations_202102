package com.psybergate.grad2021.core.inner_classes.hw1;

public class Truck {

  private String manufacturer;

  private String model;

  private Engine engine;

  public Truck(String manufacturer, String model, String type, int rpm) {
    this.manufacturer = manufacturer;
    this.model = model;
    this.engine = new Engine(type,rpm);
  }

  public Truck(String manufacturer, String model) {
    this.manufacturer = manufacturer;
    this.model = model;
  }

  @Override
  public String toString() {
    return "manufacturer='" + manufacturer + '\'' +
            " model='" + model + '\'' +
            " engine=" + engine;
  }

  //static inner class
  public static class Engine{

    private String type;

    private int RPM;

    public Engine(String type, int RPM) {
      this.type = type;
      this.RPM = RPM;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public int getRPM() {
      return RPM;
    }

    public void setRPM(int RPM) {
      this.RPM = RPM;
    }

    @Override
    public String toString() {
      return "Truck Engine{" +
              "type='" + type + '\'' +
              ", RPM=" + RPM +
              '}';
    }
  }
}
