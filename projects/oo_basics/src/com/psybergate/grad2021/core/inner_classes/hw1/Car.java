package com.psybergate.grad2021.core.inner_classes.hw1;

public class Car {

  private String manufacturer;

  private String model;

  private Engine engine;

  private double topSpeed;

  {
    topSpeed = 180;
  }

  public Car(String manufacturer, String model, String type, int RPM) {
    this.manufacturer = manufacturer;
    this.model = model;
    this.engine = new Engine(type, RPM);
  }

  public Car(String manufacturer, String model) {
    this.manufacturer = manufacturer;
    this.model = model;
  }

  public double getTopSpeed(TopSpeed topSpeedObject) {
    topSpeedObject.displayTopSpeed();
    topSpeed = topSpeedObject.getTopSpeed();
    return topSpeed;
  }

  @Override
  public String toString() {
    return "manufacturer='" + manufacturer + '\'' +
            " model='" + model + '\'' +
            " engine=" + engine +
            " topSpeed=" + topSpeed;
  }

  //object inner class
  class Engine {

    private String type;

    private int RPM;

    public Engine(String type, int RPM) {
      this.type = type;
      this.RPM = RPM;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public int getRPM() {
      return RPM;
    }

    public void setRPM(int RPM) {
      this.RPM = RPM;
    }

    @Override
    public String toString() {
      return "Car Engine{" +
              "type='" + type + '\'' +
              ", RPM=" + RPM +
              '}';
    }
  }
}
