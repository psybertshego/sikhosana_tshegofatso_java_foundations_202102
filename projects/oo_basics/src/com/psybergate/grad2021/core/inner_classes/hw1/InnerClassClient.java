package com.psybergate.grad2021.core.inner_classes.hw1;

public class InnerClassClient {
  public static void main(String[] args) {

    Car bmw = new Car("bmw", "m2", "v6", 24000);

    double speed = bmw.getTopSpeed(new TopSpeed() {

      class Printable {
        void print() {
          System.out.println("We in printable");
        }
      }

      @Override
      public double getTopSpeed() {
//        System.out.println("TopSpeed is " + 240);
        return 240;
      }

      public void print() {
        new Printable().print();
      }

      @Override
      public void displayTopSpeed() {
        System.out.println("Real TopSpeed is " + getTopSpeed() + "km");
      }
    });

//     bmw.getTopSpeed(() -> System.out.println("Top speed is 300 : lambda "));

    System.out.println(bmw);

    Truck.Engine lorryEngine = new Truck.Engine("v10", 20000);

    System.out.println("\n" + lorryEngine);

    Object methodInnerClass = methodInnerClass();
    System.out.println("methodInnerClass.toString() = " + methodInnerClass.toString());

  }

  public static Object methodInnerClass(){
    class MethodInnerClass extends Object{

      @Override
      public String toString() {
        return "We in the method";
      }
    }

    MethodInnerClass innerClass = new MethodInnerClass();



    return innerClass;
  }


}
