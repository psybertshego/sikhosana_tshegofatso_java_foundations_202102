package com.psybergate.grad2021.core.enums.inclass;

public enum Months {
  JANUARY(31, 1, "JAN", "JANUARY"),
  FEBRUARY(28, 2, "FEB", "FEBRUARY"),
  MARCH(31, 3, "MAR", "MARCH");

  private int days;
  private int position;
  private String shortDescription;
  private String description;

  Months(int days, int position, String shortDescription, String description) {
    this.days = days;
    this.position = position;
    this.shortDescription = shortDescription;
    this.description = description;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public int getDays() {
    return days;
  }
}
