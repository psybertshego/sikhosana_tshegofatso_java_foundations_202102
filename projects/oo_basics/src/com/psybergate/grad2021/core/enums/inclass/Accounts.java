package com.psybergate.grad2021.core.enums.inclass;

public enum Accounts {
  SAVINGS_ACCOUNT("SAVINGS"),
  CURRENT_ACCOUNT("CURRENT");

  private String shortDescription;

  Accounts(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getShortDescription() {
    return shortDescription;
  }
}
