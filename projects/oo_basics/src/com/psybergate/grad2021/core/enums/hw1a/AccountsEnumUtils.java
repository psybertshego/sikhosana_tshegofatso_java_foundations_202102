package com.psybergate.grad2021.core.enums.hw1a;


public class AccountsEnumUtils {
  public static void main(String[] args) {
//    Account account = new Account("001", 0);
    Account account = new Account("001", com.psybergate.grad2021.core.enums.inclass.Account.CURRENT_ACCOUNT);

    Account account1 = new Account("002", Accounts.SAVINGS_ACCOUNT.name());

    // Where is the values method found?
    for (Accounts value : Accounts.CURRENT_ACCOUNT.values()) {
      System.out.println(value);
    }
    //How do I access methods of an ENUM?
    Accounts.CURRENT_ACCOUNT.print();
    Accounts accountType = Accounts.SAVINGS_ACCOUNT;
    accountType.print();
    System.out.println(Accounts.CURRENT_ACCOUNT.getClass().getSuperclass().getSuperclass());
  }
}
