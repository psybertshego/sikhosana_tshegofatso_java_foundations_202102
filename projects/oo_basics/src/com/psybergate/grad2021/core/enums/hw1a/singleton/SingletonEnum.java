package com.psybergate.grad2021.core.enums.hw1a.singleton;

public class SingletonEnum {
  private static final SingletonEnum SAVINGS_ACCOUNT = new SingletonEnum("SAVINGS_ACCOUNT");
  private static final SingletonEnum CURRENT_ACCOUNT = new SingletonEnum("CURRENT_ACCOUNT");
  private String name;

  public SingletonEnum(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
