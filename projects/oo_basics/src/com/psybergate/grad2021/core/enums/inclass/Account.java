package com.psybergate.grad2021.core.enums.inclass;

public class Account {

  private String accountNum;
  private String accountType;
  public static final String SAVINGS_ACCOUNT = "SAVINGS_ACCOUNT";
  public static final String CURRENT_ACCOUNT = "CURRENT_ACCOUNT";


  //first implementation for enum like concepts with accountType
  public Account(String accountNum, String accountType) {
    this.accountNum = accountNum;
    this.accountType = accountType;
  }
  



}
