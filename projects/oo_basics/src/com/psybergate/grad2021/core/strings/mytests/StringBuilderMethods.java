package com.psybergate.grad2021.core.strings.mytests;

public class StringBuilderMethods {
  public static void main(String[] args) {
    StringBuilder stringBuilder = new StringBuilder("hello world");

    //It's odd that you can add a boolean to appends :/
    stringBuilder.append(true);
    System.out.println("stringBuilder = " + stringBuilder);

    System.out.println("stringBuilder.reverse() = " + stringBuilder.reverse());
  }
}
