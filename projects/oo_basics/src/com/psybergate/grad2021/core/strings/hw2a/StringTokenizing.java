package com.psybergate.grad2021.core.strings.hw2a;

import java.util.StringTokenizer;

public class StringTokenizing {
  public static void main(String[] args) {
    StringTokenizer stringTokenizer = new StringTokenizer("REUSE,REDUCE,RECYCLE", ",");

    for (int i = 1; stringTokenizer.hasMoreTokens(); i++) {
      System.out.println(i + ") " + stringTokenizer.nextToken());
    }
  }


}
