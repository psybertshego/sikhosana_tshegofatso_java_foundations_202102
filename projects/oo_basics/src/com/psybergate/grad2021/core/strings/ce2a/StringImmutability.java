package com.psybergate.grad2021.core.strings.ce2a;

public class StringImmutability {
  public static void main(String[] args) {
    String str = "abc";
    String str1 = str;
    String str2 = new String("abc");

    System.out.println("(str1 == str2) = " + (str1 == str2));

  }


}
