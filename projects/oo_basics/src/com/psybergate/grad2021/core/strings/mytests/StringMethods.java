package com.psybergate.grad2021.core.strings.mytests;

public class StringMethods {
  public static void main(String[] args) {
    String str = "abc";

    System.out.println("str.charAt(3) = " + str.charAt(2));
    System.out.println("str.concat(\"def\") = " + str.concat("def"));
//    System.out.println("str.equalsIgnoreCase(\"ABC\") = " + str.equalsIgnoreCase("ABC"));
//    str = str.replace("b","c");
//    System.out.println(str);
    System.out.println("str.substring(0,1) = " + str.substring(0, 2));

    String str1 = "abcDef ";
    System.out.println("str1.toLowerCase() = " + str1.toLowerCase());
    System.out.println("str1.toUpperCase() = " + str1.toUpperCase());
    System.out.println("str1.trim() = " + str1.trim());

    String str3 = str.intern();
    System.out.println("(str3 == str1) = " + (str3 == str1));

    System.out.println("(str3.intern() == \"abc\" = " + (str3.intern() == "abc"));
  }
}
