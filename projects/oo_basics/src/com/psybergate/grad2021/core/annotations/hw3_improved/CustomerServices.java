package com.psybergate.grad2021.core.annotations.hw3_improved;

import java.util.Scanner;

public class CustomerServices {
  public static void saveCustomer(Customer c) {
    DatabaseManager.insertCustomer(c);
  }

  public static void getCustomer(String customerNum) {
    StringBuilder queryBuilder = new StringBuilder("select * from customer where customerNum = ");
    queryBuilder.append("'" + customerNum + "'");

    String result = JDBCConnect.executeManipulationQuery(queryBuilder.toString());
    if (result == null) {
      System.out.println("no results to work with");
      return;
    }

    Scanner scanner = new Scanner(result);
    while (scanner.hasNextLine()) {
      System.out.println(scanner.nextLine());
    }


  }
}
