package com.psybergate.grad2021.core.annotations.hw2;

import java.time.LocalDate;

@DomainClass
public class Customer {
  @DomainProperty
  private String customerNum;

  @DomainProperty
  private String name;

  @DomainProperty
  private String surname;

  @DomainProperty
  private Integer dateOfBirth; // format: YearMonthDate

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = calculateAge();
  }

  private int calculateAge() {
    return LocalDate.now().getYear() - Integer.valueOf(dateOfBirth.toString().substring(0,4));
  }


}
