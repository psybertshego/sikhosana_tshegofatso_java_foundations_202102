
package com.psybergate.grad2021.core.annotations.hw4;

import com.psybergate.grad2021.core.annotations.hw3_improved.annotations.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager {
  public static void createCustomerTable() {

    StringBuilder queryBuilder = new StringBuilder();

    Class customerClass = Customer.class;
    queryBuilder.append("create table " + customerClass.getSimpleName() + "(");

    Field[] fields = customerClass.getDeclaredFields();

    for (Field field : fields) {

      Annotation[] annotations = field.getDeclaredAnnotations();

      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          DomainProperty domainAnnotation = ((DomainProperty) annotation);

          queryBuilder.append(domainAnnotation.name() + " ");
          queryBuilder.append(domainAnnotation.databaseType());

          addConstraints(queryBuilder, domainAnnotation);
          queryBuilder.append(",");
        }
      }
    }
    queryBuilder.deleteCharAt(queryBuilder.length() - 1);
    queryBuilder.append(")");

    //execute query using
    try {
      Statement statement = JDBCConnect.getDatabaseConnection().createStatement();
      statement.execute(queryBuilder.toString());
    } catch (SQLException throwables) {

    }
  }

  public static void insertCustomer(Customer customer) {
    if (customer == null) {
      throw new NullPointerException("There are no customers to add");
    }

    StringBuilder queryBuilder = new StringBuilder("insert into customer (customerNum,name,surname,dateOfBirth,id_number) values (");

    Field[] fields = customer.getClass().getDeclaredFields();
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          if (!((DomainProperty) annotation).nullable()) {
            if (passesNullChecks(customer)) {
              queryBuilder.append(getValue(customer, field.getName()));
            }
          } else {
            if (field.getName().equals("dateOfBirth")) {
              queryBuilder.append(getValue(customer, field.getName()));
            }
          }
        }
      }
    }
    queryBuilder.deleteCharAt(queryBuilder.length() - 1);
    queryBuilder.append(")");

    JDBCConnect.executeManipulationQuery(queryBuilder.toString());
  }

  private static String getValue(Customer customer, String fieldName) {
    switch (fieldName) {
      case "name":
        return "'" + customer.getName() + "'" + ",";
      case "surname":
        return "'" + customer.getSurname() + "'" + ",";
      case "idNumber":
        return customer.getIdNumber() + ",";
      case "customerNum":
        return "'" + customer.getCustomerNum() + "'" + ",";
      case "dateOfBirth":
        return customer.getDateOfBirth() + ",";
    }
    return "";
  }

  private static void addConstraints(StringBuilder queryBuilder, DomainProperty annotation) {
    if (!annotation.nullable()) {
      queryBuilder.append(" not null");
    }
    if (annotation.primaryKey()) {
      queryBuilder.append(" primary key");
    }
    if (annotation.unique()) {
      queryBuilder.append(" unique");
    }
  }

  private static boolean passesNullChecks(Customer customer) {
    if (customer.getCustomerNum() == null) return false;
    if (customer.getName() == null) return false;
    if (customer.getSurname() == null) return false;
    if (customer.getIdNumber() == null) return false;
    return true;
  }
}
