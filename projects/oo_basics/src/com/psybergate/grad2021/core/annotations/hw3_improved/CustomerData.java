package com.psybergate.grad2021.core.annotations.hw3_improved;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CustomerData {
  public static Customer generateRandomCustomer() {
    List<String> names = getRandomNames();
    Random random = new Random();
    String customerNum = getRandomCustomerNum(random);
    String name = names.get(random.nextInt(names.size()));
    String surname = names.get(random.nextInt(names.size()));
    int idNumber = 19000 + random.nextInt(900_000_000);

    return new Customer(customerNum, name, surname, idNumber, 100 * random.nextInt(900));
  }

  private static String getRandomCustomerNum(Random random) {
    return "C-" + (1000 + random.nextInt(9000));
  }

  private static List<String> getRandomNames() {
    List<String> names = new ArrayList<>();
    names.add("jake");
    names.add("fin");
    names.add("bubblegum");
    names.add("darwin");
    names.add("nicole");
    names.add("gumball");
    names.add("richard");
    names.add("rigby");
    names.add("mordecia");
    names.add("margret");
    names.add("ben");
    names.add("skips");
    names.add("benson");
    names.add("steven");
    return names;
  }


}
