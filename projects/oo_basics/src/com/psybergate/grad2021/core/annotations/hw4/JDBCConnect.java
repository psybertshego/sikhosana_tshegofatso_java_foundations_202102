
package com.psybergate.grad2021.core.annotations.hw4;

import java.sql.*;

public class JDBCConnect {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_URL = "jdbc:postgresql://localhost:5432/hw3_database";


  public static void executeDefinitionQuery(String queryString) throws ClassNotFoundException {
    try {
      Connection conn = DriverManager.getConnection(DB_URL, "postgres", "admin");
      Statement query = conn.createStatement();
      query.executeQuery(queryString);
      query.close();
      conn.close();

    } catch (SQLException e) {
      executeDefinitionQuery("drop table customer");
    }
  }

  public static String executeManipulationQuery(String queryString) {

    try {
      Connection conn = getDatabaseConnection();
      Statement query = conn.createStatement();
      ResultSet result = query.executeQuery(queryString);

      StringBuilder resultString = new StringBuilder();
      while (result.next()) {

        resultString.append(result.getInt("customer_num") + " " + result.getString("name")
                + " " + result.getString("surname"));
      }

      result.close();
      query.close();
      conn.close();
      return resultString.toString();

    } catch (SQLException e) {
      System.out.println("e = " + e);
      return null;
    }
  }

  public static Connection getDatabaseConnection(){
    Connection conn = null;
    try {
      conn = DriverManager.getConnection(DB_URL, "postgres", "admin");
      System.out.println("Connection established");
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    return conn;
  }
}
