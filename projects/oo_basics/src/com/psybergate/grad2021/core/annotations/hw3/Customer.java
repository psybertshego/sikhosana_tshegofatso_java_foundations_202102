package com.psybergate.grad2021.core.annotations.hw3;

import com.psybergate.grad2021.core.annotations.hw3.annotations.DomainClass;
import com.psybergate.grad2021.core.annotations.hw3.annotations.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw3.annotations.DomainTransient;

import java.time.LocalDate;

@DomainClass
public class Customer {

  @DomainProperty(primaryKey = true)
  private String customerNum;

  @DomainProperty
  private String name;

  @DomainProperty
  private String surname;

  @DomainProperty(nullable = true)
  private Integer dateOfBirth; // format: YearMonthDate

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = calculateAge();
  }

  private int calculateAge() {
    return LocalDate.now().getYear() - Integer.valueOf(dateOfBirth.toString().substring(0, 4));
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }
}
