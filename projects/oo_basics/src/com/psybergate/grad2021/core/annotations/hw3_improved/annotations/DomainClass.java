package com.psybergate.grad2021.core.annotations.hw3_improved.annotations;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DomainClass {
    String name();
}
