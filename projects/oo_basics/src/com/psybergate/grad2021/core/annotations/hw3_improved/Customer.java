package com.psybergate.grad2021.core.annotations.hw3_improved;

import com.psybergate.grad2021.core.annotations.hw3_improved.annotations.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw3_improved.annotations.DomainTransient;
import com.psybergate.grad2021.core.annotations.hw3_improved.annotations.DomainClass;

import java.time.LocalDate;

@DomainClass(name = "customer")
public class Customer {

  @DomainProperty(name = "customerNum", databaseType = "varchar(50)", primaryKey = true)
  private String customerNum;

  @DomainProperty(name = "name", databaseType = "varchar(50)")
  private String name;

  @DomainProperty(name = "surname", databaseType = "varchar(50)")
  private String surname;

  @DomainProperty(name = "dateOfBirth", databaseType = "int", nullable = true)
  private Integer dateOfBirth; // format: YearMonthDate

  @DomainProperty(name = "id_number", databaseType = "int", unique = true)
  private Integer idNumber;

  @DomainTransient
  private int age;

  public Customer(String customerNum, String name, String surname, int idNumber, Integer dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.idNumber = idNumber;
    this.dateOfBirth = dateOfBirth;
    setAge();
  }

  private void setAge() {
    this.age = LocalDate.now().getYear() - Integer.parseInt(dateOfBirth.toString().substring(0, 4));
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  public Integer getIdNumber() { return idNumber; }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum='" + customerNum + '\'' +
            "\t name='" + name + '\'' +
            "\t surname='" + surname + '\'' +
            "\t dateOfBirth=" + dateOfBirth +
            "\t idNumber=" + idNumber +
            "\t age=" + age +
            '}';
  }
}
