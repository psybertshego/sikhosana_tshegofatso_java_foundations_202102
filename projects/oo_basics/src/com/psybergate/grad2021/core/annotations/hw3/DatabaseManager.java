package com.psybergate.grad2021.core.annotations.hw3;

import com.psybergate.grad2021.core.annotations.hw3.annotations.DomainProperty;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class DatabaseManager {
  public static void createCustomerTable() {
    Class customerClass = Customer.class;

    Field[] fields = customerClass.getDeclaredFields();

    StringBuilder queryBuilder = new StringBuilder();
    queryBuilder.append("create table " + customerClass.getSimpleName() + "(");

    for (Field field : fields) {

      Annotation[] annotations = field.getDeclaredAnnotations();

      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          queryBuilder.append(field.getName());
          if (getFieldTypeName(field).equals("String")) {
            queryBuilder.append(" varchar(50)");
            if (!((DomainProperty) annotation).nullable()) {
              queryBuilder.append(" not null");
            }
            if (((DomainProperty) annotation).primaryKey()) {
              queryBuilder.append(" primary key");
            }
            queryBuilder.append(",");
          } else if (getFieldTypeName(field).equals("Integer")) {
            queryBuilder.append(" int");
          }
        }
      }
    }
    queryBuilder.append(")");

    //execute query using
    try {
      JDBCConnect.executeManipulationQuery(queryBuilder.toString());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public static void insertCustomer(Customer customer) {
    if (customer == null) {
      throw new NullPointerException("There are no customers to add");
    }

    StringBuilder queryBuilder = new StringBuilder("insert into customer (customerNum,name,surname,dateOfBirth) values (");


    Field[] fields = customer.getClass().getDeclaredFields();
    for (Field field : fields) {
      Annotation[] annotations = field.getDeclaredAnnotations();
      for (Annotation annotation : annotations) {
        if (annotation instanceof DomainProperty) {
          if (!((DomainProperty) annotation).nullable()) {
            if (passesNullChecks(customer)) {
              switch (field.getName()) {
                case "name":
                  queryBuilder.append("'" + customer.getName() + "'" + ",");
                  break;
                case "surname":
                  queryBuilder.append("'" + customer.getSurname() + "'" + ",");
                  break;
                case "customerNum":
                  queryBuilder.append("'" + customer.getCustomerNum() + "'" + ",");
                  break;
              }
            }
          } else {
            if (field.getName().equals("dateOfBirth")) {
              queryBuilder.append(customer.getDateOfBirth());
            }
          }
        }
      }
    }
    queryBuilder.append(")");

    System.out.println("queryBuilder = " + queryBuilder);

    try {
      JDBCConnect.executeManipulationQuery(queryBuilder.toString());
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static boolean passesNullChecks(Customer customer) {
    if (customer.getCustomerNum() == null) return false;
    if (customer.getName() == null) return false;
    if (customer.getSurname() == null) return false;
    return true;
  }


  private static String getFieldTypeName(Field field) {
    return field.getType().getSimpleName();
  }
}
