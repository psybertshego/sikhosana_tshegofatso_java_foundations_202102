package com.psybergate.grad2021.core.annotations.hw3;

import java.sql.*;

public class JDBCConnect {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";
  private static final String DB_URL = "jdbc:postgresql://localhost:5432/hw3";




  public static void executeDefinitionQuery() throws ClassNotFoundException{
    Class.forName(JDBC_DRIVER);


  }

  public static String executeManipulationQuery(String queryString) throws ClassNotFoundException {
    Class.forName(JDBC_DRIVER);

    try {
      Connection conn = DriverManager.getConnection(DB_URL, "postgres", "admin");
      Statement query = conn.createStatement();
      ResultSet result = query.executeQuery(queryString);

      StringBuilder resultString = new StringBuilder();
      while (result.next()) {
        resultString.append(result.getInt("customerNum") + " " + result.getString("name")
                + " " + result.getString("surname"));
      }


      result.close();
      query.close();
      conn.close();
      return resultString.toString();

    } catch (SQLException e) {
      System.out.println("e = " + e);
      return null;
    }
  }
}
