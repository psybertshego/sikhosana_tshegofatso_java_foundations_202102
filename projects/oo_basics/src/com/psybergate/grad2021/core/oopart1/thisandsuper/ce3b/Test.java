package com.psybergate.grad2021.core.oopart1.thisandsuper.ce3b;

public class Test {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Ralf","doge");
        Dog dog2 = new Dog("Barker","German Shepard","Me");

        dog1.sayParentSomething();
        dog2.saySomething();

        System.out.println("dog1.getOwner() = " + dog1.getOwner());
        System.out.println("dog2.getOwner() = " + dog2.getOwner());
    }
}
