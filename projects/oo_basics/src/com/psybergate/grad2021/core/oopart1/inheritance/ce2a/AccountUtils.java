package com.psybergate.grad2021.core.oopart1.inheritance.ce2a;

public class AccountUtils {
    public static void main(String[] args) {
        CurrentAccount currentAccount = new CurrentAccount(1,"Tshego","Sikhosana",100);
        currentAccount.getAccountType();


        currentAccount.withdrawAmount(1000);
        currentAccount.isOverdrawn();
        currentAccount.needsToBeReviewed();
        currentAccount.depositAmount(200);
        currentAccount.isOverdrawn();
        //currentAccount
    }
}
