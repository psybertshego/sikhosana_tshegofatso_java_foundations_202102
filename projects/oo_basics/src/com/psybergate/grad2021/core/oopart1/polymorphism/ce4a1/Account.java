package com.psybergate.grad2021.core.oopart1.polymorphism.ce4a1;

public abstract class Account {

    private int accountNum;
    private int balance;
    private String bankName;

    public Account(int accountNum, int balance,String bankName) {
        this.accountNum = accountNum;
        this.balance = balance;
        this.bankName = bankName;
    }

    public int getBalance() {
        return balance;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public abstract boolean isOverDrawn();

    public abstract boolean needsToBeReviewed();


}
