package com.psybergate.grad2021.core.oopart1.inheritance.ce2a;

public class CurrentAccount extends Account {

    private static final int MAX_OVERDRAFT = 10000;
    private static final String ACCOUNT_TYPE = "Current Account";
    private final double interestRate = 0.2;
    private double overdraft;

    public CurrentAccount(int accountNum, String name, String surname, double balance) {
        super(accountNum, name, surname, balance);
        this.overdraft = 0;
    }

    public boolean needsToBeReviewed() {
        boolean needsToBeReviewed = false;
        double balance = getBalance();

        if (overdraft > 0) {
            if (overdraft / balance > interestRate || balance <= -50000) {
                needsToBeReviewed = true;
                System.out.println("Account needs to be reviewed");
            }
        }
        return needsToBeReviewed;
    }

    @Override
    public void isOverdrawn() {
        double balance = getBalance();
        if (balance < 0) {
            if (overdraft >= 0 && overdraft <= MAX_OVERDRAFT) {
                overdraft = Math.abs(balance);
                System.out.println("Account is overdrawn by " + overdraft);
                System.out.println("Current balance:  " + balance);
            }
        }
    }

    @Override
    public void getAccountType() {
        System.out.println("ACCOUNT_TYPE = " + ACCOUNT_TYPE);
    }
}
