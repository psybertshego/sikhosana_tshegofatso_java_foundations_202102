package com.psybergate.grad2021.core.oopart1.inheritance.demo1a;

public abstract class Shape {

    public abstract double calcArea();
    public abstract void getType();

}
