package com.psybergate.grad2021.core.oopart1.homework.hw4a;

public class CustomerUtils {
    public static void main(String[] args) {
        Person person = new Person("001","The Moon","Tshego",23);
        Company company = new Company("002","The Sun and Beyond","A0023",2010);
        Customer genericCustomer = new Customer("003","Generic world");


        //Demonstrate difference
        System.out.println("person.getCustomerNum() = " + person.getCustomerNum());
        System.out.println("company.getCustomerNum() = " + company.getCustomerNum());
        System.out.println("genericCustomer.getCustomerNum() = " + genericCustomer.getCustomerNum());

        //Demonstrate similarity
        isCustomerInstance(person);
        isCustomerInstance(company);
        isCustomerInstance(genericCustomer);

        /**
         * Q: What is the difference between each object?
         * A: Their object states and the type of object each object is
         *
         * Q: What is the same for each object
         * A: Their are an instance of Customer type and carry all it's states due to inheritance
         */


     }

     private static void isCustomerInstance(Object object){
        if(object instanceof Customer){
            System.out.println("Is an instance of Customer");
        }
     }
}
