package com.psybergate.grad2021.core.oopart1.polymorphism.demo2a;

public abstract class Shape {

    public Shape(){

    }

    public abstract double calculateArea();
    public abstract double calculatePerimeter();

    public void print(){
        System.out.println("Generic Print Shape Method");
    }

    public abstract String getType();
}
