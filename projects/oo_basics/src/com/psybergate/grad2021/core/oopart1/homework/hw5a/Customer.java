package com.psybergate.grad2021.core.oopart1.homework.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerNum;
    private String customerName;
    private List<Account> accountList;

    public Customer(String customerNum, String customerName) {
        this.customerNum = customerNum;
        this.customerName = customerName;
        this.accountList = new ArrayList<>();
    }

    public void addAccount(Account account) {
        accountList.add(account);
    }

    public void printBalances() {
        System.out.println("Customer number: " + customerNum);

        for (Account account : accountList) {
            System.out.println( "Account number: "+account.getAccountNum() + " (" +
                    account.getAccountType() + ") = " + getAccountBalance(account));
        }
    }

    private int getAccountBalance(Account account) {
        return account.getBalance();
    }
}
