package com.psybergate.grad2021.core.oopart1.homework.hw2a;

public class MyOldDatePP {
    public static void main(String[] args) {

        String date = "20200314";
        dateIsValid(date);
        System.out.println("addDays(date,2) = " + addDays(date, 62));
        System.out.println("addDays = " + addDays("20201231", 13));
        System.out.println("addDays = " + addDays("20201012", 21));
        System.out.println("addDays = " + addDays("20200103", 14));
        dateCompare("20200204","20210204");
    }


    public static int dateCompare(String date1, String date2) {
        if (Integer.parseInt(date1) > Integer.parseInt(date2)) {
            System.out.println("date1 is larger than date2");
            return 1;
        } else if (Integer.parseInt(date1) == Integer.parseInt(date2)) {
            System.out.println("date1 is equal to date2");
            return -1;
        } else if (Integer.parseInt(date1) < Integer.parseInt(date2)) {
            System.out.println("date1 is smaller than date2");
            return 0;
        }
        return -2;
    }

    public static String addDays(String date, int days) {
        String newDate = "";
        int currentDay = Integer.parseInt(date.substring(6, 8));
        int currentMonth = Integer.parseInt(date.substring(4, 6));

//        if(days > 31)
//        {
//            date = addDays(date,days - 31);
//        }

        if (currentMonth == 2 && (days + currentDay) > 28) {
            currentMonth += 1;
            currentDay = days + currentDay - 28;
            return buildDate(date, currentMonth, currentDay);
        }

        if (currentMonth < 8) {
            if (currentMonth % 2 == 1) {
                if (days + currentDay > 31) {
                    newDate = monthEndingIn31(date, days, currentDay, currentMonth);
                } else {
                    newDate = buildDate(date, currentMonth, days + currentDay);
                }
            } else {
                if (days + currentDay > 30) {
                    newDate = monthEndingIn30(date, days, currentDay, currentMonth);
                } else {
                    newDate = buildDate(date, currentMonth, days + currentDay);
                }
            }
        } else if (currentMonth < 13) {
            if (currentMonth % 2 == 0) {
                if (days + currentDay > 31) {
                    newDate = monthEndingIn31(date, days, currentDay, currentMonth);
                } else {
                    newDate = buildDate(date, currentMonth, days + currentDay);
                }
            } else {
                if (days + currentDay > 30) {
                    newDate = monthEndingIn30(date, days, currentDay, currentMonth);
                } else {
                    newDate = buildDate(date, currentMonth, days + currentDay);
                }
            }
        }
        return newDate;
    }

    private static String monthEndingIn31(String date, int days, int currentDay, int currentMonth) {
        currentDay = days + currentDay - 31;
        if (currentMonth + 1 > 12) {
            currentMonth = 1;
        } else {
            currentMonth += 1;
        }
        return buildDate(date, currentMonth, currentDay);
    }

    private static String monthEndingIn30(String date, int days, int currentDay, int currentMonth) {
        currentDay = days + currentDay - 30;
        if (currentMonth + 1 > 12) {
            currentMonth = 1;
        } else {
            currentMonth += 1;
        }
        return buildDate(date, currentMonth, currentDay);
    }

    private static String buildDate(String date, int month, int day) {
        int year = Integer.parseInt(date.substring(0, 4));
        if (month == 1) {
            year += 1;
        }
        if (day < 10) {
            if (month < 10) {
                return year + "0" + month + "0" + day;
            }
            return year + "" + month + "0" + day;
        } else {
            if (month < 10) {
                return year + "0" + month + "" + day;
            }
            return "" + year + "" + month + "" + day;
        }
    }

    public static boolean dateIsValid(String date) {
        boolean isValid = false;
        int month;
        int day;

        if (date.length() == 8) {
            month = Integer.parseInt(date.substring(4, 6));
            day = Integer.parseInt(date.substring(6, 8));

            if (month > 0 && month < 13) {
                if (isDayValid(month, day)) {
                    System.out.println("Date is valid");
                    isValid = true;
                }
            }
        }

        return isValid;
    }

    private static boolean isDayValid(int month, int day) {
        boolean isValidDay = false;

        if (month == 2) {
            if (day < 29) {
                return true;
            }
        }

        if (month < 8) {
            if (month % 2 == 1) {
                if (day < 32) {
                    isValidDay = true;
                }
            } else {
                if (day < 31) {
                    isValidDay = true;
                }
            }
        } else if (month < 13) {
            if (month % 2 == 1) {
                if (day < 31) {
                    isValidDay = true;
                }
            } else {
                if (day < 32) {
                    isValidDay = true;
                }
            }
        }
        return isValidDay;
    }
}
