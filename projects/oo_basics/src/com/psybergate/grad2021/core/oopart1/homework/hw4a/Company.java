package com.psybergate.grad2021.core.oopart1.homework.hw4a;

import java.util.Date;

public class Company extends Customer{
    private String companyRegNum;
    private int yearRegistered;

    public Company(String customerNum, String customerAddress, String companyRegNum, int yearRegistered) {
        super(customerNum, customerAddress);
        this.companyRegNum = companyRegNum;
        this.yearRegistered = yearRegistered;
    }


    @Override
    public String getCustomerNum() {
        return super.getCustomerNum();
    }
}
