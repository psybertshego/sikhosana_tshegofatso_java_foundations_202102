package com.psybergate.grad2021.core.oopart1.encapsulation.ce1a;

public class CurrentAccount {

    private int accNum;
    private int balance;

    public CurrentAccount(int accNum, int balance) {
        this.accNum = accNum;
        this.balance = balance;
    }


    public int getBalance() {
        return balance;
    }
}
