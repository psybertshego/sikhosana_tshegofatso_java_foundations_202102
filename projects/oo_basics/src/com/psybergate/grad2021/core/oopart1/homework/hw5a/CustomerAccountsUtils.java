package com.psybergate.grad2021.core.oopart1.homework.hw5a;

public class CustomerAccountsUtils {
    public static void main(String[] args) {
        Customer customer1 = new Customer("001","Ivan");
        Customer customer2 = new Customer("002","Nicholas");
        Customer customer3 = new Customer("003","Micheal");

        customer1.addAccount(new CurrentAccount(102,2000,0));
        customer1.addAccount(new SavingsAccount(124,3000));
        customer2.addAccount(new SavingsAccount(225,300));
        customer3.addAccount(new CurrentAccount(326,1000,0));

        customer1.printBalances();
        customer2.printBalances();
        customer3.printBalances();

    }
}
