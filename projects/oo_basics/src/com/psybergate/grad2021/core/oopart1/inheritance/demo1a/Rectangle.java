package com.psybergate.grad2021.core.oopart1.inheritance.demo1a;

public class Rectangle extends Shape {

    private static final String TYPE = "Rectangle";
    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }


    @Override
    public double calcArea() {
        return length*width;
    }

    @Override
    public void getType(){
        System.out.println("TYPE = " + TYPE);
    }
}
