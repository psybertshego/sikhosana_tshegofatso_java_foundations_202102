package com.psybergate.grad2021.core.oopart1.encapsulation.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer extends Person{

    private String customerName;
    private int customerNumber;
    private String customerAddress;
    private List<CurrentAccount> currentAccountList;

    public Customer(String customerName, int customerNumber, String customerAddress) {
        this.customerName = customerName;
        this.customerNumber = customerNumber;
        this.customerAddress = customerAddress;
        currentAccountList = new ArrayList<>();
    }

    @Override
    public void print() {
        System.out.println("I am a customer");
    }

    public void addAccount(CurrentAccount account) {
        currentAccountList.add(account);
    }


    public void getTotalBalance() {
        int total = 0;
        for (CurrentAccount currentAccount : currentAccountList) {
            total += currentAccount.getBalance();
        }

        System.out.println("Balance total = " + total);
    }
}
