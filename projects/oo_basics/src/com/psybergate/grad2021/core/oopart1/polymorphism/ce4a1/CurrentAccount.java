package com.psybergate.grad2021.core.oopart1.polymorphism.ce4a1;

public class CurrentAccount extends Account {

    private static final int MAX_OVERDRAFT = 10000;
    private static final String ACCOUNT_TYPE = "Current Account";
    private final double INTEREST_RATE = 0.2;
    private double overdraft;

    public CurrentAccount(int accountNum, int balance, String bankName, double overdraft) {
        super(accountNum, balance, bankName);
        this.overdraft = overdraft;
    }

    @Override
    public boolean isOverDrawn() {
        return getBalance() < getOverdraftBalance();
    }

    private double getOverdraftBalance() {
        return -1 * overdraft;
    }

    public boolean needsToBeReviewed() {
        return getBalance() < (1 + INTEREST_RATE) * getOverdraftBalance();
    }

    public String getAccountType() {
        return ACCOUNT_TYPE;
    }
}
