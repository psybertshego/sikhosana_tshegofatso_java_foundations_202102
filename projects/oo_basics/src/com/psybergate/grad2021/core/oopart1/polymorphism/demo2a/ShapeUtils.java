package com.psybergate.grad2021.core.oopart1.polymorphism.demo2a;

import java.util.ArrayList;
import java.util.List;

public class ShapeUtils {
    public static void main(String[] args) {

        List<Shape> shapes = new ArrayList<>();
        Shape rectangle = new Rectangle(4, 5);
        Rectangle rectangle2 = new Rectangle(4, 5);
        Shape circle = new Circle(5);
        Shape triangle = new Triangle(2,5);

        shapes.add(rectangle);
        shapes.add(rectangle2);
        shapes.add(circle);
        shapes.add(triangle);
        printShapes(shapes);

    }

    private static void printShapes(List<Shape> shapes) {
        for (Shape shape : shapes) {
            System.out.println("shape.getType() = " + shape.getType());
            shape.print();
            System.out.println("shape.calculateArea() = " + shape.calculateArea() + "\n");
        }
    }
}
