package com.psybergate.grad2021.core.oopart1.inheritance.demo1a;

public class Triangle extends Shape {

    private static final String TYPE = "Triangle";

    private double height;
    private double base;

    public Triangle(double height, double base) {
        this.height = height;
        this.base = base;
    }

    @Override
    public double calcArea() {
        return (base * height) / 2;
    }

    @Override
    public void getType() {
        System.out.println("TYPE = " + TYPE);
    }
}
