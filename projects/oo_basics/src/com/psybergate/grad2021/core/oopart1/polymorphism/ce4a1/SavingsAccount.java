package com.psybergate.grad2021.core.oopart1.polymorphism.ce4a1;

public class SavingsAccount extends Account {

    private static final String ACCOUNT_TYPE = "Savings Account";
    private static final int MIN_BALANCE = 5000;
    public static final int MINIMUM_OVERDRAFT_BALANCE = 2000;


    public SavingsAccount(int accountNum, String bankName) {
        this(accountNum, MIN_BALANCE, bankName);
    }

    public SavingsAccount(int accountNum, int balance, String bankName) {
        super(accountNum, balance, bankName);
    }

    public boolean isOverDrawn() {
        return getBalance() < MIN_BALANCE;
    }

    @Override
    public boolean needsToBeReviewed() {
        return getBalance() < MINIMUM_OVERDRAFT_BALANCE;
    }

    public static String getAccountType() {
        return ACCOUNT_TYPE;
    }
}
