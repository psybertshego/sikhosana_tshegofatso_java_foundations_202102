package com.psybergate.grad2021.core.oopart1.homework.hw5a;

public class CurrentAccount extends Account {

    private int overdraft;
    private static final String ACCOUNT_TYPE = "Current Account";

    public CurrentAccount(int accountNum, int balance, int overdraft) {
        super(accountNum, balance);
        this.overdraft = overdraft;
    }

    public String getAccountType() {
        return ACCOUNT_TYPE;
    }

}