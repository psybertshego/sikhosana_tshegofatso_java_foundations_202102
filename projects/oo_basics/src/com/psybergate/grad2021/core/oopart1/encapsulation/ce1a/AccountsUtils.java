package com.psybergate.grad2021.core.oopart1.encapsulation.ce1a;

public class AccountsUtils {
    public static void main(String[] args) {
        Customer customer = new Customer("Peter",1,"Home");
        customer.addAccount(new CurrentAccount(1,10));
        customer.addAccount(new CurrentAccount(2,200));
        customer.addAccount(new CurrentAccount(3,15000));

        customer.getTotalBalance();

        customer.print();
//        customer.super.print();
    }
}
