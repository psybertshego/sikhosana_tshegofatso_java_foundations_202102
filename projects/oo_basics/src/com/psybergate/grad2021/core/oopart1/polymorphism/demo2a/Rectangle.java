package com.psybergate.grad2021.core.oopart1.polymorphism.demo2a;

public class Rectangle extends Shape {

    private static final String TYPE = "Rectangle";

    private double length;
    private double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public double calculateArea() {
        return length * width;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (length + width);
    }

    @Override
    public void print() {
        System.out.println("length = " + length);
        System.out.println("width = " + width);
    }

    public String getType() {
        return TYPE;
    }
}
