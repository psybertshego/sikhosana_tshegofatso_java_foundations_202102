package com.psybergate.grad2021.core.oopart1.inheritance.ce2a;

public class Account {

    private int accountNum;
    private String name;
    private String surname;
    private double balance;

    public Account(int accountNum, String name, String surname, double balance) {
        this.accountNum = accountNum;
        this.name = name;
        this.surname = surname;
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void depositAmount(int amount){
        balance += amount;
    }

    public void withdrawAmount(int amount){
        balance -= amount;
    }

    public void isOverdrawn(){};
    public void getAccountType(){};
}
