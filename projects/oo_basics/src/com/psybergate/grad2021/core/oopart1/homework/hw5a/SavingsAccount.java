package com.psybergate.grad2021.core.oopart1.homework.hw5a;

public class SavingsAccount extends Account {

    private static final String ACCOUNT_TYPE = "Savings Account";
    private static final int MIN_BALANCE = 5000;
    public static final int MINIMUM_OVERDRAFT_BALANCE = 2000;


    public SavingsAccount(int accountNum, int balance) {
        super(accountNum, balance);
    }

    public boolean isOverDrawn() {
        return getBalance() < MIN_BALANCE;
    }

    public boolean needsToBeReviewed() {
        return getBalance() < MINIMUM_OVERDRAFT_BALANCE;
    }

    public String getAccountType() {
        return ACCOUNT_TYPE;
    }
}
