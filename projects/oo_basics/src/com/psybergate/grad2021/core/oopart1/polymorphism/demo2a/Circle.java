package com.psybergate.grad2021.core.oopart1.polymorphism.demo2a;

public class Circle extends Shape {

    private static final String TYPE = "Circle";
    private double radius;
    private final double pi = 3.14;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * pi * radius;
    }

    @Override
    public double calculateArea() {
        return pi * Math.pow(radius, 2);
    }

    @Override
    public void print() {
        System.out.println("radius = " + radius);
    }

    public String getType() {
        return TYPE;
    }
}
