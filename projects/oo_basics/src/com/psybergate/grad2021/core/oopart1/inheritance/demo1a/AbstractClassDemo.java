package com.psybergate.grad2021.core.oopart1.inheritance.demo1a;

public class AbstractClassDemo {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(12,12);
        Rectangle rectangle = new Rectangle(20,10);

        System.out.println("triangle.calcArea() = " + triangle.calcArea());
        System.out.println("rectangle.calcArea() = " + rectangle.calcArea());
    }
}
