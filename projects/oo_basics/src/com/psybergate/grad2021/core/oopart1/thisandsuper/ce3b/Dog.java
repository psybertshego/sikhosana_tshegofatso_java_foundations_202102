package com.psybergate.grad2021.core.oopart1.thisandsuper.ce3b;

public class Dog extends Pet{

    private String breed;
    private String owner;

    public Dog(String name, String breed, String owner) {
        this(name,breed);
        this.owner = owner;
    }

    public Dog(String name, String breed) {
        super(name);
        this.breed = breed;
    }

    @Override
    public void saySomething() {
        System.out.println("Bark bark");
    }

    public void sayParentSomething() {
        super.saySomething();
    }

    public String getOwner() {
        return owner;
    }
}
