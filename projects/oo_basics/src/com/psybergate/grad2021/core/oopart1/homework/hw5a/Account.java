package com.psybergate.grad2021.core.oopart1.homework.hw5a;

public abstract class Account {

    private int accountNum;
    private int balance;

    public Account(int accountNum, int balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public int getAccountNum() {
        return accountNum;
    }

    public abstract String getAccountType();
}
