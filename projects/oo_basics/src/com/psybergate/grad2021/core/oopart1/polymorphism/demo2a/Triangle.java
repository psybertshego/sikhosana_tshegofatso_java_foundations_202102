package com.psybergate.grad2021.core.oopart1.polymorphism.demo2a;

public class Triangle extends Shape {

    private static final String TYPE = "Triangle";

    private double base;
    private double height;

    public Triangle(double base, double height) {
        super();
        this.base = base;
        this.height = height;
    }

    @Override
    public double calculateArea() {
        return (base / 2) * height;
    }

    @Override
    public double calculatePerimeter() {
        return 0;
    }

    @Override
    public void print() {
        System.out.println("base = " + base);
        System.out.println("height = " + height);
    }

    @Override
    public String getType() {
        return TYPE;
    }
}
