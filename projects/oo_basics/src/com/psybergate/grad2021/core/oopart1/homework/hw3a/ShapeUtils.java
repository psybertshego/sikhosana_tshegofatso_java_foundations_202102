package com.psybergate.grad2021.core.oopart1.homework.hw3a;

public class ShapeUtils {

    public static void main(String[] args) {



        Rectangle rectangle = new Rectangle(200,50);
        System.out.println("rectangle.calArea() = " + rectangle.calArea());

//        Rectangle rect1 = new Rectangle(10,50);
        Rectangle rect2 = new Rectangle(100,25);
        Rectangle rect3 = new Rectangle(130,13);

        System.out.println("rect3.calPerimeter() = " + rect3.calPerimeter());
        System.out.println("rect2.calArea() = " + rect2.calArea());


        /** This code returns the same width for the rectangle
         *  that is in the list and the reference rect.
         *  It changes this because the object that is created when creating the "rect"
         *  object, references list.get(0) object in memory and when you change the attributes in one
         *  it changes for all objects that have it as a reference
         * */

//        List<Rectangle> list = new ArrayList<>();
//        list.add(new Rectangle(201,20));
//        System.out.println("list.get(0) = " + list.get(0).getWidth());
//        Rectangle rect = list.get(0);
//        rect.setWidth(25);
//        System.out.println("rect.getWidth() = " + rect.getWidth());
//        System.out.println("list.get(0) = " + list.get(0).getWidth());

    }
}
