package com.psybergate.grad2021.core.oopart1.homework.hw4a;

public class Customer {
        private String customerNum;
        private String customerAddress;

        public Customer(String customerNum, String customerAddress) {
                this.customerNum = customerNum;
                this.customerAddress = customerAddress;
        }

        public String getCustomerAddress() {
                return customerAddress;
        }

        public String getCustomerNum() {
                return customerNum;
        }
}
