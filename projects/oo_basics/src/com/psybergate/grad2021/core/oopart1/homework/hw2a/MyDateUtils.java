package com.psybergate.grad2021.core.oopart1.homework.hw2a;

public class MyDateUtils {
    public static void main(String[] args) {

        //OO approach
        MyDateOO dateOO = new MyDateOO("20200212");
        dateOO.isValid();
        dateOO.addDays(20);
        dateOO.dateCompare("20210212");

        //PP approach
        String datePP = "20201231";
        MyDatePP.isValid(datePP);
        MyDatePP.compareDates(datePP,"20210204");
        datePP = MyDatePP.addDays(datePP,21);

    }
}
