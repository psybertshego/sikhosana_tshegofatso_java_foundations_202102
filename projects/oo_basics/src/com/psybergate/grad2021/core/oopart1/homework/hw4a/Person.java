package com.psybergate.grad2021.core.oopart1.homework.hw4a;

public class Person extends Customer {
    private static final int MIN_AGE = 18;

    private String name;
    private int age;

    public Person(String customerNum, String customerAddress, String name, int age) {
        super(customerNum, customerAddress);
        try {
            isUnderAge();
            this.age = age;
            this.name = name;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getName() {
        return name;
    }

    private void isUnderAge() throws Exception {
        if (age > MIN_AGE) {
            //throw some exception
            throw new Exception("Person has to be over" + MIN_AGE);
        }
    }
}

