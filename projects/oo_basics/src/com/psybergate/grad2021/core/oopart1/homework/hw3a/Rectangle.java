package com.psybergate.grad2021.core.oopart1.homework.hw3a;

public class Rectangle {

    private static final double MAX_LENGTH = 200;
    private static final double MAX_WIDTH = 100;
    private static final double MAX_AREA = 15000;

    private double length;
    private double width;

    public Rectangle(double length, double width) {
        try {
            checkConstraints(length, width);
            this.length = length;
            this.width = width;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkConstraints(double length, double width) throws Exception {
        if (length > MAX_LENGTH) {
            throw new Exception("cannot create object with length greater than " + MAX_LENGTH);
        } else if (width > MAX_WIDTH) {
            throw new Exception("cannot create object with witdh greater than " + MAX_WIDTH);
        } else if ((length * width) > MAX_AREA) {
            throw new Exception("cannot create object with an area greater than " + MAX_AREA);
        } else if (length < width) {
            throw new Exception("cannot create object with a width greater than length");
        }
    }

    public double calArea() {
        return length * width;
    }

    public double calPerimeter() {
        return 2 * (length + width);
    }


}
