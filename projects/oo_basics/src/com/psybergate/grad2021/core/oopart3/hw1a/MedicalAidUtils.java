package com.psybergate.grad2021.core.oopart3.hw1a;

public class MedicalAidUtils {
  public static void main(String[] args) {
    Employee admin1 = new Administrator("A001", "john", "smith", 120000);
    Employee admin2 = new Administrator("A002", "jane", "doe", 80000);
    Employee admin3 = new Administrator("A003", "gary", "sinise", 250000);
    Employee manager1 = new Manager("A010", "craig", "bart", 300000);
    Employee manager2 = new Manager("A011", "shirley", "norman", 520000);
    Employee director1 = new Director("A020", "vincent", "radebe", 600000);
    Employee director2 = new Director("A021", "sipho", "msimanga", 1_200_000);

    printMedicalAidContributions(admin1, admin2, admin3, manager1, manager2, director1, director2);
  }

  private static void printMedicalAidContributions(Employee... employees) {
    for (Employee employee : employees) {
      System.out.println(employee.getEmployeeType() + "(" +
              employee.getEmployeeFullName() + "): contributes -> " + employee.getMedicalAidContribution());
    }
  }
}
