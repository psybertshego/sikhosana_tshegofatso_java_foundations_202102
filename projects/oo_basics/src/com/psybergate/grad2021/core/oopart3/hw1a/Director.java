package com.psybergate.grad2021.core.oopart3.hw1a;

public class Director extends Employee {

  private static final String EMPLOYEE_TYPE = "Director";

  public Director(String employeeNum, String name, String surname, double annualSalary) {
    super(employeeNum, name, surname, annualSalary);
  }

  @Override
  public double getMedicalAidContribution() {
    return 5000;
  }

  @Override
  public String getEmployeeType() {
    return EMPLOYEE_TYPE;
  }
}
