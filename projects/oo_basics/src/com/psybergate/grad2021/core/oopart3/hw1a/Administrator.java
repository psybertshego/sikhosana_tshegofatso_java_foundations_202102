package com.psybergate.grad2021.core.oopart3.hw1a;

public class Administrator extends Employee {

  private static final String EMPLOYEE_TYPE = "Administrator";
  private final double MIN_CONTRIBUTION = 800;
  private final double MAX_CONTRIBUTION = 2000;
  private final double CONTRIBUTION_PERCENTAGE = 0.1; // 7.5% of their monthly salary

  public Administrator(String employeeNum, String name, String surname, double annualSalary) {
    super(employeeNum, name, surname, annualSalary);
  }

  @Override
  public double getMedicalAidContribution() {
    double contribution = getMonthlySalary() * CONTRIBUTION_PERCENTAGE;
    if (contribution < MIN_CONTRIBUTION) return MIN_CONTRIBUTION;
    if (contribution > MAX_CONTRIBUTION) return MAX_CONTRIBUTION;
    return contribution;
  }

  @Override
  public String getEmployeeType() {
    return EMPLOYEE_TYPE;
  }
}
