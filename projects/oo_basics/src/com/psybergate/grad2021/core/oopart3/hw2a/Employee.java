package com.psybergate.grad2021.core.oopart3.hw2a;

public abstract class Employee {
  private String employeeNum;
  private String name;
  private String surname;
  private double annualSalary;

  public Employee(String employeeNum, String name, String surname, double annualSalary) {
    this.employeeNum = employeeNum;
    this.name = name;
    this.surname = surname;
    this.annualSalary = annualSalary;
  }

  public double getAnnualSalary() {
    return annualSalary;
  }

  public double getMonthlySalary(){
    return annualSalary/12;
  }

  public String getEmployeeFullName(){
    return name + " " + surname;
  }

  public abstract String getEmployeeType();

  public abstract double getMedicalAidContribution();

}
