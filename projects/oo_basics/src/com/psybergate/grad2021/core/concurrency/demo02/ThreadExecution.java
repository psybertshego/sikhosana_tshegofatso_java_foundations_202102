package com.psybergate.grad2021.core.concurrency.demo02;

public class ThreadExecution {
  public static void main(String[] args) throws InterruptedException {

    MyThread myThread1 = new MyThread("keke");
    MyThread myThread2 = new MyThread("lele");



    Thread t1 = new Thread(myThread1);
    Thread t2 = new Thread(myThread2);

    t1.start();
    t2.start();

    t1.join();
    t2.join();

    //Running this program I expected the output be Heads up, run keke since I placed
    // her in first, on execution we can't predict the order in which the threads run
    //
  }
}


