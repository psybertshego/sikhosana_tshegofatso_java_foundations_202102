package com.psybergate.grad2021.core.concurrency.hw1a.polling;

import java.util.List;

public class MessageWriter {

  private MessageCache cache;

  public MessageWriter() {
    cache = MessageCache.getInstance();
  }

  public void write(Message message) {
    System.out.println("Writing message: " + message.getMessage());

    List<Message> messages = cache.getMessages();

    synchronized (messages) {
      messages.add(message);
    }
  }
}
