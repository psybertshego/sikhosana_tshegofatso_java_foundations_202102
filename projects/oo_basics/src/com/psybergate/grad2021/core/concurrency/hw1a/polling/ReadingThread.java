package com.psybergate.grad2021.core.concurrency.hw1a.polling;

public class ReadingThread implements Runnable{

  private MessageReader reader;

  public ReadingThread(MessageReader reader) {
    this.reader = reader;
  }

  @Override
  public void run() {
    while (true){
      System.out.println("Reading messages:");
      try {
        reader.read();
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }
}
