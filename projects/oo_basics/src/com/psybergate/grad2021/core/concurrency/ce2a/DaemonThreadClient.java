package com.psybergate.grad2021.core.concurrency.ce2a;

public class DaemonThreadClient {
  public static void main(String[] args) throws InterruptedException {

    Thread userThread = new Thread(new UserThread());
    Thread daemonThread = new Thread(new DaemonThread(userThread));


    daemonThread.setDaemon(true);

    userThread.start();


    daemonThread.start();


  }
}
