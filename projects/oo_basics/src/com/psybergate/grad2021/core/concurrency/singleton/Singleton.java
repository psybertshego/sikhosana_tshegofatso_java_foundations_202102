package com.psybergate.grad2021.core.concurrency.singleton;

public class Singleton {

  private static final Singleton INSTANCE = new Singleton();


 // private int value = 40;

  private Singleton() {

  }

  public static Singleton getInstance() {
    return INSTANCE;
  }
//
//  public void printName() {
//    System.out.println("Singleton instance");
//  }
//
//  public void add(int amt) {
//    //synchronized (Singleton.class) {
//      if (value < 50) {
//        value += amt;
//      } else {
//        System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
//      }
//    //}
//  }
//
//  public int getValue() {
//    return value;

//  }

}
