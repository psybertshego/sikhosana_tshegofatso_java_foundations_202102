package com.psybergate.grad2021.core.concurrency.hw2a;

public class VolatileRunnable implements Runnable{

  private Increment counter;

  public VolatileRunnable(Increment counter) {
    this.counter = counter;
  }

  @Override
  public void run() {

    synchronized (counter){
      System.out.println(Thread.currentThread().getName() + " -> counter = " + counter.getCount());
      counter.incrementCount();
      System.out.println(Thread.currentThread().getName() + " -> counter = " + counter.getCount());
    }
  }
}
