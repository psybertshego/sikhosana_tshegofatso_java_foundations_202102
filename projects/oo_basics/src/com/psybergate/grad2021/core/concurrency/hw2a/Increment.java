package com.psybergate.grad2021.core.concurrency.hw2a;

public class Increment {

  private volatile int count;

  public Increment(int count) {
    this.count = count;
  }

  public int getCount() {
    return count;
  }

  public void incrementCount(){
    count++;
  }
}
