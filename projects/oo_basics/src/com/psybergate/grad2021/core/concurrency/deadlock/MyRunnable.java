package com.psybergate.grad2021.core.concurrency.deadlock;

public class MyRunnable implements Runnable{

  private static Lock mutex;

  public MyRunnable(Lock mutex) {
    this.mutex = mutex;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " is waiting");

    synchronized (mutex){
      System.out.println(Thread.currentThread().getName());

      if(!mutex.getLock()){
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        mutex.lock();
      }
    }

  }
}
