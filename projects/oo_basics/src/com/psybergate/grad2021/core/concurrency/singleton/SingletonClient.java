package com.psybergate.grad2021.core.concurrency.singleton;

public class SingletonClient {

  public static void main(String[] args) throws InterruptedException {
    Singleton s = Singleton.getInstance();
    Singleton s2 = Singleton.getInstance();

    Worker w1 = new Worker(s);
    Worker w2 = new Worker(s);
    Thread t1 = new Thread(w1);
    Thread t2 = new Thread(w2);

    t1.start();
    t2.start();


    System.out.println("(w1.instance() == s) = " + (w1.instance() == s));
    System.out.println("(w2.instance() == s) = " + (w2.instance() == s2));

    Thread.sleep(1000);

//    System.out.println("s.getValue() = " + s.getValue());

  //  s.printName();
  }
}

class Worker implements Runnable {
  private Singleton instance;


  public Worker(Singleton instance) {
    this.instance = instance;
  }

  @Override
  public void run() {
//    instance.add(20);
  }

  public Singleton instance() {
    return instance;
  }
}
