package com.psybergate.grad2021.core.concurrency.ce3a;

public class Timer {

  private long startTime;
  private long endTime;

  public void start(){
    startTime = System.currentTimeMillis();
  }

  public void stop(){
    endTime = System.currentTimeMillis();
  }

  public long getDuration(){
    return endTime - startTime;
  }

  public void reset() {
    startTime = 0;
    endTime = 0;
  }
}
