package com.psybergate.grad2021.core.concurrency.ce1a;

public class MyThread implements Runnable {


  private SharedObject sharedObject;

  public MyThread(SharedObject sharedObject) {
    this.sharedObject = sharedObject;
  }



  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " : value -> "+ sharedObject.getValue());
    sharedObject.multiplyBy2();
  }
}
