package com.psybergate.grad2021.core.concurrency.demo02;

public class MultipleThreads {
  public static void main(String[] args) throws InterruptedException {
    MyThread myThread1 = new MyThread("Jake");
    MyThread myThread2 = new MyThread("Fin");
    MyThread myThread3 = new MyThread("Princess Bubblegum");

    Thread t1 = new Thread(myThread1);
    Thread t2 = new Thread(myThread2);
    Thread t3 = new Thread(myThread3);

    t1.start();
    t2.start();
    t3.start();

    t1.join();
    t2.join();
    t3.join();
  }
}
