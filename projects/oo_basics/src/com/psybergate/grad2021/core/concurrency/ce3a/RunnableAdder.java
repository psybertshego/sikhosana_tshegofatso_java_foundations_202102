package com.psybergate.grad2021.core.concurrency.ce3a;

public class RunnableAdder implements Runnable{

  private Adder adder;
  private long sum = 0;

  public RunnableAdder(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    sum = adder.sum();
  }

  public long sum() {
    return sum;
  }
}
