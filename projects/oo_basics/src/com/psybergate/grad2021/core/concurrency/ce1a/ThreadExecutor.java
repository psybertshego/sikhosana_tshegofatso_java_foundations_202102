package com.psybergate.grad2021.core.concurrency.ce1a;

public class ThreadExecutor {
  public static void main(String[] args) throws InterruptedException {

    SharedObject s1 = new SharedObject(70);
    SharedObject s2 = new SharedObject(15);
    SharedObject s3 = new SharedObject(100);


    Thread t1 = new Thread(new MyThread(s1));
    Thread t2 = new Thread(new MyThread(s2));
    Thread t3 = new Thread(new MyThread(s3));

    Thread t4 = new Thread(new MyThread(s2));
   // Thread t5 = new Thread(new MyThread(null));

    t1.start();
    t2.start();
    t3.start();
    t4.start();
   // t5.start();

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    //t5.join();

    System.out.println("s1.getValue() = " + s1.getValue());
    System.out.println("s2.getValue() = " + s2.getValue());
    System.out.println("s3.getValue() = " + s3.getValue());
  }
}
