package com.psybergate.grad2021.core.concurrency.ce1a;

public class SharedObject {

  private int value;

  public SharedObject(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public boolean isEven() throws IllegalArgumentException{
    if(this.getValue()%2 == 0){
      return true;
    }
    else{
      throw new IllegalArgumentException();
    }
  }

  public void multiplyBy2() throws IllegalArgumentException{
    if(this.getValue() < 100 && isEven() ){
      this.setValue(this.getValue() *2 );
    }
  }
}
