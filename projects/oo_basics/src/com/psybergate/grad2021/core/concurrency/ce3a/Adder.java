package com.psybergate.grad2021.core.concurrency.ce3a;

public class Adder {

  private long start;

  private long end;

  public Adder(long start, long end) {
    this.start = start;
    this.end = end;
  }

  public long sum(){
    long result = 0;

    for(long i = start; i <= end;i++){
      
      result = result + i;
    }

    return result;
  }
}
