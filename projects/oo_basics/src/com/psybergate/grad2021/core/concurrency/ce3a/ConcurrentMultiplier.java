package com.psybergate.grad2021.core.concurrency.ce3a;

import java.util.ArrayList;
import java.util.List;

public class ConcurrentMultiplier {
  public static void main(String[] args) throws InterruptedException {

    long start = 1;
    long end = 10;

    Timer timer = new Timer();


    timer.start();
    long mainResult = (new Multiplier(start,end)).factorial();
    timer.stop();
    System.out.println("mainResult = " + mainResult + " and it took " + timer.getDuration() + "ms \n ");

    timer.reset();

    timer.start();
    long threadsResult = multiplyProducts(8, start, end);
    timer.stop();
    System.out.println("result = " + threadsResult + " and it took " + timer.getDuration() + "ms");

  }

  private static long multiplyProducts(int numberOfThreads, long lowerBound, long upperBound) throws InterruptedException {
    long range = (long) Math.floor(upperBound / numberOfThreads);
    long endRange = range;

    List<RunnableMultiplier> runnableThreads = new ArrayList<>();
    List<Thread> threads = new ArrayList<>();

    for (int i = 1; i <= numberOfThreads; i++) {

      Multiplier multiplier = new Multiplier(lowerBound,  i == numberOfThreads ? upperBound : endRange );

      lowerBound = endRange + 1;
      endRange += range;

      RunnableMultiplier runnable = new RunnableMultiplier(multiplier);
      runnableThreads.add(runnable);
      threads.add(new Thread(runnable));
    }

    ThreadUtils.startThreads(threads);
    ThreadUtils.joinThreads(threads);

    return getProducts(runnableThreads);
  }

  private static long getProducts(List<RunnableMultiplier> runnableThreads) {

    long product = 1;
    for (RunnableMultiplier runnableThread : runnableThreads) {
      product = product * runnableThread.product();
    }

    return product;
  }
}
