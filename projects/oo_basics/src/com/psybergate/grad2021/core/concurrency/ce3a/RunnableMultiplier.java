package com.psybergate.grad2021.core.concurrency.ce3a;

public class RunnableMultiplier implements Runnable{

  private Multiplier multiplier;
  private long product = 0;

  public RunnableMultiplier(Multiplier multiplier) {
    this.multiplier = multiplier;
  }

  @Override
  public void run() {
    product = multiplier.factorial();
  }

  public long product() {
    return product;
  }
}
