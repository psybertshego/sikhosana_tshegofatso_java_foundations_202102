package com.psybergate.grad2021.core.concurrency.hw1a.nonpolling;


public class MessagesClient {
  public static void main(String[] args) throws InterruptedException {

    MessageWriter writer = new MessageWriter();
    MessageReader reader = new MessageReader();

    Thread writingThread1 = new Thread(new WritingThread(writer));
    Thread writingThread2 = new Thread(new WritingThread(writer));
    Thread writingThread3 = new Thread(new WritingThread(writer));
    Thread readingThread = new Thread(new ReadingThread(reader));

    readingThread.start();
    writingThread1.start();
    writingThread2.start();
    writingThread3.start();

  }
}
