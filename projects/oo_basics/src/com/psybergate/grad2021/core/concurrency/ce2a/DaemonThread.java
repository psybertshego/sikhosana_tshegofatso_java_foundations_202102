package com.psybergate.grad2021.core.concurrency.ce2a;

public class DaemonThread implements Runnable{

  private Thread current;

  public DaemonThread(Thread current) {
    this.current = current;
  }

  @Override
  public void run() {
    System.out.println("Thread.currentThread().getName() = " + Thread.currentThread().getName());
    boolean status = true;
    int count = 0;
    while (status){
      if(current.isAlive()){
        count++;
        System.out.println("count = " + count);
      }
      else{
        status = false;
      }
    }
    System.out.println("I am a daemon thread");
  }
}
