package com.psybergate.grad2021.core.concurrency.ce3a;

import java.util.List;

public class ThreadUtils {


  public static void startThreads(List<Thread> threads) {
    for (Thread thread : threads) {
      thread.start();
    }
  }

  public static void joinThreads(List<Thread> threads) throws InterruptedException {
    for (Thread thread : threads) {
      thread.join();
    }
  }
}
