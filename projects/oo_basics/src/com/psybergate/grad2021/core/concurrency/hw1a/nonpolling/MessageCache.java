package com.psybergate.grad2021.core.concurrency.hw1a.nonpolling;

import java.util.ArrayList;
import java.util.List;


//Singleton Class
public class MessageCache {

  private static MessageCache instance = new MessageCache();

  private List<Message> messages = new ArrayList<>();

  private MessageCache(){

  }

  public static MessageCache getInstance() {
    return instance;
  }

  public List<Message> getMessages() {
    return messages;
  }


}
