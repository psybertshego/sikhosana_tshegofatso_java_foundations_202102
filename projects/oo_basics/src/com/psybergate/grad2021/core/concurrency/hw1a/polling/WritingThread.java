package com.psybergate.grad2021.core.concurrency.hw1a.polling;

import java.util.Random;

public class WritingThread implements Runnable{

  private MessageWriter writer;

  public WritingThread(MessageWriter writer) {
    this.writer = writer;
  }

  @Override
  public void run() {

    for (int i = 0; i < 10; i++) {
      try {
//        System.out.println("Writing message:");
        writer.write(new Message("hello world " + new Random().nextInt(200)  + " -> " +Thread.currentThread().getName()));
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
