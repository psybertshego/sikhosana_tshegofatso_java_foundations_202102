package com.psybergate.grad2021.core.concurrency.demo01;

public class ThreadCreator {

  public static void main(String[] args) throws InterruptedException {

    RunnableObject runnableObject = new RunnableObject();

    Thread runnableThread = new Thread(runnableObject);


    ObjectThread threadObject = new ObjectThread();

    runnableThread.start();
    threadObject.start();

    runnableThread.join();
    threadObject.join();

  }


}

class RunnableObject implements Runnable {

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName());
    System.out.println("hello world from runnable object");
  }
}

class ObjectThread extends Thread {

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName());
    System.out.println("hello world from object thread");
  }
}