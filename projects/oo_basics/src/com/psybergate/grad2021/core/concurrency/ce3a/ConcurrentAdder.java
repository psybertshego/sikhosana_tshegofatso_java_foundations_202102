package com.psybergate.grad2021.core.concurrency.ce3a;

import java.util.ArrayList;
import java.util.List;

public class ConcurrentAdder {

  public static void main(String[] args) throws InterruptedException {


    long start = 1;
    long end = 1_000_000_000;

    Timer timer = new Timer();


    timer.start();
    long mainResult = sumUpRangeMain(start,end);
    timer.stop();
    System.out.println("mainResult = " + mainResult + " and it took " + timer.getDuration() + "ms \n ")  ;

    timer.reset();

    timer.start();
    long threadsResult = sumUpRange(16, start, end);
    timer.stop();
    System.out.println("result = " + threadsResult + " and it took " + timer.getDuration() + "ms");
  }

  private static long sumUpRangeMain( long lowerBound, long upperBound) throws InterruptedException {

    long result = 0;

    for (long i = lowerBound; i <= upperBound; i++) {
      result = result + i;
    }

    return result;
  }

  private static long sumUpRange(int numberOfThreads, long lowerBound, long upperBound) throws InterruptedException {

    long range = (long) Math.floor(upperBound / numberOfThreads);
    long endRange = range;

    List<RunnableAdder> runnableAdders = new ArrayList<>();
    List<Thread> threads = new ArrayList<>();

    for (int i = 1; i <= numberOfThreads; i++) {

      Adder adder = new Adder(lowerBound,  i == numberOfThreads ? upperBound : endRange );

      lowerBound = endRange + 1;
      endRange += range;

      RunnableAdder runnableAdder = new RunnableAdder(adder);
      runnableAdders.add(runnableAdder);
      threads.add(new Thread(runnableAdder));
    }

    ThreadUtils.startThreads(threads);
    ThreadUtils.joinThreads(threads);

    return getTotalSum(runnableAdders);
  }

  private static final long getTotalSum(List<RunnableAdder> runnableAdders) {
    long totalSum = 0;

    for (RunnableAdder runnableAdder : runnableAdders) {
      totalSum += runnableAdder.sum();
    }

    return totalSum;
  }
}
