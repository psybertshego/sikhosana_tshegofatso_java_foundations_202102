package com.psybergate.grad2021.core.concurrency.hw1a.nonpolling;

import java.util.Random;

public class WritingThread implements Runnable{

  private MessageWriter writer;

  public WritingThread(MessageWriter writer) {
    this.writer = writer;
  }

  @Override
  public void run() {

    for (int i = 0; i < 5; i++) {
      try {
//        System.out.println("Writing message:");
        writer.write(new Message("hello world " + new Random().nextInt(200)  + " -> " +Thread.currentThread().getName()));
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
