package com.psybergate.grad2021.core.concurrency.hw1a.polling;

import java.time.LocalTime;

public class Message {

  private String message;

  private LocalTime timestamp;

  public Message(String message) {
    this.message = message;
    this.timestamp = LocalTime.now();
  }

  public String getMessage() {
    return message + " : " + timestamp;
  }
}
