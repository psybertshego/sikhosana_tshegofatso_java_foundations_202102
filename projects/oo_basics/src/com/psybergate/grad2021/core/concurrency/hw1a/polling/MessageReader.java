package com.psybergate.grad2021.core.concurrency.hw1a.polling;

import java.util.List;

public class MessageReader {

  private MessageCache cache;

  public MessageReader() {
    cache = MessageCache.getInstance();
  }

  public void read() throws InterruptedException {
    List<Message> messages = cache.getMessages();
    synchronized (messages){
      while(messages.size() > 0){
        System.out.println(messages.remove(0).getMessage());
      }
    }
  }
}
