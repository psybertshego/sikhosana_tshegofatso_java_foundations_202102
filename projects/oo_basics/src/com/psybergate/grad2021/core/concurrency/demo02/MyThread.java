package com.psybergate.grad2021.core.concurrency.demo02;

public class MyThread implements Runnable{

  private String name;

  public MyThread(String name) {
    this.name = name;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName());
    System.out.println("Heads up, run " + getName() + "!!");
  }

  public String getName() {
    return name;
  }
}
