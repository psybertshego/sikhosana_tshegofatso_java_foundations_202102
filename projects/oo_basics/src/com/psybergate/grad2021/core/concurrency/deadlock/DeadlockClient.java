package com.psybergate.grad2021.core.concurrency.deadlock;

public class DeadlockClient {
  public static void main(String[] args) throws InterruptedException {

    Lock lock = new Lock();
    Thread t1 = new Thread(new MyRunnable(lock));
    Thread t2 = new Thread(new MyRunnable(lock));

    t1.start();
    t2.start();

    t1.join();
    t2.join();
  }
}
