package com.psybergate.grad2021.core.concurrency.hw1a.nonpolling;

public class ReadingThread implements Runnable{

  private MessageReader reader;

  public ReadingThread(MessageReader reader) {
    this.reader = reader;
  }

  @Override
  public void run() {
    while (true){
      try {
        reader.read();
        //Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }
}
