package com.psybergate.grad2021.core.concurrency.hw2a;

public class VolatileClient {
  public static void main(String[] args) throws InterruptedException {

    Increment i = new Increment(0);

    Thread t1 = new Thread(new VolatileRunnable(i));
    Thread t2 = new Thread(new VolatileRunnable(i));

    t1.start();
    t2.start();
  }
}
