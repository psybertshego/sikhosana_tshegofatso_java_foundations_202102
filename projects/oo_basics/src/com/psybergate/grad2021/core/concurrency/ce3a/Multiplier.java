package com.psybergate.grad2021.core.concurrency.ce3a;

public class Multiplier {

  private long start;
  private long end;

  public Multiplier(long n) {
    this(1, n);
  }

  public Multiplier(long start, long end) {
    this.start = start;
    this.end = end;
  }

  public long factorial() {
    long result = 1;

    for (long i = start; i <= end; i++) {
      result = result * i;
    }

    return result;
  }


}
