package com.psybergate.grad2021.core.concurrency.deadlock;

public class Lock {
  private boolean mutex;

  public Lock() {
    this.mutex = false;
  }

  public void lock(){
    mutex = true;
  }

  public boolean getLock(){
    return mutex;
  }
}
