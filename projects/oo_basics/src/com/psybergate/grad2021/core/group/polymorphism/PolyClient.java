package com.psybergate.grad2021.core.group.polymorphism;

public class PolyClient {
  public static void main(String[] args) {

    Person p = new Person("Knowledge");

    Student s = new Student("Knowledge",120032);

    System.out.println("p.getName() = " + p.getName());

    System.out.println("s.getName() = " + s.getName());

    Person p2 = new Student("Knowledge",120032);
    System.out.println("p2.getName() = " + p2.getName());

    p.printName();

    s.printName();

    p2.printName();

  }
}
