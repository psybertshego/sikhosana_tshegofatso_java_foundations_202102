package com.psybergate.grad2021.core.group.polymorphism;

public class Person implements Print{

  private String name;

  public Person(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public void printName() {
    System.out.println("In person");
  }
}
