package com.psybergate.grad2021.core.group.polymorphism;

public class Student extends Person{

  private int studentNum;

  public Student(String name, int studentNum) {
    super(name);
    this.studentNum = studentNum;
  }

  @Override
  public String getName() {
    return super.getName() + ":" + studentNum;
  }

  @Override
  public void printName() {
    System.out.println("in student class");
  }
}
