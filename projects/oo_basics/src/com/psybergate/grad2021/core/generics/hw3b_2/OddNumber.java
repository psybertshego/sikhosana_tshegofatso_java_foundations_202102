package com.psybergate.grad2021.core.generics.hw3b_2;

public class OddNumber implements GenericType<Integer>{
  @Override
  public boolean check(Integer number) {
    return number % 2 != 0;
  }
}
