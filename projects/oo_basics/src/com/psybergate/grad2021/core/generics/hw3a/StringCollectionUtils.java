package com.psybergate.grad2021.core.generics.hw3a;

import java.util.List;

public class StringCollectionUtils {
  public static int countLetterOccurrences(List<String> list, char letter){
    int count = 0;
    for (String s : list) {
      if(s.toCharArray()[0] == letter ){
        count++;
      }
    }
    return count;
  }
}
