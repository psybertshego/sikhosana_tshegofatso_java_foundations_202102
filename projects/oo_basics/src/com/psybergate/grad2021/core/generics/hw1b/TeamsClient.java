package com.psybergate.grad2021.core.generics.hw1b;

public class TeamsClient {
  public static void main(String[] args) {
    Team team1 = new Team("A-team");
    team1.addMember(new Person("john"));
    team1.addMember(new Student("sbu",1525976));
    team1.printMembers();

    TeamAfterErasure team2 = new TeamAfterErasure("B-team");
    team2.addMember(new Person("jake"));
    team2.addMember(new Student("thabo",10002));
    team2.printMembers();
  }
}
