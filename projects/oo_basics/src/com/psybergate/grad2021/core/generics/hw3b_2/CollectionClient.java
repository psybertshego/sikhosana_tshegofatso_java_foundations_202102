package com.psybergate.grad2021.core.generics.hw3b_2;

import com.psybergate.grad2021.core.generics.hw3b.IntegerCollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CollectionClient {
  public static void main(String[] args) {

    List<Integer> numList = IntegerCollectionUtils.initializeRandomIntegersList();
    System.out.println("Even Numbers = " + countOccurrences(numList, new EvenNumber()));
    System.out.println("Odd Numbers = " + countOccurrences(numList, new OddNumber()));
    System.out.println("Prime Numbers = " + countOccurrences(numList, new PrimeNumber()));

    List<String> wordList = initializeWords();
    System.out.println("StringBeginsWith('c') = " + countOccurrences(wordList, new StringBeginsWith('c')));

    List<Account> accountList = AccountsManager.initialRandomAccounts();
    System.out.println("OverdrawnAccount() = " + countOccurrences(accountList, new OverdrawnAccount()));
  }

  private static <T extends Object, P extends GenericType<T>> int countOccurrences(Collection<T> collection, P constraint) {
    int count = 0;
    for (Iterator<T> i = collection.iterator(); i.hasNext(); ) {
      if (constraint.check(i.next())) {
        count++;
      }
    }
    return count;
  }

  private static List<String> initializeWords() {
    List<String> stringList = new ArrayList<>();
    stringList.add("cat");
    stringList.add("car");
    stringList.add("cap");
    stringList.add("beanie");
    stringList.add("hat");
    return stringList;
  }

  private static <T> void printList(Collection<T> coll) {
    for (Object o : coll) {
      System.out.println(o.toString());
    }
  }

}
