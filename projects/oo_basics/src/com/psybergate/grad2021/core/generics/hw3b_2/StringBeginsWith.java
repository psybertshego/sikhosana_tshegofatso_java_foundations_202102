package com.psybergate.grad2021.core.generics.hw3b_2;

public class StringBeginsWith implements GenericType<String>{
  private char character;
  public StringBeginsWith(char c) {
    this.character = c;
  }

  @Override
  public boolean check(String word) {
    return word.charAt(0) == character;
  }
}
