package com.psybergate.grad2021.core.generics.cars.example;

public class Dealership<T extends Vehicle>{
  private T t;

  public Dealership() {
  }

  public void setT(T t) {
    this.t = t;
  }

  public T getT() {
    return t;
  }
}
