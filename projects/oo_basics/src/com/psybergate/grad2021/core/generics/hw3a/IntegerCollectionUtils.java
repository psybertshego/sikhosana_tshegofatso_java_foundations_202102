package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class IntegerCollectionUtils {
  public static int oddNumberOccurrences(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      if (integer.intValue() % 2 != 0) {
        count++;
      }
    }
    return count;
  }

  public static int evenNumberOccurrences(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      if (integer.intValue() % 2 == 0) {
        count++;
      }
    } 
    return count;
  }

  public static int primeNumberOccurrences(Collection<Integer> coll) {
    int count = 0;
    for (Integer integer : coll) {
      if (isPrime(integer)) {
        count++;
      }
    }
    return count;
  }

  public static boolean isPrime(Integer num) {
    for (int i = 2; i <= num.intValue() / 2; ++i) {
      if (num.intValue() % i == 0) {
        return false;
      }
    }
    return true;
  }

  public static List<Integer> initialRandomIntegersList() {
    List<Integer> list = new ArrayList();

    Random random = new Random();

    int size = random.nextInt(10) + 10;

    for (int i = 0; i < size; i++) {
      list.add(random.nextInt(100) + 1);
    }
    return list;
  }
}
