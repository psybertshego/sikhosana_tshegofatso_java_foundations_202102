package com.psybergate.grad2021.core.generics.hw1b;

public class Student extends Person{
  private float studentNumber;

  public Student(String name, float studentNumber) {
    super(name);
    this.studentNumber = studentNumber;
  }

  public float getStudentNumber() {
    return studentNumber;
  }
}
