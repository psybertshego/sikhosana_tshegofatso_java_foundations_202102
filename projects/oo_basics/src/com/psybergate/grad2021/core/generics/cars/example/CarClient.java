package com.psybergate.grad2021.core.generics.cars.example;

public class CarClient {
  public static void main(String[] args) {
    Dealership<Truck> truck = new Dealership<>();
    truck.setT(new Truck());
    Object obj = truck.getT();


//    trucks.setT(new SportsCar()); // won't compile because a sports car is not a truck

  }
}
