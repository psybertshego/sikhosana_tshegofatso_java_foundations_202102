package com.psybergate.grad2021.core.generics.example;

public class BoxClient {
  public static void main(String[] args) {
    NonGenericBox nonGenericBox = new NonGenericBox();

    Box<String> box = new Box<>();
    box.set("calc");
    String str = box.get();
    nonGenericBox.setObject(1);

//    String str = box.getObject(); //won't compile because we returning object

    Object obj = nonGenericBox.getObject();
    System.out.println("obj = " + obj);
    System.out.println("str = " + str);
  }
}
