package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.List;

public class AccountsManager {
  public static List<Account> initialRandomAccounts() {
    List<Account> accounts = new ArrayList<>();
    accounts.add(new Account("A0002", 100));
    accounts.add(new Account("A0030", -200));
    accounts.add(new Account("A0023", 0));
    accounts.add(new Account("A0056", -20));
    accounts.add(new Account("A0100", 10101));
    accounts.add(new Account("A0430", 350));
    return accounts;
  }


  public static int accountsOverdrawn(List<Account> accounts) {
    int count = 0;
    for (Account account : accounts) {
      if (account.getBalance() < 0) {
        count++;
      }
    }
    return count;
  }

}
