package com.psybergate.grad2021.core.generics.hw3b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CollectionsClient {
  public static void main(String[] args) {
    List<Integer> numbersList = IntegerCollectionUtils.initializeRandomIntegersList();
    printList(numbersList);
    System.out.println("OddNumberOccurrences(numbersList) = " + getOccurrences(numbersList,1));
    System.out.println("EvenNumberOccurrences(numbersList) = " + getOccurrences(numbersList,2));
    System.out.println("PrimeNumberOccurrences(numbersList) = " + getOccurrences(numbersList,3));

    List<String> stringList = new ArrayList<>();
    stringList.add("cat");
    stringList.add("car");
    stringList.add("cap");
    stringList.add("beanie");
    stringList.add("hat");

    //getOccurrences(numbersList,5);
    //getOccurrences(stringList,"c");
    int stringOccurrences = getOccurrences(stringList, "search");
    printList(stringList);
    System.out.println("stringOccurrences = " + stringOccurrences);

    List<Account> accounts = AccountsManager.initialRandomAccounts();
    System.out.println("accountsOverdrawn(accounts) = " + AccountsManager.accountsOverdrawn(accounts));
  }

  private static <T extends Object> int getOccurrences(Collection<T> coll, Object parameter) {
    int counter = 0;
    for (Iterator i = coll.iterator(); i.hasNext(); ) {
      if (parameter instanceof String) {
        if (containsCharacter((String) i.next(), "c")) {
          counter++;
        }
      } else if (parameter instanceof Integer) {
        int option = (((Integer) parameter).intValue());
        if (option == 1) {
          if(!isEven((Integer) i.next())){
            counter++;
          }
        } else if (option == 2) {
          if(isEven((Integer) i.next())){
            counter++;
          }
        } else if (option == 3) {
          if(isPrime((Integer) i.next())){
            counter++;
          }
        }
      }
    }
    return counter;
  }

  private static boolean isEven(Integer integer) {
    return integer.intValue() % 2 == 0;
  }

  public static boolean isPrime(Integer num) {
    for (int i = 2; i <= num.intValue() / 2; ++i) {
      if (num.intValue() % i == 0) {
        return false;
      }
    }
    return true;
  }

  private static boolean containsCharacter(String word, String character) {
    return word.charAt(0) == character.charAt(0);
  }

  //print method uses raw types
  private static <T extends Object> void printList(Collection<T> coll) {
    for (Object o : coll) {
      System.out.println(o.toString());
    }
  }

}
