package com.psybergate.grad2021.core.generics.hw3b;

import java.util.List;

public class StringCollectionUtils {
  public static int countLetterOccurrences(List<String> list, String letter){
    int count = 0;
    for (String s : list) {
      if(containsCharacter(letter, s)){
        count++;
      }
    }
    return count;
  }

  private static boolean containsCharacter(String letter, String s) {
    return s.toCharArray()[0] == letter.charAt(0);
  }
}
