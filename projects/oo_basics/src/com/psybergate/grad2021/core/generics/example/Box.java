package com.psybergate.grad2021.core.generics.example;

public class Box <T>{ // T is the type variable

  private T t;

  public Box() {
  }

  public void set(T t) {
    this.t = t;
  }

  public T get() {
    return t;
  }


}
