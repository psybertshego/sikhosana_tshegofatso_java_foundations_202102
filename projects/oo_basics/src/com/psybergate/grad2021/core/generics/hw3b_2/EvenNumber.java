package com.psybergate.grad2021.core.generics.hw3b_2;

public class EvenNumber implements GenericType<Integer>{


  // Compiler generates and the delegates to the bottom method
  // It does this to uphold polymorphism behaviour and
  // this method is called a Bridging method
//  public boolean check(Object number){
//    return check((Integer) number);
//  }

  @Override
  public boolean check(Integer number) {
    return number % 2 == 0;
  }
}
