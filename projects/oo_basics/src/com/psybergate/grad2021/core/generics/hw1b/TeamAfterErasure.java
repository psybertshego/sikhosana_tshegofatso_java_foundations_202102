package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class TeamAfterErasure {

  private String name;
  private List<Person> members;


  public TeamAfterErasure(String name) {
    this.name = name;
    this.members = new ArrayList<>();
  }

  public void addMember(Person person){
    members.add(person);
  }

  public void printMembers(){
    for (Person member : members) {
      System.out.println(member.getName());
    }
  }
}
