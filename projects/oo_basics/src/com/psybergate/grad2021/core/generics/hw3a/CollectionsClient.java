package com.psybergate.grad2021.core.generics.hw3a;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CollectionsClient {
  public static void main(String[] args) {
    List<Integer> numbersList = IntegerCollectionUtils.initialRandomIntegersList();
    printList(numbersList);
    System.out.println("OddNumberOccurrences(numbersList) = " + IntegerCollectionUtils.oddNumberOccurrences(numbersList));
    System.out.println("EvenNumberOccurrences(numbersList) = " + IntegerCollectionUtils.evenNumberOccurrences(numbersList));
    System.out.println("PrimeNumberOccurrences(numbersList) = " + IntegerCollectionUtils.primeNumberOccurrences(numbersList));

    List<String> stringList = new ArrayList<>();
    stringList.add("cat");
    stringList.add("car");
    stringList.add("cap");
    stringList.add("beanie");
    stringList.add("hat");

    printList(stringList);
    System.out.println("stringOccurrences = " + StringCollectionUtils.countLetterOccurrences(stringList, 'b'));

    List<Account> accounts = AccountsManager.initialRandomAccounts();
    System.out.println("accountsOverdrawn(accounts) = " + AccountsManager.accountsOverdrawn(accounts));
  }

  //print method uses raw types
  private static <T extends Object> void printList(Collection<T> coll) {
    System.out.println("numbers.size() = " + coll.size());
    for (Object o : coll) {
      System.out.println(o.toString());
    }
  }

}
