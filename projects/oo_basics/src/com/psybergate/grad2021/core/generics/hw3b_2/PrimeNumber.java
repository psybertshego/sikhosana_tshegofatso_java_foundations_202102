package com.psybergate.grad2021.core.generics.hw3b_2;

public class PrimeNumber implements GenericType<Integer>{
  @Override
  public boolean check(Integer number) {
    for (int i = 2; i <= number.intValue() / 2; ++i) {
      if (number.intValue() % i == 0) {
        return false;
      }
    }
    return true;
  }
}
