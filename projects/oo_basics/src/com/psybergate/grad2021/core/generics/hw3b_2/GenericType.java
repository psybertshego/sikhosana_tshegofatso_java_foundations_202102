package com.psybergate.grad2021.core.generics.hw3b_2;

public interface GenericType<T>{
  boolean check(T type);
}
