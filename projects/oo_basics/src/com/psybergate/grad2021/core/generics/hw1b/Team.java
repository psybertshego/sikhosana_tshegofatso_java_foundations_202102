package com.psybergate.grad2021.core.generics.hw1b;

import java.util.ArrayList;
import java.util.List;

public class Team <T extends Person>{

  private String name;
  private List<T> members;
//
//  public Team() {
//    this.members = new ArrayList<>();
//  }

  public Team(String name) {
    this.name = name;
    this.members = new ArrayList<>();
  }

  public void addMember(T t){
    members.add(t);
  }

  public void printMembers(){
    for (T member : members) {
      System.out.println(member.getName());
    }
  }
}
