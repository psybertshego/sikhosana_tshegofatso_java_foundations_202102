package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {
  public static void main(String[] args) {
    List<String> stringList = new ArrayList<>();
    stringList.add("abc");
    stringList.add("def");

    stringList = reverseListAfterErasure(stringList);

    for (String s : stringList) {
      System.out.println(s);
    }
  }

  private static List reverseListAfterErasure(List list){
    List newList = new ArrayList();
    for (Object o : list) {
      newList.add(reverseString(o.toString()));
    }
    return newList;
  }

  private static List<String> reverseList(List<String> list) {
    List<String> reversedList = new ArrayList<>();
    for (String s : list) {
      reversedList.add(reverseString(s));
    }
    return reversedList;
  }

  private static String reverseString(String s) {
    String[] characters = s.split("");
    String reversedString = "";
    for (int i = characters.length - 1; i >= 0; i--) {
      reversedString = reversedString + characters[i];
    }
    return reversedString;
  }

}
