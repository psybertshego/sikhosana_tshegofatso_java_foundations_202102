package com.psybergate.grad2021.core.generics.hw2a;

import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    List<Integer> list = new ArrayList<>();
    list.add(1000);
    list.add(450000);
    list.add(200);
    list.add(20003);
    list.add(540000);
    list = GreaterThan1000.greaterThan1000(list);
    printList(list);
  }

  private static void printList(List<Integer> list) {
    for (int i = 0; i < list.size(); i++) {
      System.out.println(list.get(i));
    }
  }
}
