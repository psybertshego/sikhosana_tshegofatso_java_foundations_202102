package com.psybergate.grad2021.core.generics.hw1a;

import java.util.HashMap;
import java.util.Map;

public class MyMapClient {
  public static void main(String[] args) {
    MyMap<Integer,String> map = new MyMap(1,"abc");
    MyMapAfterErasure mapAfterErasure = new MyMapAfterErasure(2,"def");
  }
}
