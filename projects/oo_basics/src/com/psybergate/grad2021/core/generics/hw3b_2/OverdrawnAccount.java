package com.psybergate.grad2021.core.generics.hw3b_2;

public class OverdrawnAccount implements GenericType<Account> {
  @Override
  public boolean check(Account account) {
    return account.getBalance() < 0;
  }
}
