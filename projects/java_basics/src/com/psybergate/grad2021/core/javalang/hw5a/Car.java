package com.psybergate.grad2021.core.javalang.hw5a;

public class Car {
    private String name;
    private int topSpeed;


    public Car(String name) {
        this.name = name;
        this.topSpeed = 200;
    }

    public String getName() {
        return name;
    }

    void getTopSpeed(){
        System.out.println("Top speed is "+topSpeed);
    }
}
