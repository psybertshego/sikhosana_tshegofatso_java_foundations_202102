package com.psybergate.grad2021.core.javalang.hw4a;

public class Operators {

    public static void main(String[] args) {

        Person person1 = new Person(23, "Tshego", true);
        Person person2 = new Person(24, "Galaxy", false);
        Person person3 = person1;
        Person person4 = new Person(18, "Thato", true);

        person3.secret();

        if (person1.getAge() % 2 == 0) {
            System.out.println(person1.getName() + " has an even age:" + person1.getAge());
        } else {
            System.out.println(person1.getName() + " has an odd age: " + person1.getAge());
        }

        if (isPersonIdentical(person1, person3)) {
            System.out.println("Twins");
        } else {
            System.out.println("Not Twins");
        }

        //isLegal(person1);
        bothSkaters(person1, person2);
        myBirthday(person4);
    }

    private static int myBirthday(Person person) {
        int age = person.getAge();
        age += 1;
        System.out.println(person.getName() + " is now " + age);
        return age;
    }

    private static void bothSkaters(Person person1, Person person2) {
        if (person1.isSkater() && person2.isSkater()) {
            System.out.println(person1.getName() + " and " + person2.getName() + " are skaters");
        } else {
            System.out.println(person1.getName() + " and " + person2.getName() + " are both not skaters");
        }


    }

    private static boolean isPersonIdentical(Person person1, Person person2) {
        return person1 == person2 ? true : false;
    }


    private static boolean isLegal(Person person) {

        boolean b;

        switch (person.getAge()) {
            case 18:
                System.out.println(person.getName() + " is legal");
                b = true;
                break;
            case 16:
                System.out.println(person.getName() + " has 2 more years before they can vote");
                b = false;
                break;
            case 21:
                System.out.println(person.getName() + "is legal");
                b = true;
                break;
            default:
                b = false;
                break;
        }
        return b;
    }
}
