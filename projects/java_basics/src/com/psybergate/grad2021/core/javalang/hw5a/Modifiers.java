package com.psybergate.grad2021.core.javalang.hw5a;

import com.psybergate.grad2021.core.javalang.hw4a.Person;

public class Modifiers extends Person {


    public Modifiers(int age, String name, boolean isSkater) {
        super(age, name, isSkater);
        secret();
        System.out.println(this.age);
    }

    public static void main(String[] args) {
        Person person = new Person(21, "Jane", false);

        //  person.secret();
        Car car = new Car("Audi");
        System.out.println(car.getName());
        car.getTopSpeed();
    }
}
