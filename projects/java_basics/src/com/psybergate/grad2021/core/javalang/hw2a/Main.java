package com.psybergate.grad2021.core.javalang.hw2a;

import com.psybergate.grad2021.core.javalang.hw4a.Person;

public class Main {

    static String name;


    static {
        name = "Richard";
    }

    static String name2;

    static {
        name2 = "Peter";
        System.out.println("Initializer");
        System.out.println(name2);
    }

    public Main() {
        Person person1 = new Person(18,"Zee",true);
        name = "Thabo";
    }

    public static void main(String[] args) {
        System.out.println(name);
    }
}
