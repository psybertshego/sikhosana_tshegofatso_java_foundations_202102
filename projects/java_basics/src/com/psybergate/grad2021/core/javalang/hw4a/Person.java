package com.psybergate.grad2021.core.javalang.hw4a;

public class Person {

    protected int age;
    private String name;
    private boolean isSkater;

    public Person(int age, String name, boolean isSkater) {
        this.age = age;
        this.name = name;
        this.isSkater = isSkater;
    }

    public Person() {
        System.out.println(new Person().age);
    }

    public int getAge() {
        new Person();
        return age;
    }

    public String getName() {
        return name;
    }

    public boolean isSkater() {
        return isSkater;
    }

    void favourite() {
        System.out.println("My favourite car is a Mercedes C63 AMG");
    }

    protected void secret() {
        System.out.println("I also have a secret");
    }
}
