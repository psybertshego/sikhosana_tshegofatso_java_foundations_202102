<html>

    <%@ page import =  "java.io.*,java.util.*"
    %>

    <body style="background-color:whitesmoke">

        <%@ include file = "Menu.jsp"%>
        <h1> All the HTTP Header Names and Values</h1>
        <%    //Loop through header names
            String brk = "<br>";
            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()){
                String name = headerNames.nextElement();
                out.print( name + " : ");
                out.print(request.getHeader(name) + brk + brk );
            }
        %>

         <h2> Http Method </h2>
            <%= "getMethod() = " + request.getMethod() + brk %>
        <h2> Protocol </h2>
            <%= "getProtocol() = " + request.getProtocol() + brk %>
        <h2> Request Uri </h2>
            <%="getrequestURI() = " + request.getRequestURI() + brk %>
        <h2> PathInfo </h2>
           <%= "getPathInfo() = " + request.getPathInfo() + brk %>
       
    </body>

</html>