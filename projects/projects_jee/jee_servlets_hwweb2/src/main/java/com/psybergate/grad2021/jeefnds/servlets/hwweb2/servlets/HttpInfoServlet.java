package com.psybergate.grad2021.jeefnds.servlets.hwweb2.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;


@WebServlet(name = "httpinfo", urlPatterns = "/httpinfo")
public class HttpInfoServlet extends HttpServlet {

  public HttpInfoServlet() {
    System.out.println("We in HttpInfoServlet");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String brk = "<br>";
    PrintWriter writer = resp.getWriter();

    writer.write("<h2> All the HTTP Header Names and Values </h2>");
    //Loop through header names
    Enumeration<String> headerNames = req.getHeaderNames();
    while (headerNames.hasMoreElements()){
      String name = headerNames.nextElement();
      writer.write(name  + " : ");
      writer.write(req.getHeader(name) + brk + brk );
    }

    writer.write("<h2> Protocol </h2>");
    writer.write("getProtocol() = " + req.getProtocol() + brk);
    writer.write("<h2> Http Method </h2>");
    writer.write("getMethod() = " + req.getMethod() + brk);
    writer.write("<h2> Request Uri </h2>");
    writer.write("getRequestURI() = " + req.getRequestURI() + brk);
    writer.write("<h2> PathInfo </h2>");
    writer.write("getPathInfo() = " + req.getPathInfo() + brk);
  }
}
