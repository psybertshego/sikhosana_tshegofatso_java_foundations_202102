package com.psybergate.grad2021.jeefnds.enterprise.hwent3.controllers;

import javax.inject.Named;
import java.time.LocalDate;


@Named
public interface Controller {

  public default String errorMessage(){

    StringBuilder response = new StringBuilder();
    response.append("<html>");
    response.append("<body>");
    response.append("<h1> ERROR 404, page not found on server </h1>");
    response.append("<br> Error occurred at : " + LocalDate.now());
    response.append("<body>");
    response.append("</html>");

    return response.toString();
  }
}
