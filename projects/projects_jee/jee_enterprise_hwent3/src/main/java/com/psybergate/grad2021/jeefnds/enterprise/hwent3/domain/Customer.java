package com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @Column
  private String customerNum;

  @Column
  private String name;

  @Column
  private String surname;

  @Column
  private LocalDate dateOfBirth;

  public Customer() {
  }

  public Customer(String customerNum, String name, String surname, LocalDate dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }


  @Override
  public String toString() {
    return "Customer{" +
            "id=" + id +
            ", customerNum='" + customerNum + '\'' +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", dateOfBirth=" + dateOfBirth +
            '}';
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }
}
