package com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Audit;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Named
@ApplicationScoped
public class AuditResource {

  @PersistenceContext(unitName = "CustomerAuditJPA")
  private EntityManager entityManager;

  public void saveAudit(Audit audit){
    entityManager.persist(audit);
  }
}
