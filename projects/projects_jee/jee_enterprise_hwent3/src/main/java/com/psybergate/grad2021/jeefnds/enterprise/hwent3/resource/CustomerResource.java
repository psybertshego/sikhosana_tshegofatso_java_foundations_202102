package com.psybergate.grad2021.jeefnds.enterprise.hwent3.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Customer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Named
@ApplicationScoped
public class CustomerResource {

  @PersistenceContext(unitName = "CustomerAuditJPA")
  private EntityManager entityManager;

  public void saveCustomer(Customer customer) {
      entityManager.persist(customer);
  }


}
