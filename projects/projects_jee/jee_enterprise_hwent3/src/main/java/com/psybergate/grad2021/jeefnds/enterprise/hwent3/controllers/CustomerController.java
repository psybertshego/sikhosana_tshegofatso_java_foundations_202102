package com.psybergate.grad2021.jeefnds.enterprise.hwent3.controllers;

import com.psybergate.grad2021.jeefnds.enterprise.hwent3.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent3.service.interfaces.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;


@Named
@ApplicationScoped
public class CustomerController implements Controller {

  @Inject
  private CustomerService customerService;

  public CustomerController(){
  }

  public String register() {
    return "/WEB-INF/views/AddCustomer.jsp";
  }

  public String save(Customer customer) throws IOException, ServletException {
    int result ;

    System.out.println("\n\n\n test = " + customerService == null + "\n\n\n");
    result = customerService.register(customer);

    return result > 0 ? "/WEB-INF/views/CustomerDetails.jsp" : null;
  }
}
