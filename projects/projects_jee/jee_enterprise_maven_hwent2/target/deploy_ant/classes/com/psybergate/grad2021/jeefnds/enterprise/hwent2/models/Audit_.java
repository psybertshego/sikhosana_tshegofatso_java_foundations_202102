package com.psybergate.grad2021.jeefnds.enterprise.hwent2.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Audit.class)
public abstract class Audit_ {

	public static volatile SingularAttribute<Audit, String> description;
	public static volatile SingularAttribute<Audit, Long> id;
	public static volatile SingularAttribute<Audit, String> timestamp;

	public static final String DESCRIPTION = "description";
	public static final String ID = "id";
	public static final String TIMESTAMP = "timestamp";

}

