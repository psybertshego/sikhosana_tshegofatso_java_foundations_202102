package com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.domain.Audit;

import javax.persistence.EntityManager;

public class AuditResource {

  public static void saveAudit(Audit audit, EntityManager entityManager){
    entityManager.persist(audit);
  }
}
