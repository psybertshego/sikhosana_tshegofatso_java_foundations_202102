package com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.controllers;

import java.time.LocalDate;

public interface Controller {

  default String errorMessage(){

    return "<html>" +
            "<body>" +
            "<h1> ERROR 404, page not found on server </h1>" +
            "<br> Error occurred at : " + LocalDate.now() +
            "<body>" +
            "</html>";
  }
}
