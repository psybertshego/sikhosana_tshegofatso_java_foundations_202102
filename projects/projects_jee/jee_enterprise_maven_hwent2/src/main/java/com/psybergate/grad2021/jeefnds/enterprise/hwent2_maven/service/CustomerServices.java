package com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.resource.CustomerResources;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Random;

public class CustomerServices {

  private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("CustomerAuditJPA");

  public static int register(Customer customer) throws IOException, ServletException {

    Audit audit = new Audit("adding customer to system");

    EntityManager entityManager = null;

    try {
      entityManager = EMF.createEntityManager();
      entityManager.getTransaction().begin();
      System.out.println("Starting transaction");

      CustomerResources.saveCustomer(customer, entityManager);
      AuditResource.saveAudit(audit, entityManager);

      System.out.println("Ending transaction");
      entityManager.getTransaction().commit();

    } catch (Throwable throwable) {
      System.out.println("Something went wrong bro\n\n" + throwable.getMessage() + "\n\n");
      System.out.println("throwable.getCause().getMessage() = " + throwable.getCause().getMessage() + "\n\n");
      if (entityManager != null) {
        entityManager.getTransaction().rollback();
      }
    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }


    EMF.close();
    return 1;
  }

  public static String generateRandomCustomerNum() {
    Random random = new Random();
    int number = random.nextInt(10000);
    return "C-" + number;
  }


}
