package com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.controllers;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2_maven.service.CustomerServices;

import javax.servlet.ServletException;
import java.io.IOException;

public class CustomerController implements Controller {


  public String register() {
    return "/WEB-INF/views/AddCustomer.jsp";
  }

  public String save(Customer customer) throws IOException, ServletException {
    int result ;

    result = CustomerServices.register(customer);

    return result > 0 ? "/WEB-INF/views/CustomerDetails.jsp" : null;
  }
}
