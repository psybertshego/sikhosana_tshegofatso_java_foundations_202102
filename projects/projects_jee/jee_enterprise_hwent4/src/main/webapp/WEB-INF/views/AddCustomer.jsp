<%@ page import =  "java.io.*,java.util.*,org.postgresql.Driver"
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Register</title>
</head>
<body>
 <div>
  <h1>Customer Register Form:</h1>
  <form action="<%= request.getContextPath() %>/customer/save" method="post">
   <table style="width: 80%">
    <tr>
     <td>Name</td>
     <td><input type="text" name="name" /></td>
    </tr>
    <tr>
     <td>Surname</td>
     <td><input type="text" name="surname" /></td>
    </tr>
    <tr>
        <td>Date of birth</td>
        <td><input type="date" name="date_of_birth"/></td>
    </tr>
    <tr>
        <td>Audit Description</td>
        <td><input type="text" name="audit_description"/></td>
    </tr>
   </table>
   <input type="submit" value="Submit" />
  </form>
 </div>
</body>
</html>

