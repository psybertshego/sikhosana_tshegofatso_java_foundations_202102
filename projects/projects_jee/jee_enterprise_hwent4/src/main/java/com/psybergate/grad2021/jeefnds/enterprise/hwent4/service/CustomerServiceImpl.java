package com.psybergate.grad2021.jeefnds.enterprise.hwent4.service;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource.CustomerResource;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.service.interfaces.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.Random;

@ApplicationScoped
public class CustomerServiceImpl implements CustomerService {

  @Inject
  private CustomerResource customerResource;

  @Inject
  private AuditResource auditResource;

  public int register(Customer customer) {

    Audit audit = new Audit("adding customer to system");

    customerResource.saveCustomer(customer);
    auditResource.saveAudit(audit);

    //Remove
    return 1;

  }

  public String generateRandomCustomerNum() {
    Random random = new Random();
    int number = random.nextInt(10000);
    return "C-" + number;
  }


}
