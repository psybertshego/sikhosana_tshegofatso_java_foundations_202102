package com.psybergate.grad2021.jeefnds.enterprise.hwent4.framework;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.controllers.CustomerController;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.controllers.Controller;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.controllers.ErrorMessageController;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.service.CustomerServiceImpl;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


@WebServlet(name = "enterprise_dispatcher_servlet", urlPatterns = {"/customer/register","/customer/save"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();


  {
    try {
      loadControllers();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public DispatcherServlet() {
    System.out.println("\n Enterprise Dispatcher servlet");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String[] pathInfo = req.getServletPath().split("/");
    System.out.println(req.getServletPath());

    System.out.println("We in enterprise dispatcher and requested: " + pathInfo[1]);

    String methodName ;
    String controllerName ;

    controllerName = pathInfo[1];
    methodName = pathInfo[2];

//    System.out.println("controller + method: " + controllerName + " " + methodName);
//    customerController.register();
    invokeController(req, resp, methodName, controllerName);
  }



  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {


    String[] pathInfo = request.getServletPath().split("/");
    String controllerName = pathInfo[1];


    System.out.println(request.getServletPath());

    System.out.println("We in enterprise dispatcher and posted: " + pathInfo[1]);


    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    String dateOfBirth = request.getParameter("date_of_birth");

    System.out.println(dateOfBirth);

    Customer customer = new Customer(new CustomerServiceImpl().generateRandomCustomerNum(), name, surname, LocalDate.parse(dateOfBirth));

    CustomerController customerController = (CustomerController) CDI.current().select(getController(controllerName).getClass()).get();
    request.getRequestDispatcher(customerController.save(customer)).forward(request,response);

    System.out.println(customer);

  }

  private void invokeController(HttpServletRequest req, HttpServletResponse resp, String methodName, String controllerName) throws IOException, ServletException {
    Controller requestController = getController(controllerName);

    Map<String, String[]> parameters = req.getParameterMap();

    if (requestController instanceof ErrorMessageController) {
      resp.getWriter().write(requestController.errorMessage().toCharArray());
    } else {
      String response = invokeRequestedMethod(requestController, methodName, parameters);
      if(response != null){
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(response);
        requestDispatcher.forward(req, resp);
      }
    }
  }

  private String invokeRequestedMethod(Controller controller, String methodName, Map<String, String[]> parameters) {

    String results = "";

    Method[] methods = controller.getClass().getMethods();

    for (Method method : methods) {
      if (method.getName().toLowerCase().equals(methodName)) {
        try {
          results = (String) method.invoke(controller);
        } catch (Exception e) {
          return new ErrorMessageController("An error occurred while invoking method").errorMessage();
        }
      }
    }

    //need to fix
    return results;
  }

  private Controller getController(String request){
    if (CONTROLLERS.containsKey(request)) {
      return CONTROLLERS.get(request);
    } else {
      return new ErrorMessageController("An error occurred while loading controller: \n ");
    }
  }

  private void loadControllers() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {

    Properties properties = new Properties();

    InputStream file = this.getClass().getClassLoader().getResourceAsStream("controllers.properties");

    properties.load(file);

    //load controllers from properties
    for (String propertyName : properties.stringPropertyNames()) {

      String controllerClassName = properties.getProperty(propertyName);
      CONTROLLERS.put(propertyName, (Controller) Class.forName(controllerClassName).newInstance());
    }
  }
}
