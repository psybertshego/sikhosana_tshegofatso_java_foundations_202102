package com.psybergate.grad2021.jeefnds.enterprise.hwent4.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Audit;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Named
@ApplicationScoped
public class AuditResource {

  @PersistenceContext(unitName = "CustomerAuditJPA")
  private EntityManager entityManager;

  @Transactional
  public void saveAudit(Audit audit){
    entityManager.persist(audit);
  }
}
