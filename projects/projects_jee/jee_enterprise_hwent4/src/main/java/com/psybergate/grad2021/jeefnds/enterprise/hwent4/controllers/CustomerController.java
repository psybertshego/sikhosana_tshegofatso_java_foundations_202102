package com.psybergate.grad2021.jeefnds.enterprise.hwent4.controllers;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent4.service.interfaces.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import java.io.IOException;


@Named
@ApplicationScoped
public class CustomerController implements Controller {

  @Inject
  private CustomerService customerService;

  public CustomerController(){
  }

  public String register() {
    return "/WEB-INF/views/AddCustomer.jsp";
  }

  public String save(Customer customer) throws IOException, ServletException {
    int result ;

    result = customerService.register(customer);

    return result > 0 ? "/WEB-INF/views/CustomerDetails.jsp" : null;
  }
}
