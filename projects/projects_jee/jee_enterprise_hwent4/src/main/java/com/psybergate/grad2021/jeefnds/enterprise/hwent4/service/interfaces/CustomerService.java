package com.psybergate.grad2021.jeefnds.enterprise.hwent4.service.interfaces;

import com.psybergate.grad2021.jeefnds.enterprise.hwent4.domain.Customer;

import javax.ejb.Local;

@Local
public interface CustomerService {

  public int register(Customer customer);
}
