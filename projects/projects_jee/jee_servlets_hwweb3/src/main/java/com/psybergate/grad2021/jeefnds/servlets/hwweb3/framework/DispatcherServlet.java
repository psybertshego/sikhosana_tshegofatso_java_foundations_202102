package com.psybergate.grad2021.jeefnds.servlets.hwweb3.framework;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.models.Customer;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3.services.CustomerServices;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Random;


@WebServlet(name = "dispatcher2", urlPatterns = {"/register"})
public class DispatcherServlet extends HttpServlet {
  public DispatcherServlet() {
    System.out.println("We in dispatcher2 for customer");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println(req.getContextPath());
    RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/views/AddCustomer.jsp");
    dispatcher.forward(req, resp);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    String dateOfBirth = request.getParameter("date_of_birth");

    System.out.println(dateOfBirth);

    Customer customer = new Customer(generateRandomCustomerNum(), name, surname, LocalDate.parse(dateOfBirth));

    System.out.println(customer);
    int result = 0;
    try {
      result = CustomerServices.register(customer);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (result != 0) {
      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/views/CustomerDetails.jsp");
      dispatcher.forward(request, response);
    } else {
      response.getWriter().write("<html><h1>Error: failed to save your details</h1></html>");
    }

  }

  private long generateRandomCustomerNum() {
    Random random = new Random();
    long number = random.nextLong();
    return number < 0 ? -1 * number : number;
  }
}
