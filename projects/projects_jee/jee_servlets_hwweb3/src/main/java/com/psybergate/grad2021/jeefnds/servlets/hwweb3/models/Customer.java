package com.psybergate.grad2021.jeefnds.servlets.hwweb3.models;

import java.time.LocalDate;

public class Customer {

  private long customerNum;
  private String name;
  private String surname;
  private LocalDate dateOfBirth;



  public Customer(long customerNum, String name, String surname, LocalDate dateOfBirth) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }

  public long getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCustomerNum(long customerNum) {
    this.customerNum = customerNum;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", dateOfBirth=" + dateOfBirth +
            '}';
  }
}
