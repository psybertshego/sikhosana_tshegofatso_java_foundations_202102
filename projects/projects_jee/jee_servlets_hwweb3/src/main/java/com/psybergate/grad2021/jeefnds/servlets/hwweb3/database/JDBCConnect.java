package com.psybergate.grad2021.jeefnds.servlets.hwweb3.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnect {

  private static final String JDBC_DRIVER = "org.postgresql.Driver";
  private static final String DB_URL = "jdbc:postgresql://localhost:5432/jee_servlets_hwweb3";



  public static Connection getDatabaseConnection(){
    Connection conn = null;
    try {
      Class.forName(JDBC_DRIVER);
      conn = DriverManager.getConnection(DB_URL, "postgres", "admin");
      System.out.println("Connection established");
    } catch (SQLException | ClassNotFoundException throwables) {
      throwables.printStackTrace();
    }
    return conn;
  }

}
