package com.psybergate.grad2021.jeefnds.servlets.hwweb3.controllers;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.services.CustomerServices;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3.models.Customer;

public class CustomerController implements Controller{


  public String register(){
    return "/WEB-INF/views/AddCustomer.jsp";
  }

  public String save(Customer customer){
    int result = 0;

    result = CustomerServices.register(customer);

    return result > 0 ? "../../WEB-INF/views/CustomerDetails.jsp" : null;
  }
}
