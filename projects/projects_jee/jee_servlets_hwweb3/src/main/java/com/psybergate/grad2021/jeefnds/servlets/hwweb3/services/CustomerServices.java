package com.psybergate.grad2021.jeefnds.servlets.hwweb3.services;

import com.psybergate.grad2021.jeefnds.servlets.hwweb3.database.JDBCConnect;
import com.psybergate.grad2021.jeefnds.servlets.hwweb3.models.Customer;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

public class CustomerServices {

  public static int register(Customer customer){

    StringBuilder queryBuilder = new StringBuilder("insert into customer (customer_num,name,surname,date_of_birth) values" +
            "(?,?,?,?)");

    int result = 0;

    Connection connection = JDBCConnect.getDatabaseConnection();

    try {

      PreparedStatement preparedStatement = connection.prepareStatement(queryBuilder.toString());
      preparedStatement.setString(1, customer.getCustomerNum()+"");
      preparedStatement.setString(2, customer.getName());
      preparedStatement.setString(3, customer.getSurname());
      preparedStatement.setDate(4, Date.valueOf(customer.getDateOfBirth().toString()));

//      System.out.println(preparedStatement);

      result = preparedStatement.executeUpdate();

      } catch (SQLException throwables) {
        throwables.printStackTrace();
      }

    return result;
  }
}
