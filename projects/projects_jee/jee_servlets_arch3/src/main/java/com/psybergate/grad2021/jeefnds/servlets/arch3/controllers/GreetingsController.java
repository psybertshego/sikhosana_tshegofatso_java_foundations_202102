package com.psybergate.grad2021.jeefnds.servlets.arch3.controllers;

public class GreetingsController implements Controller{

  public GreetingsController() {
    System.out.println("Greeting controller");
  }

  public String helloWorld(){
    return "hello world";
  }

  public String goodbyeWorld(){
    return "goodbye world";
  }
}
