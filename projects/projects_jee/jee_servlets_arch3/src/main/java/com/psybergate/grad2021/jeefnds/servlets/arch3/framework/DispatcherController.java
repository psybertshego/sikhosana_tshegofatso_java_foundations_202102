package com.psybergate.grad2021.jeefnds.servlets.arch3.framework;

import com.psybergate.grad2021.jeefnds.servlets.arch3.ResponseUtils;
import com.psybergate.grad2021.jeefnds.servlets.arch3.controllers.Controller;
import com.psybergate.grad2021.jeefnds.servlets.arch3.controllers.ErrorMessageController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


@WebServlet(name = "dispatcher_servlet", urlPatterns = "/*")
public class DispatcherController extends HttpServlet {

  private static Map<String, Controller> controllers = new HashMap<>();

  {
    try {
      loadControllers();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InstantiationException e) {
      e.printStackTrace();
    }
  }

  public DispatcherController() {
    System.out.println("Dispatcher servlet");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String[] pathInfo = req.getPathInfo().split("/");

    System.out.println("We in dispatcher and requested: " + pathInfo[1]);

    String methodName = "";
    String controllerName = "";

    if (pathInfo.length > 1) {
      controllerName = pathInfo[1];
      methodName = pathInfo[2];
    } else if (pathInfo.length == 1) {
      controllerName = pathInfo[1];
    }

//    System.out.println("controller + method: " + controllerName + " " + methodName);

    Controller requestController = getController(controllerName);

    Map<String, String[]> parameters = req.getParameterMap();

    if (requestController instanceof ErrorMessageController) {
      resp.getWriter().write(requestController.errorMessage().toCharArray());
    } else {
      String response = invokeRequestedMethod(requestController, methodName, parameters);
      resp.getWriter().write(response.toCharArray());
    }
  }

  private String invokeRequestedMethod(Controller controller, String methodName, Map<String, String[]> parameters) {

    String results = "";

    Method[] methods = controller.getClass().getMethods();

    for (Method method : methods) {
      if (method.getName().toLowerCase().equals(methodName)) {
        try {
          results = (String) method.invoke(controller);
        } catch (IllegalAccessException e) {
          return new ErrorMessageController().errorMessage();
        } catch (InvocationTargetException e) {
          return new ErrorMessageController().errorMessage();
        }
      }
    }
    return ResponseUtils.getPageResponse(results);
  }

  private Controller getController(String request) throws IOException {
    if (controllers.containsKey(request)) {
      return controllers.get(request);
    } else {
      return new ErrorMessageController();
    }
  }

  private void loadControllers() throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {

    Properties properties = new Properties();

    InputStream file = this.getClass().getClassLoader().getResourceAsStream("controllers.properties");

    properties.load(file);

    //load controllers from properties
    for (String propertyName : properties.stringPropertyNames()) {

      String controllerClassName = properties.getProperty(propertyName);
      controllers.put(propertyName, (Controller) Class.forName(controllerClassName).newInstance());
    }
  }
}
