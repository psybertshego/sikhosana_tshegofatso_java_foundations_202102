package com.psybergate.grad2021.jeefnds.servlets.arch3.controllers;

import java.time.LocalDate;

public class DateController implements Controller{

  public String today() {
    return "Today is " + LocalDate.now().toString();
  }

  public String nextWeek(){
    return LocalDate.now().plusDays(7).toString() + " is a week away from today";
  }
}
