package com.psybergate.grad2021.jeefnds.servlets.arch3;

public class ResponseUtils {

  public static String getPageResponse(String message){
    StringBuilder response = new StringBuilder();
    response.append("<html>");
    response.append("<body>");
    response.append("<h1> "+ message +"</h1>");
    response.append("<body>");
    response.append("</html>");

    return response.toString();
  }
}
