package com.psybergate.grad2021.jeefnds.servlets.hwweb2;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;



@WebServlet(name = "helloapp", urlPatterns = "/hello")


public class HelloWorldServlet extends HttpServlet {

  public HelloWorldServlet() {
    System.out.println("New servlet");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    System.out.println("In service doGet");

    PrintWriter writer = resp.getWriter();

    writer.println("<html>");
    writer.println("<body>");
    writer.println("<h1> Hello World, from Tshego </h1>");
    writer.println("<br> Said hello at : " + LocalDate.now());
    writer.println("<body>");
    writer.println("</html>");

  }

  @Override
  protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("In service HttpServletRequest");
    super.service(req, resp);
  }

  @Override
  public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
    System.out.println("In service ServletRequest");
    super.service(req, res);
  }
}
