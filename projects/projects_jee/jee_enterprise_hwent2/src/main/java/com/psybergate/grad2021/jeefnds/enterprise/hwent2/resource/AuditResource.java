package com.psybergate.grad2021.jeefnds.enterprise.hwent2.resource;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Audit;

import javax.persistence.EntityManager;

public class AuditResource {

  public static void saveAudit(Audit audit, EntityManager entityManager){
    entityManager.persist(audit);
  }
}
