package com.psybergate.grad2021.jeefnds.enterprise.hwent2.controllers;

import com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.hwent2.service.CustomerServices;

import javax.servlet.ServletException;
import java.io.IOException;

public class CustomerController implements Controller {


  public String register() {
    return "/WEB-INF/views/AddCustomer.jsp";
  }

  public String save(Customer customer) throws IOException, ServletException {
    int result = 0;

    result = CustomerServices.register(customer);

    return result > 0 ? "/WEB-INF/views/CustomerDetails.jsp" : null;
  }
}
