package com.psybergate.grad2021.jeefnds.enterprise.hwent2.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long id;

  @Column
  private String timestamp;

  @Column
  private String description;

  public Audit() {
  }

  public Audit(String description) {
    this.timestamp = LocalDateTime.now().toString();
    this.description = description;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getDescription() {
    return description;
  }

  public void log(String description){
    timestamp = LocalDateTime.now().toString();
    this.description = description;
  }
}
