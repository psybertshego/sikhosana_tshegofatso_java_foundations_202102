package com.psybergate.grad2021.jeefnds.servlets.hwweb5.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.Map;

@WebFilter(urlPatterns = "/hello")
public class NameFilter implements Filter {
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    ServletContext servletContext = servletRequest.getServletContext();

    Map<String, String[]> params = servletRequest.getParameterMap();

    String[] names = params.get("name");

//    System.out.println("names[0] = " + names[0]);

    if(names != null){
      if(!names[0].equals("John")){
        filterChain.doFilter(servletRequest,servletResponse);
      }else{
        servletResponse.getWriter().write("John is unavailable");
      }
    }
    else{
      filterChain.doFilter(servletRequest,servletResponse);
    }
  }

  @Override
  public void destroy() {

  }
}
