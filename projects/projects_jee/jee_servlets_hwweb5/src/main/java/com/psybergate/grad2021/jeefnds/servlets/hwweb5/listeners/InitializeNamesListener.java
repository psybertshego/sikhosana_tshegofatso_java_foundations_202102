package com.psybergate.grad2021.jeefnds.servlets.hwweb5.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class InitializeNamesListener implements ServletContextListener {

  public void contextInitialized(ServletContextEvent sce) {

    System.out.println("Initializing");
    List<String> names = new ArrayList<>();

    names.add("Rigby");
    names.add("Jake");
    names.add("Fin");
    names.add("Ben");
    names.add("Gwen");

    sce.getServletContext().setAttribute("names",names);
  }
}
