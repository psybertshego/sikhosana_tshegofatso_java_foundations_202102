package com.psybergate.grad2021.jeefnds.servlets.hwweb5.servlets;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Random;

@WebServlet(name = "hello_servlet",urlPatterns = "/hello")
public class HelloServlet extends HttpServlet {

  @Override
  public void init() throws ServletException {
    super.init();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    Map<String,String[]> parameters = req.getParameterMap();


    ServletContext servletContext = getServletContext();

    System.out.println("req.getParameter(\"name\") = " + req.getParameter("name"));

    List<String> names = (List<String>) servletContext.getAttribute("names");

    Random random  = new Random();

    if(parameters.isEmpty()){
      req.setAttribute("message","Hello " + names.get(random.nextInt(names.size())));
    }
    else{
      req.setAttribute("message","Hello " + parameters.get("name")[0]);
    }


    req.getRequestDispatcher("WEB-INF/views/helloworld.jsp").forward(req,resp);
  }
}
