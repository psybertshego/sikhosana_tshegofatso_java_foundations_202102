package com.psybergate.grad2021.anttest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@WebServlet(name = "ant_test", urlPatterns = "/antgreeting")
public class HelloWorldServletTest extends HttpServlet {


  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Path currentRelativePath = Paths.get("");

    Path reader = Paths.get(currentRelativePath + "ant_test/out/production/ant_test/index.html");

    byte[] page = Files.readAllBytes(reader);

    PrintWriter writer = resp.getWriter();

    writer.write(page.toString());
  }
}
