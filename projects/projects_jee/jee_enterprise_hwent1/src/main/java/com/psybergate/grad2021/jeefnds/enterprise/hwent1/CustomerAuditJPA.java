package com.psybergate.grad2021.jeefnds.enterprise.hwent1;

import com.psybergate.grad2021.jeefnds.enterprise.hwent1.models.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.hwent1.models.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class CustomerAuditJPA {

  private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("CustomerAuditJPA");

  public CustomerAuditJPA() {
  }

  public void saveCustomer(Customer customer) {
    Audit audit = new Audit("adding customer to system");

    EntityManager entityManager = null;

    try {
      entityManager = EMF.createEntityManager();
      entityManager.getTransaction().begin();

      entityManager.persist(customer);
      entityManager.persist(audit);

      entityManager.getTransaction().commit();
    } catch (Throwable throwable) {
      if (entityManager != null) {
        entityManager.getTransaction().rollback();
      }
    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  public static void close() {
    EMF.close();
  }

}
