package com.psybergate.grad2021.jeefnds.enterprise.hwent1;

import com.psybergate.grad2021.jeefnds.enterprise.hwent1.models.Customer;

import java.sql.SQLException;

public class Client {

  public static void main(String[] args) throws SQLException {

    CustomerAuditJPA customerJPA = new CustomerAuditJPA();

    customerJPA.saveCustomer(new Customer("C-1000","Darwin","Wattason"));

    CustomerAuditJPA.close();
  }
}
