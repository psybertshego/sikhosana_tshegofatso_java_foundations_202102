package com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard;

public interface Command {

  public void setParameter(Object parameter);
  //Results after execution
  public Object getResult();

  public String getName();


}
