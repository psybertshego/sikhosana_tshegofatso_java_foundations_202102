package com.psybergate.grad2021.jee_basics.interfaces.ce3a.vendorimpl1;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandRequest;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandResponse;


/**
 * Wrapping results from command
 */
public class CommandResponseImpl implements CommandResponse {

  private CommandRequest commandRequest;

  private Object results;

  public CommandResponseImpl(CommandRequest commandRequest, Object results) {
    this.commandRequest = commandRequest;
    this.results = results;
  }

  @Override
  public Object getResults(){
    return results;
  }

  @Override
  public CommandRequest getCommandRequest() {
    return commandRequest;
  }
}
