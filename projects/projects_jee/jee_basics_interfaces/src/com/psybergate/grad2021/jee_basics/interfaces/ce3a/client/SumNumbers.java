package com.psybergate.grad2021.jee_basics.interfaces.ce3a.client;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.Command;

import java.util.List;

public class SumNumbers implements Command {

  private String name = "Sum numbers";
  private List<Integer> numbers;

  public SumNumbers(List<Integer> numbers) {
    this.numbers = numbers;
  }

  public SumNumbers() {
  }

  @Override
  public void setParameter(Object parameter) {
    numbers = (List<Integer>) parameter;
  }

  @Override
  public Object getResult() {
    int sum = 0;
    for (Integer number : numbers) {
      sum += number;
    }

    return sum;
  }

  @Override
  public String getName() {
    return name;
  }
}
