package com.psybergate.grad2021.jee_basics.interfaces.ce2a.jdbcdriver.client;

import com.psybergate.grad2021.jee_basics.interfaces.ce2a.jdbcdriver.vendor.MyDriver;

import java.sql.*;

public class MyDatabaseDemo {
  public static void main(String[] args) throws SQLException {


    Driver myDriver = new MyDriver();
    DriverManager.registerDriver(myDriver);

    Connection conn = DriverManager.getConnection("my database");
    Statement statement = conn.createStatement();
    ResultSet resultSet = statement.getResultSet();

  }
}
