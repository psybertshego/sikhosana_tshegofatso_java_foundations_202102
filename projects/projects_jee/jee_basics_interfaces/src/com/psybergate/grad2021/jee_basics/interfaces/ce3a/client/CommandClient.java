package com.psybergate.grad2021.jee_basics.interfaces.ce3a.client;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.Command;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandEngine;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandResponse;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.vendorimpl1.CommandEngineVendor1;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.vendorimpl1.CommandRequestImpl;

import java.util.Arrays;

public class CommandClient {

  public static void main(String[] args) {

    CommandEngine commandEngine = new CommandEngineVendor1();

    Command helloWorld1 = new HelloWorld();

    Command helloWorld2 = new HelloWorld();

    Command sumNumbers = new SumNumbers();



    CommandResponse response = commandEngine.execute(new CommandRequestImpl(helloWorld1,"Jane"));

    CommandResponse response2 = commandEngine.execute(new CommandRequestImpl(sumNumbers,Arrays.asList(1,2,3,4,5)));

    System.out.println("response = " + response.getResults());
    System.out.println("response2 = " + response2.getResults());
  }
}
