package com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard;


/**
 * This class is used to execute commands
 */
public interface CommandEngine {

  public CommandResponse execute(CommandRequest command);
}
