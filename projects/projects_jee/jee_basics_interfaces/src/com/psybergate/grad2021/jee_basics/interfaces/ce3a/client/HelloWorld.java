package com.psybergate.grad2021.jee_basics.interfaces.ce3a.client;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.Command;

import java.time.LocalDateTime;

public class HelloWorld implements Command {

  private final String name = "hello world";

  private String parameter;

  private LocalDateTime executed_at;

  public HelloWorld(String parameter) {
    this.parameter = parameter;
  }

  public HelloWorld() {
    parameter = null;
  }

  @Override
  public void setParameter(Object parameter) {
    this.parameter = (String) parameter;
  }

  @Override
  public Object getResult() {

    String result;

    if (parameter == null) {
      result = name.toUpperCase();
    } else {
      result = "Hello world, from " + parameter;
    }

    return result;
  }

  @Override
  public String getName() {
    return name;
  }
}
