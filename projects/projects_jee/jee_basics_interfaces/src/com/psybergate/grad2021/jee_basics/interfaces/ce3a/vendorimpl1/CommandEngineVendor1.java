package com.psybergate.grad2021.jee_basics.interfaces.ce3a.vendorimpl1;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandEngine;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandRequest;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandResponse;

public class CommandEngineVendor1 implements CommandEngine {


  //TODO allow engine to execute multiple results
  @Override
  public CommandResponse execute(CommandRequest commandRequest) {

    System.out.println("commandRequest.getCommand().getName() = " + commandRequest.getCommand().getName());
    System.out.println("commandRequest.getRequestParameter() = " + commandRequest.getRequestParameter());

    Object results = commandRequest.getCommand().getResult();

    return new CommandResponseImpl(commandRequest, results);
  }
}
