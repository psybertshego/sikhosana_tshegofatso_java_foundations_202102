package com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard;

/**
 * getCommandRequest() is used to get a handle to the requested command
 */
public interface CommandResponse {

  public CommandRequest getCommandRequest();

  public Object getResults();

}
