package com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard;



public interface CommandRequest {

  public Command getCommand();

  public Object getRequestParameter();

}
