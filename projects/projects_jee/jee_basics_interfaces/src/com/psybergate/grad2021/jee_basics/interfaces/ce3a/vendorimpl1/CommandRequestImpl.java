package com.psybergate.grad2021.jee_basics.interfaces.ce3a.vendorimpl1;

import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.Command;
import com.psybergate.grad2021.jee_basics.interfaces.ce3a.standard.CommandRequest;

public class CommandRequestImpl implements CommandRequest {

  private Command command;

  private Object requestParameter;

  public CommandRequestImpl(Command command, Object requestParameter) {
    this.command = command;
    this.requestParameter = requestParameter;
    command.setParameter(requestParameter);
  }

  @Override
  public Command getCommand() {
    return command;
  }

  @Override
  public Object getRequestParameter() {
    return requestParameter;
  }


//  public CommandResponse getResult() {
//
//    if(requestParameter == null){
//      return null;
//    }
//
//
//    String result = "Hello world";
//    CommandResponse response = new CommandResponseImpl(this,result);
//    command.log();
//
//    return response;
//  }
}
