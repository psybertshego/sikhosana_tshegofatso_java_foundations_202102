package com.psybergate.grad2021.jeefnds.servlets.hwweb2.framework;


import com.psybergate.grad2021.jeefnds.servlets.hwweb2.controllers.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


@WebServlet(name = "dispatcher_servlet", urlPatterns = "/dispatcher/*")
public class DispatcherController extends HttpServlet {

  private static HashMap<String, Controller> controllers = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadControllers();
  }

  public DispatcherController() {
    System.out.println("Dispatcher servlet");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String servletName = req.getPathInfo();

    String name = req.getParameter("name");
    System.out.println("req = " + req.getParameter("name"));

    System.out.println("We in dispatcher and requested: " + servletName);

    resp.getWriter().write(getController(servletName).execute(name).toCharArray());

  }

  private Controller getController(String request){
    if(controllers.containsKey(request)){
      return controllers.get(request);
    }else {
      return new ErrorMessageController();
    }
  }

  private void loadControllers() {
    controllers.put("/helloservlet2", new HelloWorldController());
    controllers.put("/goodbyeservlet2", new GoodbyeWorldController());
    controllers.put("/dateservlet2", new DateController());
  }


}
