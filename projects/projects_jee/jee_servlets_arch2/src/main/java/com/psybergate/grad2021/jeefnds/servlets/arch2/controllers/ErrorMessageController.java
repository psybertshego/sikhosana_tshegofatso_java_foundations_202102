<<<<<<< HEAD
package com.psybergate.grad2021.jeefnds.servlets.helloworld;
=======
package com.psybergate.grad2021.jeefnds.servlets.helloworld.controllers;
>>>>>>> remotes/origin/master

import com.psybergate.grad2021.jeefnds.servlets.hwweb2.controllers.Controller;

import java.io.IOException;
import java.time.LocalDate;

public class ErrorMessageController implements Controller {

  public ErrorMessageController() {
    System.out.println("An error occurred");
  }

  public String execute(String... params) throws IOException {

    StringBuilder response = new StringBuilder();
    response.append("<html>");
    response.append("<body>");
    response.append("<h1> ERROR 404, page not found on server </h1>");
    response.append("<br> Error occurred at : " + LocalDate.now());
    response.append("<body>");
    response.append("</html>");

    return response.toString();
  }
}
