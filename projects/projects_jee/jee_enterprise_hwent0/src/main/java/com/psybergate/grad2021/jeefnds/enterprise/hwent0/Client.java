import database.JDBCConnect;
import database.Services;
import models.Audit;
import models.Customer;

import java.sql.Connection;
import java.sql.SQLException;

public class Client {

  public static void main(String[] args) throws SQLException {


    Connection conn = JDBCConnect.getDatabaseConnection();

    Services services = new  Services(conn);
    services.startTransaction();

    services.storeCustomer(new Customer("C-1000","Gumball","Watteson"));
    services.storeAudit(new Audit("creating customer account"));

    services.commit();
    services.closeConnection();
  }
}
