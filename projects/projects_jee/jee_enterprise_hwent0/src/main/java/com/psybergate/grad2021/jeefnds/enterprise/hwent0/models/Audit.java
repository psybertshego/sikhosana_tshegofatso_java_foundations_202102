package models;

import java.time.LocalDateTime;

public class Audit {

  private String timestamp;
  private String description;

  public Audit(String description) {
    this.timestamp = LocalDateTime.now().toString();
    this.description = description;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getDescription() {
    return description;
  }
}
