package models;

import java.time.LocalDate;

public class Customer {

  private String customerNum;
  private String name;
  private String surname;

  public Customer(String customerNum, String name, String surname) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }


  public void setName(String name) {
    this.name = name;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }


  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerNum=" + customerNum +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            '}';
  }
}
