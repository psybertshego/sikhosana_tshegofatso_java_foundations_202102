package database;

import models.Audit;
import models.Customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Services {

  private Connection conn;

  /**
   *
   * @param conn
   */
  public Services(Connection conn) {
    this.conn = conn;
  }

  public void startTransaction() throws SQLException {
    conn.setAutoCommit(false);
  }

  public void storeCustomer(Customer customer) {
    String sql = "insert into customer values (?,?,?)";

    try {
      PreparedStatement statement = conn.prepareStatement(sql);
      statement.setString(1, customer.getCustomerNum());
      statement.setString(2, customer.getName());
      statement.setString(3, customer.getSurname());
      statement.execute();
      statement.close();

    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void storeAudit(Audit audit) {
    String sql = "insert into audit values (?,?)";

    try {
      PreparedStatement statement = conn.prepareStatement(sql);
      statement.setString(1, audit.getTimestamp());
      statement.setString(2, audit.getDescription());
      statement.execute();
      statement.close();

    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void closeConnection() {
    try {
      conn.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void commit() throws SQLException {
    conn.commit();
  }


}
