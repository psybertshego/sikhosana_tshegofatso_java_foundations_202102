package com.psybergate.database.basics.hw2b;

public enum AccountType {
  SAVINGS_ACCOUNT,
  CURRENT_ACCOUNT,
  CREDIT_CARD
}
