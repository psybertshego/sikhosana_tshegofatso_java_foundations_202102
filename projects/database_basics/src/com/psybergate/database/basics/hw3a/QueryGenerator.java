package com.psybergate.database.basics.hw3a;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QueryGenerator {

  private static int accountID = 1;
  private static int transactionID = 1;
  private static final String PATH = "./src/com/psybergate/database/basics/hw3a/";

  public static void main(String[] args) {
    List<String> customers = generateCustomers();

    List<String> accounts = generateAccounts(customers);

    List<String> transactions = generateTransactions(accounts);

    printToFile("customers",customers);
    printToFile("accounts",accounts);
    printToFile("transactions",transactions);
  }

//  TODO:Use a generic method for the generate methods

  private static List<String> generateCustomers() {
    List<String> customers = new ArrayList<>();
    for (int i = 1; i <= 20; i++) {
      customers.add(CustomerData.generateCustomerInsertQuery(i));
    }
    return customers;
  }

  private static List<String> generateAccounts(List<String> customers) {
    List<String> accounts = new ArrayList<>();
    Random numberOfAccounts = new Random();

    for (int i = 1; i <= customers.size(); i++) {
      for (int j = numberOfAccounts.nextInt(8); j > 0; j--) {
        accounts.add(AccountData.generateAccountInsertQuery(accountID, i));
        accountID++;
      }
    }
    return accounts;
  }

  private static List<String> generateTransactions(List<String> accounts) {
    List<String> transactions = new ArrayList<>();
    Random numberOfTransactions = new Random();

    for (int i = 1; i <= accounts.size(); i++) {
      for (int j = numberOfTransactions.nextInt(100); j > 0; j--) {
        transactions.add(TransactionData.generateTransactionInsertQuery(transactionID, i));
        transactionID++;
      }
    }
    return transactions;
  }

  private static void printToFile(String name, List<String> queries){
    try {
      FileWriter file = new FileWriter(PATH + name + ".sql");
      for (int i = 0; i < queries.size(); i++) {
        file.write(queries.get(i) + "\n");
      }

      file.close();
      System.out.println("Done writing insert queries to " + name + ".sql");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
