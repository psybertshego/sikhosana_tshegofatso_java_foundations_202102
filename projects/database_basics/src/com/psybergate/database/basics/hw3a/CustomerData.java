package com.psybergate.database.basics.hw3a;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CustomerData {

  public static String generateCustomerInsertQuery(int id) {

    List<String> names = getRandomNames();
    Random random = new Random();
    String customerNum = getRandomCustomerNum(random);
    String name = getName(names);
    String surname = getName(names);
    int idNumber = 19000 + random.nextInt(900_000_000);
    String insertCustomerQuery = "INSERT INTO customer (id,customer_num,name,surname,id_number) values (";

    return insertCustomerQuery.concat(id + "," + customerNum + "," + name + "," + surname + "," + idNumber + ");");
  }

  private static String getRandomCustomerNum(Random random) {
    return formatForVarchar("C-" + (1000 + random.nextInt(9000)));
  }

  private static String formatForVarchar(String value) {
    return "'" + value + "'";
  }

  private static String getName(List<String> names) {
    Random random = new Random();
    return formatForVarchar(names.get(random.nextInt(names.size())));
  }

  private static List<String> getRandomNames() {
    List<String> names = new ArrayList<>();
    names.add("jake");
    names.add("fin");
    names.add("bubblegum");
    names.add("darwin");
    names.add("nicole");
    names.add("gumball");
    names.add("richard");
    names.add("rigby");
    names.add("mordecia");
    names.add("margret");
    names.add("ben");
    names.add("skips");
    names.add("benson");
    names.add("steven");
    names.add("robin");
    names.add("raven");
    names.add("watson");
    names.add("parker");
    names.add("stark");
    names.add("edison");
    names.add("jack");
    names.add("jill");
    return names;
  }

}
