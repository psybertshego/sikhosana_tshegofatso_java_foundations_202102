package com.psybergate.database.basics.hw3a;

import java.time.LocalDate;
import java.util.Random;

public class AccountData {

  public static String generateAccountInsertQuery(int id, int customer_id) {

    int accountNum = getRandomAccountNum();
    int accountTypeID = getRandomAccountType();
    double balance = generateBalance(accountTypeID);

    String insertQuery = "INSERT INTO account (id,account_num,balance,account_type_id,customer_id) values (";

    return insertQuery + id + "," + accountNum + "," + balance + "," + accountTypeID + "," + customer_id + ")";
  }


  //TODO: write a utils
  private static double generateBalance(int accountTypeID) {
    Random random = new Random();
    double balance = random.nextDouble() * 10000;
    balance = (Math.round(balance * 100.0) / 100.0);

    return (randomInt(0,100) % 23 != 0 && accountTypeID != 1) ? balance : -1 * balance;
  }

  private static int getRandomAccountNum() {
    return randomInt(10000,900000);
  }

  private static int getRandomAccountType() {
    return randomInt(1,3);
  }


  private static int randomInt(int min, int max){
    Random randomizer = new Random();
    return randomizer.nextInt(max) + min;

  }
}
