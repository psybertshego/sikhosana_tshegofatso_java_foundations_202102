package com.psybergate.database.basics.hw3a_object;

public enum AccountType {
  SAVINGS_ACCOUNT,
  CURRENT_ACCOUNT,
  CREDIT_CARD
}
