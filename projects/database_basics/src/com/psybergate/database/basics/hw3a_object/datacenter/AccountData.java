package com.psybergate.database.basics.hw3a_object.datacenter;

import com.psybergate.database.basics.hw3a_object.Account;
import com.psybergate.database.basics.hw3a_object.AccountType;
import com.psybergate.database.basics.hw3a_object.Customer;

import java.util.Random;

public class AccountData {

  public static void main(String[] args) {
    for (int i = 0; i < 100; i++) {
      double bal = generateBalance(getRandomAccountType());
      if(bal < 0) {
        System.out.println(bal);;
      }
    }
  }

  public static Account generateRandomAccount(Customer owner) {

    Random randomizer = new Random();
    int accountNum = getRandomAccountNum(randomizer);
    double balance = generateBalance(getRandomAccountType());

    Account randomAccount = new Account(accountNum, balance, getRandomAccountType(), owner);

    return randomAccount;
  }

  private static double generateBalance(AccountType accountType) {
    Random random = new Random();
    double balance = random.nextDouble() * 10000;

    if (random.nextInt(100) % 2 == 0 && accountType.equals(AccountType.CREDIT_CARD)) {
      return -1 * (Math.round(balance * 100.0) / 100.0);
    }

    return Math.round(balance * 100.0) / 100.0;
  }

  private static int getRandomAccountNum(Random random) {
    return (1000 + random.nextInt(9000));
  }

  private static AccountType getRandomAccountType() {
    Random randomizer = new Random();

    if (randomizer.nextInt(100) % 3 == 0) return AccountType.SAVINGS_ACCOUNT;
    if (randomizer.nextInt(100) % 3 == 1) return AccountType.CURRENT_ACCOUNT;
    if (randomizer.nextInt(100) % 3 == 2) return AccountType.CREDIT_CARD;

    return getRandomAccountType();
  }
}
