package com.psybergate.database.basics.hw3a_object;

public class Transaction {

  private String transactionID;

  private double amount;

  private TransactionType transactionType;

  private Account account;

  public Transaction(String transactionID, double amount, TransactionType transactionType, Account account) {
    this.transactionID = transactionID;
    this.amount = amount;
    this.transactionType = transactionType;
    this.account = account;
  }

  public String getTransactionID() {
    return transactionID;
  }

  public TransactionType getTransactionType() {
    return transactionType;
  }

  public Account getAccount() {
    return account;
  }

  public double getAmount() {
    return amount;
  }
}
