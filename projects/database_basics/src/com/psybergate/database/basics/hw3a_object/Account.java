package com.psybergate.database.basics.hw3a_object;

public class Account {

  private int accountNum; // 10- digit int

  private double balance;

  private AccountType accountType;

  private Customer owner;

  public Account(int accountNum, double balance, AccountType accountType, Customer owner) {
    this.accountNum = accountNum;
    this.balance = balance;
    this.accountType = accountType;
    this.owner = owner;
  }

  public int getAccountNum() {
    return accountNum;
  }

  public double getBalance() {
    return balance;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public Customer getOwner() {
    return owner;
  }

}
