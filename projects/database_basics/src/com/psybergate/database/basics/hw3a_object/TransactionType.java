package com.psybergate.database.basics.hw3a_object;

public enum TransactionType {
  WITHDRAWAL,
  DEPOSIT
}
