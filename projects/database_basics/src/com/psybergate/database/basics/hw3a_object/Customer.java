package com.psybergate.database.basics.hw3a_object;

public class Customer {

  private String customerNum;

  private String name;

  private String surname;

  private int idNumber;

  public Customer(String customerNum, String name, String surname, int idNumber) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.idNumber = idNumber;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public int getIdNumber() {
    return idNumber;
  }
}
