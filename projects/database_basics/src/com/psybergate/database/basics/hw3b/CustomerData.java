package com.psybergate.database.basics.hw3b;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CustomerData {

  public static String generateCustomerInsertQuery(int id) {

    List<String> names = getRandomNames();
    Random random = new Random();
    String customerNum = getRandomCustomerNum(random);
    String name = getName(names);
    String surname = getName(names);
    int idNumber = 19000 + random.nextInt(900_000_000);
    String city = getCity(random);
    String insertCustomerQuery = "INSERT INTO customer (id,customer_num,name,surname,id_number,city) values (";

    return insertCustomerQuery.concat(id + "," + customerNum + "," + name + "," + surname + "," + idNumber +
            "," + city + ");");
  }

  private static String getRandomCustomerNum(Random random) {
    return SQLUtils.formatForVarchar("C-" + (1000 + random.nextInt(9000)));
  }

  private static String getName(List<String> names) {
    Random random = new Random();
    return SQLUtils.formatForVarchar(names.get(random.nextInt(names.size())));
  }

  private static List<String> getRandomNames() {
    List<String> names = new ArrayList<>();
    names.add("jake");
    names.add("fin");
    names.add("bubblegum");
    names.add("darwin");
    names.add("nicole");
    names.add("gumball");
    names.add("richard");
    names.add("rigby");
    names.add("mordecia");
    names.add("margret");
    names.add("ben");
    names.add("skips");
    names.add("benson");
    names.add("steven");
    names.add("robin");
    names.add("raven");
    names.add("watson");
    names.add("parker");
    names.add("stark");
    names.add("edison");
    names.add("jack");
    names.add("jill");
    return names;
  }

  private static String getCity(Random random) {
    List<String> cities = new ArrayList<>();
    cities.add("Pretoria");
    cities.add("Johannesburg");
    cities.add("Cape Town");
    cities.add("Durban");
    cities.add("Kimberly");
    cities.add("Polokwane");
    cities.add("Bloemfontein");

    return SQLUtils.formatForVarchar(cities.get(random.nextInt(cities.size())));

  }

}
