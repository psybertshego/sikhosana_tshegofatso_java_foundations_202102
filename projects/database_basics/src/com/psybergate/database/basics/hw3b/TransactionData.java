package com.psybergate.database.basics.hw3b;

import java.util.Random;

public class TransactionData {

  public static String generateTransactionInsertQuery(int id, int accountID) {
    int amount = getAmount();
    int transactionTypeID = getTransactionType();
    String insertQuery = "INSERT INTO transaction (id,amount,transaction_type_id,account_id) values (";

    return insertQuery + id + "," + amount + "," + transactionTypeID + "," + accountID + ");";
  }

  private static int getTransactionType() {
    Random random = new Random();
    return random.nextInt(2) + 1; // 1 -3?
  }

  private static int getAmount() {
    Random random = new Random();
    return 100 + random.nextInt(100000);
  }

}
