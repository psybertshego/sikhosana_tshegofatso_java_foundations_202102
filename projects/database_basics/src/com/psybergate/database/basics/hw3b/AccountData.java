package com.psybergate.database.basics.hw3b;

import java.time.LocalDate;
import java.util.Random;
public class AccountData {

  public static String generateAccountInsertQuery(int id, int customer_id) {

    int accountNum = getRandomAccountNum();
    int accountTypeID = getRandomAccountType();
    double balance = generateBalance(accountTypeID);
    String openDate = generateRandomDate();

    String insertQuery = "INSERT INTO account (id,account_num,balance,account_type_id,customer_id,open_date) values (";

    return insertQuery + id + "," + accountNum + "," + balance + "," + accountTypeID + "," + customer_id + ","
            + openDate + ");";
  }


  //TODO: write a utils
  private static double generateBalance(int accountTypeID) {
    Random random = new Random();
    double balance = random.nextDouble() * 10000;
    balance = (Math.round(balance * 100.0) / 100.0);

    return (randomInt(0, 100) % 23 != 0 && accountTypeID != 1) ? balance : -1 * balance;
  }

  private static int getRandomAccountNum() {
    return randomInt(100000, 900000);
  }

  private static int getRandomAccountType() {
    return randomInt(1, 3);
  }

  public static String generateRandomDate() {
    int year = getYearRange(2015, 2022);
    String date = LocalDate.ofYearDay(year, randomInt(1, 365)).toString();
    return SQLUtils.formatForVarchar(date);
  }

  private static int getYearRange(int min, int max) {
    return (int) Math.floor(Math.random() * (max - min + 1) + min);
  }

  private static int randomInt(int min, int max) {
    Random randomizer = new Random();
    return randomizer.nextInt(max) + min;
  }
}

